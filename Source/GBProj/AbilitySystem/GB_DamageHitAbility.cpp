// Fill out your copyright notice in the Description page of Project Settings.


#include "GB_DamageHitAbility.h"

#include "GBProj/Actors/GBCharacterBase.h"
#include "GBProj/Actors/Weapon/WeaponBase.h"
#include "Tasks/AbilityTask_WaitDamageHit.h"

float UGB_DamageHitAbility::CalculateDamage()
{
	if(bForceAbilityDamage)
	{
		return Damage;
	}
	
	if(ActiveWeapon)
	{
		return ActiveWeapon->GetDamage() * DamageCoefficient;
	}
	
	return Damage;
}

FDamageHitInfo UGB_DamageHitAbility::MakeDamageHitInfo()
{
	FDamageHitInfo Info;

	Info.Damage = CalculateDamage();
	Info.bCanBeBlocked = bCanBeBlocked;
	Info.bCanBeParried = bCanBeParried;
	Info.HitEffect = DamageEffect;

	return Info;
}

void UGB_DamageHitAbility::ApplyDamageHitToActor(FDamageHitInfo Info, const AActor* Target)
{
	UGBAbilitySystemComponent* SourceASC = Cast<UGBAbilitySystemComponent>(GetAbilitySystemComponentFromActorInfo());
	if(SourceASC)
	{
		SourceASC->ApplyAbilityDamageHitToActor(Info ,Target);
	}
}

bool UGB_DamageHitAbility::CommitAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo)
{
	const bool Result = Super::CommitAbility(Handle, ActorInfo, ActivationInfo);

	if(OwningCharacter)
	{
		ActiveWeapon = OwningCharacter->GetCurrentWeapon();
	}
	
	return Result;
}

void UGB_DamageHitAbility::ApplyDamageHitToTarget(FDamageHitInfo Info, UGBAbilitySystemComponent* TargetASC)
{
	UGBAbilitySystemComponent* SourceASC = Cast<UGBAbilitySystemComponent>(GetAbilitySystemComponentFromActorInfo());
	if(SourceASC)
	{
		SourceASC->ApplyAbilityDamageHitToTarget(Info, TargetASC);
	}
}

