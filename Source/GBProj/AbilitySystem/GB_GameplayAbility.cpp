// Fill out your copyright notice in the Description page of Project Settings.


#include "GB_GameplayAbility.h"

#include "GBProj/Actors/GBCharacterBase.h"
#include "GBProj/Func/GBLib.h"
#include "GBProj/Game/GBGameInstance.h"

UGB_GameplayAbility::UGB_GameplayAbility()
{

}


void UGB_GameplayAbility::SetupTags(const UObject* WorldContextObject)
{
		
	for(const auto& It : TagsPresetsToLoad)
	{
		FAbilityTagsPreset Preset;
		UGBGameInstance* MyGI = UGBLib::GetGBGameInstance(WorldContextObject);
		if(MyGI)
		{
				
			// If find preset with given name add tags to ability.
			if(MyGI->GetPresetAbilityTagsByName(It, Preset))
			{
				AbilityTags.AppendTags(Preset.AbilityTags);
				CancelAbilitiesWithTag.AppendTags(Preset.CancelAbilitiesWithTag);
				BlockAbilitiesWithTag.AppendTags(Preset.BlockAbilitiesWithTag);
				ActivationOwnedTags.AppendTags(Preset.ActivationBlockedTags);
				ActivationRequiredTags.AppendTags(Preset.ActivationRequiredTags);
				ActivationBlockedTags.AppendTags(Preset.ActivationBlockedTags);
				SourceRequiredTags.AppendTags(Preset.SourceRequiredTags);
				SourceBlockedTags.AppendTags(Preset.SourceBlockedTags);
				TargetRequiredTags.AppendTags(Preset.TargetRequiredTags);
				TargetBlockedTags.AppendTags(Preset.TargetBlockedTags);
			}
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, TEXT("UGB_GameplayAbility() GameInstance nullptr"));
		}
	}
}

void UGB_GameplayAbility::EndAbility(const FGameplayAbilitySpecHandle Handle,
                                     const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
                                     bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);

	OnAbilityEnded.Broadcast();
}

bool UGB_GameplayAbility::CommitAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo)
{
	if(!Super::CommitAbility(Handle, ActorInfo, ActivationInfo))
	{
		return false;
	}

	OwningCharacter = Cast<AGBCharacterBase>(ActorInfo->AvatarActor);
	if(!OwningCharacter)
	{
		return false;
	}

	return true;
}


void UGB_GameplayAbility::OnGiveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	 UGB_GameplayAbility* MyAbility = Cast<UGB_GameplayAbility>(Spec.Ability);
	 if(MyAbility)
	 {
	 	MyAbility->SetupTags(ActorInfo->AvatarActor->GetWorld());
	 }
	
	Super::OnGiveAbility(ActorInfo, Spec);
}

void UGB_GameplayAbility::PreActivate(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
	FOnGameplayAbilityEnded::FDelegate* OnGameplayAbilityEndedDelegate)
{

	SetupTags(ActorInfo->AvatarActor->GetWorld());
	
	Super::PreActivate(Handle, ActorInfo, ActivationInfo, OnGameplayAbilityEndedDelegate);
}

