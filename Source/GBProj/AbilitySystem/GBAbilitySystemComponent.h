// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "GB_GameplayAbility.h"
#include "GBProj/Func/Types.h"
#include "GBAbilitySystemComponent.generated.h"

// DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHandleDamageHit, FDamageHitInfo, Info);
// DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDamageHitReceived, UResultHitInfo*, ReturnInfo);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDamageTaken, float, DamageTaken, float, UnmitigatedDamage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStunned, float, StunDuration);
/**
 * 
 */
UCLASS()
class GBPROJ_API UGBAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()


	public:
	UGBAbilitySystemComponent();

	// UPROPERTY(BlueprintAssignable)
	// FOnHandleDamageHit OnHandleDamageHit;
	//
	// UPROPERTY(BlueprintAssignable)
	// FOnDamageHitReceived OnDamageHitReceived;

	UPROPERTY(BlueprintAssignable)
	FOnDamageTaken OnDamageTaken;

	UPROPERTY(BlueprintAssignable)
	FOnStunned OnStunned;


	UPROPERTY(EditDefaultsOnly)
	FGameplayTagContainer CancelOnStunTags;
	
	
	//UFUNCTION(BlueprintCallable)
	//void TakeDamageHit(FDamageHitInfo Info, UGBAbilitySystemComponent* ASCSource);

	// Called by character when stunned
	void Stunned(float Duration);
	UFUNCTION(BlueprintImplementableEvent)
	void Stunned_BP(float Duration);

	// Logic for receive damage hit. This way we can block it or receive damage;
	UFUNCTION(BlueprintCallable)
	void ReceiveAbilityDamageHit(FDamageHitInfo Info, UGBAbilitySystemComponent* ASCSource);

	// Run ReceiveAbilityDamageHit on target ASC
	UFUNCTION(BlueprintCallable)
	void ApplyAbilityDamageHitToTarget(FDamageHitInfo Info, UGBAbilitySystemComponent* ASCTarget);

	// Check Actor implements AbilitySystem  and Run ReceiveAbilityDamageHit on it
	UFUNCTION(BlueprintCallable)
	void ApplyAbilityDamageHitToActor(FDamageHitInfo Info, const AActor* Target);

	// Apply damage effect to target
	UFUNCTION(BlueprintCallable)
	void ApplyAbilityDamageToTarget(FDamageHitInfo Info, UAbilitySystemComponent* ASCTarget);

	// Create and modify duration of effect and apply it to target, if target nullptr - apply to self.
	// Return true if effect applied
	UFUNCTION(BlueprintCallable)
	bool CreateAndApplyEffectWithModifiedDuration(TSubclassOf<UGameplayEffect> EffectClass, float NewDuration, UAbilitySystemComponent* ASCTarget = nullptr);

	// Called by character on received damage
	UFUNCTION(BlueprintCallable)
	void OnReceiveDamage(float DamageTaken, float UnmitigatedDamage);
	
	UFUNCTION(BlueprintCallable)
	void GetGameplayAbilitiesWithMatchingTags(const FGameplayTagContainer& GameplayTagContainer, TArray<UGB_GameplayAbility*>& ActiveAbilities);
};
