// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "GBProj/Actors/ActorComponents/FlaskComponent.h"

#include "FlaskHealEffect.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API UFlaskHealEffect : public UGameplayEffect
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ChargeConsumePerTick = 5.f;

};
