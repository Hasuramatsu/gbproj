// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "./GB_GameplayAbility.h"
#include "GBAbilitySystemComponent.h"
#include "GBProj/Func/Types.h"
#include "GB_DamageHitAbility.generated.h"

class UAbilityTask_WaitDamageHit;
/**
 * 
 */
UCLASS()
class GBPROJ_API UGB_DamageHitAbility : public UGB_GameplayAbility
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	bool bForceAbilityDamage = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage", meta=(EditCondition="bForceAbilityDamage"))
	float Damage = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage", meta=(EditCondition="!bForceAbilityDamage", ClampMin="0", ClampMax="10"))
	float DamageCoefficient = 1.f;
	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitInfo")
	bool bCanBeBlocked = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitInfo")
	bool bCanBeParried = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitInfo")
	bool bHitOnce = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	TSubclassOf<UGameplayEffect> DamageEffect;

protected:

	UPROPERTY()
	AWeaponBase* ActiveWeapon;
	

	UFUNCTION(BlueprintCallable)
	virtual float CalculateDamage();

	UFUNCTION(BlueprintCallable)
	FDamageHitInfo MakeDamageHitInfo();

	UFUNCTION(BlueprintCallable)
	void ApplyDamageHitToTarget(FDamageHitInfo Info,  UGBAbilitySystemComponent* Target);

	UFUNCTION(BlueprintCallable)
	void ApplyDamageHitToActor(FDamageHitInfo Info, const AActor* TargetASC);

	virtual bool CommitAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) override;
private:


};
