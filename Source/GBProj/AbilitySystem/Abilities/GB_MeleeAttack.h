// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GBProj/AbilitySystem/GB_DamageHitAbility.h"
#include "GB_MeleeAttack.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API UGB_MeleeAttack : public UGB_DamageHitAbility
{
	GENERATED_BODY()

public:

	UGB_MeleeAttack();

	// Attack Properties
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	UAnimMontage* AttackMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	float AttackTime = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties | Tags")
	FGameplayTag EventHitTag;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties | Tags")
	FGameplayTag EventComboTag;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties | Tags")
	FGameplayTag AbilityComboTag;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties | Tags")
	FGameplayTag AbilityIgnoreTag;

	
	// Overriden functions
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const override;

	// virtual void OnHit(const UResultHitInfo* Info) override;
protected:

	// Montage delegate binds
	UFUNCTION()
	void OnCompletedMontage();
	UFUNCTION()
	void OnCancelledMontage();
	UFUNCTION()
	void OnInterruptedMontage();

	// Event Delegate Binds
	UFUNCTION(BlueprintNativeEvent)
	void OnComboEvent(FGameplayEventData Data);

	UFUNCTION(BlueprintNativeEvent)
	void OnDamageEvent(FGameplayEventData Data);

	// AbilityCommit Delegate Binds
	UFUNCTION()
	void OnAbilityCommit(UGameplayAbility* ActivatedAbility);
	
private:

	UPROPERTY()
	class AGBCharacterBase* OwnerCharacter = nullptr;
	
	
};


