// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../GB_GameplayAbility.h"
#include "GB_DashWithAction.generated.h"

class UAbilityTask_ApplyRootMotionConstantForce;
/**
 * 
 */
UCLASS()
class GBPROJ_API UGB_DashWithAction : public UGB_GameplayAbility
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Properites")
	float Strength = 1200.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Properites")
	float Duration = 1.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Properites")
	FName DashCollisionPresetName = FName("Boss");

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Properites")
	UAnimMontage* DashMontage = nullptr;
	
protected:

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

	UPROPERTY(BlueprintReadOnly)
	UAbilityTask_ApplyRootMotionConstantForce* ForceTask;

	UFUNCTION()
	void OnDashFinish();
private:

	
};


UCLASS()
class GBPROJ_API UGB_DashWithDamage : public UGB_DashWithAction
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTag WithTag;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTag WithoutTag;
	
protected:

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
private:

	UFUNCTION()
	void OnNewAbilityCommit(UGameplayAbility* NewAbility);
};