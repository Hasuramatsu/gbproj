// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../GB_GameplayAbility.h"
#include "GB_Drain.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API UGB_Drain : public UGB_GameplayAbility
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties")
	TSubclassOf<UGameplayEffect> DamageEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties")
	TSubclassOf<UGameplayEffect> SuppressEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties")
	float AbilityDuration = 2.f;

	// Damage for full ability duration;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties|Damage")
	float Damage = 50.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties|Damage")
	float NumberOfHits = 5.f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties|Damage")
	UAnimMontage* DamageMontage = nullptr;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties|ShadowWalk")
	UAnimMontage* ShadowWalkMontage = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties|ShadowWalk")
	float ShadowWalkDuration = 0.5f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties|BackJump", meta=(ClampMin="0", ClampMax="100"))
	int32 BackJumpChance = 1.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties|BackJump")
	float BackJumpDistance = 200.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties|BackJump")
	float BackJumpHeight = 50.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties|BackJump")
	float BackJumpDuration = 0.5f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties|BackJump")
	UAnimMontage* BackJumpMontage = nullptr;
	
	
protected:

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
	virtual bool CommitAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) override;
private:

	UPROPERTY()
	const AGBCharacterBase* TargetCharacter = nullptr;

	UPROPERTY()
	TArray<FActiveGameplayEffectHandle> SuppressSpecHandles;

	UPROPERTY()
	TArray<FActiveGameplayEffectHandle> DamageSpecHandles;

	UFUNCTION()
	void StartDrain();

	UFUNCTION()
	void OnDamageMontageEnded();

	UFUNCTION()
	void OnShadowStepComplete();

	UFUNCTION()
	void OnLanded();

	float ActualDuration;
	float ActualDamage;
	int32 ActualNumberOfHits;
};

