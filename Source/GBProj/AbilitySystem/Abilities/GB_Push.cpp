// Fill out your copyright notice in the Description page of Project Settings.


#include "GB_Push.h"

#include "GameplayTagsManager.h"
#include "Abilities/Tasks/AbilityTask_WaitGameplayEvent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GBProj/Actors/MainCharacter.h"
#include "Kismet/KismetMathLibrary.h"


void UGB_Push::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	Activator = (AActor*)TriggerEventData->Instigator;

	MainCharacter = Cast<AMainCharacter>(GetAvatarActorFromActorInfo());

	// Check Character facing object
	FVector SecondVector = Activator->GetActorLocation() - MainCharacter->GetActorLocation();
	SecondVector.Normalize();
	float FloatResult = UKismetMathLibrary::Dot_VectorVector(MainCharacter->GetActorForwardVector(), SecondVector);
	FloatResult = UKismetMathLibrary::DegAcos(FloatResult);
	
	
	if(FloatResult <= 45 && FloatResult >= 0)
	{
		if(!CommitAbility(Handle, ActorInfo, ActivationInfo))
		{
			EndAbility(Handle, ActorInfo, ActivationInfo, false, false);
		}

		UAbilityTask_WaitGameplayEvent* CancelTask = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this, CancelTag);
		CancelTask->EventReceived.AddDynamic(this, &UGB_Push::OnCancelEventReceived);

		CancelTask->ReadyForActivation();

		UAbilityTask_WaitGameplayEvent* GrabTask = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this, GrabTag);
		GrabTask->EventReceived.AddDynamic(this, &UGB_Push::OnGrabEventReceived);

		GrabTask->ReadyForActivation();

		MainCharacter->ForceSetInteractable(Activator);

		const FAttachmentTransformRules Rules{EAttachmentRule::KeepWorld, false};

		MainCharacter->AttachToActor(Activator, Rules);

		SetupPushableEvent();

		// Apply Push gameplay effect
		const FGameplayEffectContextHandle Context = MakeEffectContext(Handle, ActorInfo);
		PushEffectHandle = GetAbilitySystemComponentFromActorInfo()->MakeOutgoingSpec(PushEffect, 1, Context);
		FGameplayEffectSpec* Spec = PushEffectHandle.Data.Get();
		if(Spec)
		{
			Spec->SetSetByCallerMagnitude(UGameplayTagsManager::Get().RequestGameplayTag(FName("Data.Speed")), Speed);
		}
		ActivePushEffect = ApplyGameplayEffectSpecToOwner(Handle, ActorInfo, ActivationInfo, PushEffectHandle);
	}
	else
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, false, false);
	}
}

void UGB_Push::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{

	StopPushEvent();
	
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UGB_Push::OnCancelEventReceived(FGameplayEventData EventData)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}

void UGB_Push::OnGrabEventReceived(FGameplayEventData EventData)
{
	GrabEvent();
}


