// Fill out your copyright notice in the Description page of Project Settings.


#include "GB_SpawnDamageProjectile.h"

#include "GameplayTagsManager.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"
#include "Abilities/Tasks/AbilityTask_WaitGameplayEvent.h"
#include "GameFramework/Character.h"
#include "GBProj/Actors/GBCharacterBase.h"
#include "GBProj/Actors/Projectiles/ProjectileBase.h"

class AGBCharacterBase;

void UGB_SpawnDamageProjectile::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
                                                const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
                                                const FGameplayEventData* TriggerEventData)
{

	if(!CommitAbility(Handle, ActorInfo, ActivationInfo))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, false, false);
	}

	float Rate = Montage->GetPlayLength() / CastTime;
	UAbilityTask_PlayMontageAndWait* MontageTask = UAbilityTask_PlayMontageAndWait::CreatePlayMontageAndWaitProxy(this, FName(""), Montage, Rate);
	if(MontageTask)
	{
		MontageTask->OnCompleted.AddDynamic(this, &UGB_SpawnDamageProjectile::OnMontageEnded);
		MontageTask->ReadyForActivation();
	}

	FGameplayTag Tag = UGameplayTagsManager::Get().RequestGameplayTag(FName("Event.Montage.Spawn"));
	UAbilityTask_WaitGameplayEvent* EventTask = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this, Tag);
	if(EventTask)
	{
		EventTask->EventReceived.AddDynamic(this, &UGB_SpawnDamageProjectile::OnEventReceived);
		EventTask->ReadyForActivation();
	}
}

bool UGB_SpawnDamageProjectile::CanActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* GameplayTags,
	const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const
{
	if(!Super::CanActivateAbility(Handle, ActorInfo, GameplayTags, TargetTags, OptionalRelevantTags))
	{
		return false;
	}

	if(!ProjectileClass)
	{
		return false;
	}
	if(!Montage)
	{
		return false;
	}
	if(!Cast<ACharacter>(ActorInfo->AvatarActor))
	{
		return false;
	}
	
	return true;
}

void UGB_SpawnDamageProjectile::EndAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
	bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UGB_SpawnDamageProjectile::OnMontageEnded()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}

void UGB_SpawnDamageProjectile::OnEventReceived(FGameplayEventData Data)
{
	UMeshComponent* Mesh = Cast<ACharacter>(GetAvatarActorFromActorInfo())->GetMesh();
	
	FVector Start = Mesh->GetSocketLocation(SocketName);
	Start.X = 0;
	FVector End;

	//TODO TargetData class
	AGBCharacterBase* Character = Cast<AGBCharacterBase>(GetAvatarActorFromActorInfo());
	if(Character)
	{
		End = Character->GetObjectLocation();
	}
	else
	{
		End = Start + GetAvatarActorFromActorInfo()->GetActorForwardVector() * 1000;
	}

	FVector Direction = End - Start;
	Direction.Normalize();
	const FMatrix MyMatrix(Direction, FVector::ZeroVector, FVector(0, 0, 1), FVector(1, 0, 0));
	FRotator SpawnRotation = MyMatrix.Rotator();

	
	// if(Mesh->GetSocketRotation(SocketName).Yaw > 0)
	// {
	// 	SpawnRotation.Yaw = 90;
	// }
	// else
	// {
	// 	SpawnRotation.Yaw = -90;
	// }

	FTransform Transform = Mesh->GetSocketTransform(SocketName);
	Transform.SetRotation(SpawnRotation.Quaternion());
	Transform.SetScale3D(FVector(1.f));

	APawn* Owner = Cast<APawn>(GetAvatarActorFromActorInfo());

	AProjectileBase* Projectile = GetWorld()->SpawnActorDeferred<AProjectileBase>(ProjectileClass, Transform, GetOwningActorFromActorInfo(), Owner, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

	if(Projectile)
	{
		if(bOverrideProjectileProperties)
		{
			Projectile->SetLifeSpan(LifeSpan);
			Projectile->GetMovementComponent()->InitialSpeed = ProjectileInitSpeed;
		}

		Projectile->SetDamageHitInfo(MakeDamageHitInfo());
		Projectile->SetOwner(GetAvatarActorFromActorInfo());
		Projectile->FinishSpawning(Transform);

	}
	
}
	
	

