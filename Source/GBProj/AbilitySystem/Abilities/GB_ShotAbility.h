// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../GB_GameplayAbility.h"
#include "GBProj/AbilitySystem/GB_DamageHitAbility.h"
#include "GBProj/Actors/Projectiles/ProjectileBase.h"
#include "GBProj/Actors/Weapon/RangedWeaponBase.h"

#include "GB_ShotAbility.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API UGB_ShotAbility : public UGB_DamageHitAbility
{
	GENERATED_BODY()


public:
	// Constructor
	UGB_ShotAbility();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage* ShotMontage;
	
	//
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const override;

	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

	virtual void CancelAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateCancelAbility) override;
protected:

	UFUNCTION()
	void OnCancelled(FGameplayTag EventTag, FGameplayEventData EventData);

	UFUNCTION()
	void OnCompleted(FGameplayTag EventTag, FGameplayEventData EventData);

	UFUNCTION()
	void EventReceived(FGameplayEventData EventData);

	
private:

	//
	UPROPERTY()
	ARangedWeaponBase* RangedWeapon;
	
	//
	TSubclassOf<AProjectileBase> ProjectileClass;
	
	
};

