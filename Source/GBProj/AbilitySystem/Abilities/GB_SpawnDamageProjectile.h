// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../GB_DamageHitAbility.h"
#include "GB_SpawnDamageProjectile.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API UGB_SpawnDamageProjectile : public UGB_DamageHitAbility
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Properties")
	UAnimMontage* Montage = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Properties")
	float CastTime = 1.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Properties")
	FName SocketName = FName("WeaponSocket");

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Properties|Projectile")
	TSubclassOf<AProjectileBase> ProjectileClass;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Properties|Projectile")
	bool bOverrideProjectileProperties = true;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Properties|Projectile", meta= (EditCondition = "bOverrideProjectileProperties"))
	float LifeSpan = 3.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Properties|Projectile", meta= (EditCondition = "bOverrideProjectileProperties"))
	float ProjectileInitSpeed = 1000.f;
	
	
	
protected:

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
	
private:
	
	UFUNCTION()
	void OnMontageEnded();

	UFUNCTION()
	void OnEventReceived(FGameplayEventData Data);

	// UFUNCTION()
	// void RemoveProjectile(AActor* Proj);
	//
	// virtual void OnHit(const UResultHitInfo* Info) override;
	// UPROPERTY()
	// TArray<AProjectileBase*> ActiveProjectiles;
};
