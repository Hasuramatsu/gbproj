// Fill out your copyright notice in the Description page of Project Settings.


#include "GB_MeleeAttack.h"

#include "GameplayTagsManager.h"
#include "Abilities/GameplayAbilityTargetActor_Trace.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"
#include "Abilities/Tasks/AbilityTask_WaitAbilityCommit.h"
#include "Abilities/Tasks/AbilityTask_WaitGameplayEvent.h"
#include "GBProj/AbilitySystem/Tasks/AbilityTask_WaitDamageHit.h"
#include "GBProj/Actors/GBCharacterBase.h"

UGB_MeleeAttack::UGB_MeleeAttack()
{
	EventHitTag = UGameplayTagsManager::Get().RequestGameplayTag(TEXT("Event.Hit.Damage"));
	EventComboTag = UGameplayTagsManager::Get().RequestGameplayTag(TEXT("Event.Montage.Strike"));
	AbilityComboTag = UGameplayTagsManager::Get().RequestGameplayTag(TEXT("Attack.Melee"));
}


void UGB_MeleeAttack::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
                                      const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
                                      const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	if(!CommitAbility(Handle, ActorInfo, ActivationInfo))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, true);
		return;
	}

	OwnerCharacter = Cast<AGBCharacterBase>(ActorInfo->AvatarActor);
	if(OwnerCharacter)
	{
		
		OwnerCharacter->SetCanAttack(false);

		if(AttackMontage)
		{
			float PlayRate = AttackMontage->GetPlayLength() / AttackTime;
			UAbilityTask_PlayMontageAndWait* MontageTask = UAbilityTask_PlayMontageAndWait::CreatePlayMontageAndWaitProxy(this, NAME_None, AttackMontage, PlayRate, NAME_None, true);

			if(MontageTask)
			{
				MontageTask->OnCompleted.AddDynamic(this, &UGB_MeleeAttack::OnCompletedMontage);
				MontageTask->OnCancelled.AddDynamic(this, &UGB_MeleeAttack::OnCancelledMontage);
				MontageTask->OnInterrupted.AddDynamic(this, &UGB_MeleeAttack::OnInterruptedMontage);
				
				//StartWaitDamage();

				UAbilityTask_WaitGameplayEvent* DamageTask = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this, EventHitTag);
				if(DamageTask)
				{
					DamageTask->EventReceived.AddDynamic(this, &UGB_MeleeAttack::UGB_MeleeAttack::OnDamageEvent);

					DamageTask->ReadyForActivation();
				}
				
				UAbilityTask_WaitGameplayEvent* ComboTask = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this, EventComboTag);
				if(ComboTask)
				{
					ComboTask->EventReceived.AddDynamic(this, &UGB_MeleeAttack::OnComboEvent);

					ComboTask->ReadyForActivation();
				}
				
				MontageTask->ReadyForActivation();
			}
		}
		else
		{
			EndAbility(Handle, ActorInfo, ActivationInfo, false, true);
		}
		
		
	}
}

void UGB_MeleeAttack::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

bool UGB_MeleeAttack::CanActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* GameplayTags,
	const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const
{
	if(!Super::CanActivateAbility(Handle, ActorInfo, GameplayTags, TargetTags, OptionalRelevantTags))
	{
		return false;
	}

	if(!Cast<AGBCharacterBase>(ActorInfo->AvatarActor))
	{
		return false;
	}

	return true;
}

void UGB_MeleeAttack::OnCompletedMontage()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}

void UGB_MeleeAttack::OnCancelledMontage()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, true);
}

void UGB_MeleeAttack::OnInterruptedMontage()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, true);
}



void UGB_MeleeAttack::OnComboEvent_Implementation(FGameplayEventData Data)
{
	if(OwnerCharacter)
	{
		OwnerCharacter->SetCanAttack(true);

		UAbilityTask_WaitAbilityCommit* Task = UAbilityTask_WaitAbilityCommit::WaitForAbilityCommit(this, AbilityComboTag, AbilityIgnoreTag);
		if(Task)
		{
			Task->OnCommit.AddDynamic(this, &UGB_MeleeAttack::OnAbilityCommit);

			Task->ReadyForActivation();
		}
	}
}

void UGB_MeleeAttack::OnDamageEvent_Implementation(FGameplayEventData Data)
{
	ApplyDamageHitToActor(MakeDamageHitInfo(), Data.Target);
}

void UGB_MeleeAttack::OnAbilityCommit(UGameplayAbility* ActivatedAbility)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}
