// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../GB_GameplayAbility.h"
#include "GBProj/Actors/MainCharacter.h"
#include "GB_Push.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API UGB_Push : public UGB_GameplayAbility
{
	GENERATED_BODY()


public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Setup")
	FGameplayTag CancelTag;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Setup")
	FGameplayTag GrabTag;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Setup")
	TSubclassOf<UGameplayEffect> PushEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Setup")
	TSubclassOf<UGameplayEffect> GrabEffectEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Setup")
	float Speed;
	
protected:
	
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

	UFUNCTION(BlueprintImplementableEvent)
	void SetupPushableEvent();
	UFUNCTION(BlueprintImplementableEvent)
	void StopPushEvent();
	UFUNCTION(BlueprintImplementableEvent)
	void GrabEvent();
	
	UPROPERTY(VisibleAnywhere ,BlueprintReadOnly)
	AMainCharacter* MainCharacter = nullptr;

	UPROPERTY(VisibleAnywhere ,BlueprintReadOnly)
	AActor* Activator = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FActiveGameplayEffectHandle ActivePushEffect;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FGameplayEffectSpecHandle PushEffectHandle;

private:
	
	UFUNCTION()
	void OnCancelEventReceived(FGameplayEventData EventData);

	UFUNCTION()
	void OnGrabEventReceived(FGameplayEventData EventData);
};
