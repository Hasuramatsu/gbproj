// Fill out your copyright notice in the Description page of Project Settings.


#include "GB_DashWithAction.h"

#include "Abilities/Tasks/AbilityTask_ApplyRootMotionConstantForce.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"
#include "Abilities/Tasks/AbilityTask_WaitAbilityCommit.h"
#include "Abilities/Tasks/AbilityTask_WaitGameplayEvent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "GBProj/Actors/GBCharacterBase.h"

void UGB_DashWithAction::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
                                         const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
                                         const FGameplayEventData* TriggerEventData)
{
	if(!CommitAbility(Handle, ActorInfo, ActivationInfo))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, false, false);
	}

	OwningCharacter->GetCapsuleComponent()->SetCollisionProfileName(FName(DashCollisionPresetName));

	const float ClampSpeed = OwningCharacter->GetMovementComponent()->GetMaxSpeed();

	
	FVector Direction = OwningCharacter->GetActorForwardVector();
	if(bUseOnTarget)
	{
		Direction = OwningCharacter->GetObjectLocation() - OwningCharacter->GetActorLocation();
		Direction.Normalize();
	}
	
	ForceTask = UAbilityTask_ApplyRootMotionConstantForce::ApplyRootMotionConstantForce(this, FName(""), Direction, Strength, Duration, false, nullptr, ERootMotionFinishVelocityMode::ClampVelocity, FVector::ZeroVector, ClampSpeed);

	if(ForceTask)
	{
		ForceTask->OnFinish.AddDynamic(this, &UGB_DashWithAction::OnDashFinish);

		ForceTask->ReadyForActivation();
	}

	if(DashMontage)
	{
		const float PlayRate = DashMontage->GetPlayLength() / Duration;
		UAbilityTask_PlayMontageAndWait* MontageTask = UAbilityTask_PlayMontageAndWait::CreatePlayMontageAndWaitProxy(this, FName(""), DashMontage, PlayRate);

		if(MontageTask)
		{
			MontageTask->ReadyForActivation();
		}
	}
}

void UGB_DashWithAction::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);

	OwningCharacter->ResetCollisionToBase();
}

void UGB_DashWithAction::OnDashFinish()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}

void UGB_DashWithDamage::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	UAbilityTask_WaitAbilityCommit* WaitCommitTask = UAbilityTask_WaitAbilityCommit::WaitForAbilityCommit(this, WithTag, WithoutTag);
	if(WaitCommitTask)
	{
		WaitCommitTask->OnCommit.AddDynamic(this, &UGB_DashWithDamage::OnNewAbilityCommit);

		WaitCommitTask->ReadyForActivation();
	}
}

void UGB_DashWithDamage::OnNewAbilityCommit(UGameplayAbility* NewAbility)
{
	if(ForceTask)
	{
		ForceTask->EndTask();
	}
	if(UGB_GameplayAbility* GBAbility = Cast<UGB_GameplayAbility>(NewAbility))
	{
		GBAbility->OnAbilityEnded.AddDynamic(this, &UGB_DashWithDamage::OnDashFinish);
	}
	else
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
	}
}
