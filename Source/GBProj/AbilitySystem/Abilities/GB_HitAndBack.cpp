// Fill out your copyright notice in the Description page of Project Settings.


#include "GB_HitAndBack.h"

#include "Abilities/Tasks/AbilityTask_ApplyRootMotionRadialForce.h"
#include "GameFramework/PawnMovementComponent.h"
#include "GBProj/Actors/GBCharacterBase.h"

void UGB_HitAndBack::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
                                     const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
                                     const FGameplayEventData* TriggerEventData)
{
	if(!CommitAbility(Handle, ActorInfo, ActivationInfo))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, false, false);	
	}

	OwningCharacter->GetMovementComponent()->StopMovementImmediately();
	
	ApplyDamageHitToActor(MakeDamageHitInfo(), TriggerEventData->Target);

	const FVector RadialLocation = TriggerEventData->Target->GetActorLocation();
		
	UAbilityTask_ApplyRootMotionRadialForce* ForceTask = UAbilityTask_ApplyRootMotionRadialForce::ApplyRootMotionRadialForce(this, FName(""), 
	RadialLocation, nullptr,
	Force, Duration, Distance, true, true, false,
	nullptr, nullptr, false,
	FRotator::ZeroRotator, ERootMotionFinishVelocityMode::MaintainLastRootMotionVelocity,
	FVector::ZeroVector, 0.f);
	if(ForceTask)
	{
		ForceTask->OnFinish.AddDynamic(this, &UGB_HitAndBack::OnForceFinish);

		ForceTask->ReadyForActivation();
	}
}

void UGB_HitAndBack::OnForceFinish()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}
