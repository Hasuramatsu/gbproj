// Fill out your copyright notice in the Description page of Project Settings.


#include "GB_ShotAbility.h"

#include "GameplayTagsManager.h"
#include "GBProj/Actors/GBCharacterBase.h"
#include "Abilities/Tasks/AbilityTask_WaitGameplayEvent.h"

UGB_ShotAbility::UGB_ShotAbility()
{
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;

	FGameplayTag AbilityTag = UGameplayTagsManager::Get().RequestGameplayTag(FName("Ability.Weapon.Shooting"));
	AbilityTags.AddTag(AbilityTag);
	ActivationOwnedTags.AddTag(AbilityTag);

	//ActivationBlockedTags.AddTag(FGameplayTag::RequestGameplayTag(FName("")));
}

void UGB_ShotAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
                                      const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
                                      const FGameplayEventData* TriggerEventData)
{

	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
	
	if(!CommitAbility(Handle, ActorInfo, ActivationInfo))
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
	}

	// Skip check cuz we already check it in CanActivateAbility()
	RangedWeapon = Cast<ARangedWeaponBase>(Cast<AGBCharacterBase>(ActorInfo->AvatarActor)->GetCurrentWeapon());
	
	ProjectileClass = RangedWeapon->RangedWeaponInfo.Projectile;

	

	UAbilityTask_WaitGameplayEvent* Task = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this, UGameplayTagsManager::Get().RequestGameplayTag(FName("Event.Weapon.Shot")));
	Task->EventReceived.AddDynamic(this, &UGB_ShotAbility::EventReceived);

	Task->ReadyForActivation();

	RangedWeapon->SetIsFiring(true);
}

bool UGB_ShotAbility::CanActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags,
	const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const
{
	if(!Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags))
	{
		return  false;
	}

	 AGBCharacterBase* Character = Cast<AGBCharacterBase>(ActorInfo->AvatarActor);
	if(Character)
	{
		ARangedWeaponBase* RangeWeapon = Cast<ARangedWeaponBase>(Character->GetCurrentWeapon());
		if(RangeWeapon)
		{
			
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;	
	}
	
	return true;
}

void UGB_ShotAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	if(RangedWeapon)
	{
		RangedWeapon->SetIsFiring(false);
	}
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UGB_ShotAbility::CancelAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateCancelAbility)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
}

void UGB_ShotAbility::OnCancelled(FGameplayTag EventTag, FGameplayEventData EventData)
{
}

void UGB_ShotAbility::OnCompleted(FGameplayTag EventTag, FGameplayEventData EventData)
{
}

void UGB_ShotAbility::EventReceived(FGameplayEventData EventData)
{
	if(!RangedWeapon)
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
	}

	FVector Start = RangedWeapon->ShootLocation->GetComponentLocation();
	Start.X = 0;
	FVector End;

	//TODO TargetData class
	AGBCharacterBase* Character = Cast<AGBCharacterBase>(GetAvatarActorFromActorInfo());
	if(Character)
	{
		End = Character->GetObjectLocation();
	}
	else
	{
		End = Start + RangedWeapon->GetActorForwardVector() * 1000;
	}

	FVector Direction = End - Start;
	Direction.Normalize();
	const FMatrix MyMatrix(Direction, FVector::ZeroVector, FVector(0, 0, 1), FVector(1, 0, 0));
	FRotator SpawnRotation = MyMatrix.Rotator();

	
	if(RangedWeapon->ShootLocation->GetComponentRotation().Yaw > 0)
	{
		SpawnRotation.Yaw = 90;
	}
	else
	{
		SpawnRotation.Yaw = -90;
	}

	FTransform Transform = RangedWeapon->ShootLocation->GetComponentTransform();
	Transform.SetRotation(SpawnRotation.Quaternion());
	Transform.SetScale3D(FVector(1.f));

	APawn* Owner = Cast<APawn>(GetAvatarActorFromActorInfo());

	AProjectileBase* Projectile = GetWorld()->SpawnActorDeferred<AProjectileBase>(ProjectileClass, Transform, GetOwningActorFromActorInfo(), Owner, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

	Projectile->SetDamageHitInfo(MakeDamageHitInfo());
	Projectile->FinishSpawning(Transform);

	if(ShotMontage)
	{
		ACharacter* BaseCharacter = Cast<ACharacter>(GetAvatarActorFromActorInfo());
		if(BaseCharacter)
		{
			BaseCharacter->PlayAnimMontage(ShotMontage);
		}
	}
	
}
