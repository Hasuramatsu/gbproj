// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../GB_DamageHitAbility.h"
#include "GB_HitAndBack.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API UGB_HitAndBack : public UGB_DamageHitAbility
{
	GENERATED_BODY()

public:

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BackMovement")
	float Force = 1500.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BackMovement")
	float Duration = 1.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BackMovement")
	float Distance = 450.f;
	
	
private:

	UFUNCTION()
	void OnForceFinish();
	
	
};
