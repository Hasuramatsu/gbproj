// Fill out your copyright notice in the Description page of Project Settings.


#include "GB_Drain.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "GameplayTagsManager.h"
#include "Abilities/Tasks/AbilityTask_ApplyRootMotionJumpForce.h"
#include "Abilities/Tasks/AbilityTask_ApplyRootMotionMoveToForce.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"
#include "Components/CapsuleComponent.h"
#include "GBProj/AbilitySystem/Tasks/AbilityTask_RotateCharacter.h"
#include "GBProj/Actors/GBCharacterBase.h"
#include "GBProj/Func/GBLib.h"
#include "Kismet/KismetSystemLibrary.h"

void UGB_Drain::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
                                const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{

	TargetCharacter = Cast<AGBCharacterBase>(TriggerEventData->Target);
	
	if(!CommitAbility(Handle, ActorInfo, ActivationInfo))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, false, false);
	}

	ActualDuration = AbilityDuration;
	ActualNumberOfHits = NumberOfHits;
	ActualDamage = Damage;

	if(SuppressEffect)
	{
		FGameplayEffectContextHandle EffectContext = MakeEffectContext(Handle, ActorInfo);
		FGameplayEffectSpecHandle EffectHandle = MakeOutgoingGameplayEffectSpec(SuppressEffect, 1);
		FGameplayEffectSpec* EffectSpec = EffectHandle.Data.Get();
		if(EffectSpec)
		{
			FGameplayTag Tag = UGameplayTagsManager::Get().RequestGameplayTag(FName("Data.Duration"));
			EffectSpec->SetSetByCallerMagnitude(Tag, AbilityDuration);
		}

		FGameplayAbilityTargetDataHandle TargetDataHandle = UGBLib::MakeTargetDataFromActor(TargetCharacter->GetInstigator());

		
		SuppressSpecHandles = ApplyGameplayEffectSpecToTarget(Handle, ActorInfo, ActivationInfo, EffectHandle, TargetDataHandle);
	}
	
	if(UGBLib::IsFacing(TargetCharacter, OwningCharacter))
	{
		//TODO Got to target's back
		ActualDuration = AbilityDuration - ShadowWalkDuration;
		ActualNumberOfHits = static_cast<int32>(NumberOfHits / AbilityDuration * ActualDuration);

		float PositionOffset = TargetCharacter->GetCapsuleComponent()->GetScaledCapsuleRadius() + OwningCharacter->GetCapsuleComponent()->GetScaledCapsuleRadius();
		FVector TargetLocation = TargetCharacter->GetActorLocation() + TargetCharacter->GetActorForwardVector() * -1 * PositionOffset;
		TargetLocation.Z = OwningCharacter->GetActorLocation().Z;
		bool bLocationValid = false;

		float Radius = OwningCharacter->GetCapsuleComponent()->GetScaledCapsuleRadius();
		float HalfHeight = OwningCharacter->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
		FName Profile = OwningCharacter->GetCapsuleComponent()->GetCollisionProfileName();
		TArray<AActor*> IgnoreActors;
		FHitResult HitResult;
		
		UKismetSystemLibrary::CapsuleTraceSingleByProfile(GetWorld(), OwningCharacter->GetActorLocation(),
			TargetLocation, Radius, HalfHeight, Profile, false, IgnoreActors, EDrawDebugTrace::None,
			HitResult, true);

		if(!HitResult.IsValidBlockingHit())
		{
			FVector GroundTraceLocation = HitResult.TraceEnd - FVector{0.f, 0.f, 15.f};
			UKismetSystemLibrary::CapsuleTraceSingleByProfile(GetWorld(), HitResult.TraceEnd,
			GroundTraceLocation, Radius, HalfHeight, Profile, false, IgnoreActors, EDrawDebugTrace::None,
			HitResult, true);
			if(HitResult.IsValidBlockingHit())
			{
				bLocationValid = true;
			}
		}
		
		if(bLocationValid)
		{
			UAbilityTask_ApplyRootMotionMoveToForce* MoveTask = UAbilityTask_ApplyRootMotionMoveToForce::ApplyRootMotionMoveToForce(this,
			FName(""), TargetLocation, ShadowWalkDuration, false, EMovementMode::MOVE_Walking,
			false, nullptr, ERootMotionFinishVelocityMode::ClampVelocity, FVector::ZeroVector, 0.f);
			if(MoveTask)
			{
				MoveTask->OnTimedOutAndDestinationReached.AddDynamic(this, &UGB_Drain::OnShadowStepComplete);

				if(ShadowWalkMontage)
				{
					float PlayRate = ShadowWalkMontage->GetPlayLength() / ShadowWalkDuration;
					UAbilityTask_PlayMontageAndWait* MoveMontage = UAbilityTask_PlayMontageAndWait::CreatePlayMontageAndWaitProxy(this, FName(""), ShadowWalkMontage, PlayRate);
					if(MoveMontage)
					{
						MoveMontage->ReadyForActivation();
					}
				}

				UAbilityTask_RotateCharacter* RotateTask = UAbilityTask_RotateCharacter::RotateCharacter(this, ShadowWalkDuration);
				if(RotateTask)
				{
					RotateTask->ReadyForActivation();
				}

				MoveTask->ReadyForActivation();
			}
		}
		else
		{
			ActualDamage /= 2;
			ActualDuration /= 2;
			StartDrain();
		}
	}
	else
	{
		StartDrain();
	}


}

void UGB_Drain::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);

	for(auto& It : SuppressSpecHandles)
	{
		if(It.IsValid())
		{
			TargetCharacter->AbilitySystemComponent->RemoveActiveGameplayEffect(It);
		}
	}
	SuppressSpecHandles.Empty();
	
	for(auto& It : DamageSpecHandles)
	{
		if(It.IsValid())
		{
			TargetCharacter->AbilitySystemComponent->RemoveActiveGameplayEffect(It);
		}
	}

	DamageSpecHandles.Empty();
}


bool UGB_Drain::CommitAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
                              const FGameplayAbilityActivationInfo ActivationInfo)
{
	if(!Super::CommitAbility(Handle, ActorInfo, ActivationInfo))
	{
		return false;
	}
	if(!TargetCharacter)
	{
		return false;
	}

	return true;	
}


void UGB_Drain::StartDrain()
{
	if(DamageEffect)
	{
		if(DamageMontage)
		{
			float PlayRate = DamageMontage->GetPlayLength() / ActualDuration;
			UAbilityTask_PlayMontageAndWait* DamageTask = UAbilityTask_PlayMontageAndWait::CreatePlayMontageAndWaitProxy(this, FName(""), DamageMontage, PlayRate);
			if(DamageTask)
			{
				DamageTask->OnCompleted.AddDynamic(this, &UGB_Drain::OnDamageMontageEnded);

				DamageTask->ReadyForActivation();
			}
		}
			
		FGameplayEffectContextHandle EffectContext = MakeEffectContext(CurrentSpecHandle, CurrentActorInfo);
		FGameplayEffectSpecHandle EffectHandle = MakeOutgoingGameplayEffectSpec(DamageEffect, 1);
		FGameplayEffectSpec* EffectSpec = EffectHandle.Data.Get();
		if(EffectSpec)
		{
			FGameplayTag DurationTag = UGameplayTagsManager::Get().RequestGameplayTag(FName("Data.Duration"));
			FGameplayTag DamageTag = UGameplayTagsManager::Get().RequestGameplayTag(FName("Data.Damage"));
			EffectSpec->SetSetByCallerMagnitude(DurationTag, ActualDuration);
			EffectSpec->SetSetByCallerMagnitude(DamageTag, ActualDamage / ActualNumberOfHits);
			EffectSpec->Period = ActualDuration / ActualNumberOfHits;
		}
		
		FGameplayAbilityTargetDataHandle TargetDataHandle = UGBLib::MakeTargetDataFromActor(TargetCharacter->GetInstigator());

		
		DamageSpecHandles = ApplyGameplayEffectSpecToTarget(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, EffectHandle, TargetDataHandle);
	}
	else
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
	}
}

void UGB_Drain::OnDamageMontageEnded()
{
	
	if(FMath::RandRange(0, 100) <= BackJumpChance)
	{
		UAbilityTask_ApplyRootMotionJumpForce* BackJumpTask = UAbilityTask_ApplyRootMotionJumpForce::ApplyRootMotionJumpForce(this, 
	FName(""), UGBLib::GetReverseRotation(OwningCharacter->GetActorRotation()),
	BackJumpDistance, BackJumpHeight, BackJumpDuration, 0.5f, true, ERootMotionFinishVelocityMode::ClampVelocity,
	FVector::ZeroVector, 0.f, nullptr, nullptr);
		if(BackJumpTask)
		{
			BackJumpTask->OnLanded.AddDynamic(this, &UGB_Drain::OnLanded);

			if(BackJumpMontage)
			{
				float PlayRate = BackJumpMontage->GetPlayLength() / BackJumpDuration;
				UAbilityTask_PlayMontageAndWait* BJMontageTask = UAbilityTask_PlayMontageAndWait::CreatePlayMontageAndWaitProxy(this, FName(""), BackJumpMontage, PlayRate);
				if(BJMontageTask)
				{
					BJMontageTask->ReadyForActivation();
				}
			}
		
			BackJumpTask->ReadyForActivation();
		}
	}
	else
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
	}
}

void UGB_Drain::OnShadowStepComplete()
{
	StartDrain();
}

void UGB_Drain::OnLanded()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}
