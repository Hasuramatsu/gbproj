// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityTask_WaitDamageHit.h"

#include "GBProj/AbilitySystem/GBAbilitySystemComponent.h"

UAbilityTask_WaitDamageHit::UAbilityTask_WaitDamageHit()
{
	TriggerOnce = false;
}

void UAbilityTask_WaitDamageHit::Activate()
{
	Super::Activate();

	// UGBAbilitySystemComponent* ASC = Cast<UGBAbilitySystemComponent>(AbilitySystemComponent);
	//
	// if(ASC)
	// {
	// 	ASC->OnHandleDamageHit.AddDynamic(this, &UAbilityTask_WaitDamageHit::HitHandled);
	// }
}

// void UAbilityTask_WaitDamageHit::HitHandled(FDamageHitInfo Info)
// {
// 	if(ShouldBroadcastAbilityTaskDelegates())
// 	{
// 		OnHitHandled.Broadcast(Info);
// 	}
//
// 	if(TriggerOnce)
// 	{
// 		EndTask();
// 	}
// }

UAbilityTask_WaitDamageHit* UAbilityTask_WaitDamageHit::WaitDamageHit(UGameplayAbility* OwningAbility, bool InTriggerOnce)
{
	UAbilityTask_WaitDamageHit* MyObj = NewAbilityTask<UAbilityTask_WaitDamageHit>(OwningAbility);
	MyObj->TriggerOnce = InTriggerOnce;
	return MyObj;
}


void UAbilityTask_WaitDamageHit::OnDestroy(bool bInOwnerFinished)
{
	
	// UGBAbilitySystemComponent* ASC = Cast<UGBAbilitySystemComponent>(AbilitySystemComponent);
	//
	// if(ASC)
	// {
	// 	ASC->OnHandleDamageHit.RemoveDynamic(this, &UAbilityTask_WaitDamageHit::HitHandled);
	// }

	Super::OnDestroy(bInOwnerFinished);
}