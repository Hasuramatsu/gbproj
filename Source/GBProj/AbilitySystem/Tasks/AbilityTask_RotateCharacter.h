// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "AbilityTask_RotateCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRotationFinished);
/**
 * 
 */
UCLASS()
class GBPROJ_API UAbilityTask_RotateCharacter : public UAbilityTask
{
	GENERATED_BODY()

public:
	UAbilityTask_RotateCharacter();

	UPROPERTY(BlueprintAssignable)
	FOnRotationFinished OnRotationFinished;
	
	UFUNCTION(BlueprintCallable, Category="Ability|Tasks", meta=(HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "TRUE"))
	static UAbilityTask_RotateCharacter* RotateCharacter(UGameplayAbility* OwningAbility, float RotationTime);

private:
	bool bIsFinished = false;

	float RotationRate = 1.f;

	
	virtual void TickTask(float DeltaTime) override;
};
