// Fill out your copyright notice in the Description page of Project Settings.


#include "FindClimbLocation.h"

#include "Components/CapsuleComponent.h"
#include "GameFramework/Character.h"

UFindClimbLocation::UFindClimbLocation(const FObjectInitializer& ObjectInitializer)
	:	Super(ObjectInitializer)
{
	bTickingTask = true;
	bSimulatedTask = true;
	bIsFinished = false;
}

void UFindClimbLocation::TickTask(float DeltaTime)
{
	if(bIsFinished)
	{
		return;	
	}
	
	Super::TickTask(DeltaTime);

	AActor* MyActor = GetAvatarActor();
	if(MyActor)
	{
		ACharacter* MyCharacter = Cast<ACharacter>(MyActor);
		if(MyCharacter)
		{
			FHitResult HitResult;
			FVector Start = MyCharacter->GetActorLocation();
			FVector End;
			int k = 0;	
			while(!bIsFinished && k < 10)
			{
				Start += FVector(0, 0, 10);
				End = Start + (MyCharacter->GetActorForwardVector() * 90);
				k++;
				GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECC_GameTraceChannel4);
				if(HitResult.bBlockingHit)
				{
					FVector SecondEnd = HitResult.Location + (MyCharacter->GetActorForwardVector() * 10);
					FVector SecondStart = SecondEnd + FVector(0,0, MyCharacter->GetCapsuleComponent()->GetScaledCapsuleHalfHeight() / 2);
					GetWorld()->LineTraceSingleByChannel(HitResult, SecondStart, SecondEnd, ECC_GameTraceChannel4);
					if(HitResult.IsValidBlockingHit())
					{
						bIsFinished = true;

						FGameplayAbilityTargetData_SingleTargetHit* TargetData = new FGameplayAbilityTargetData_SingleTargetHit(HitResult);

						FGameplayAbilityTargetDataHandle Handle;
						Handle.Data.Add(TSharedPtr<FGameplayAbilityTargetData>(TargetData));
						
						if(ShouldBroadcastAbilityTaskDelegates())
						{
							OnFindClimbLocation.Broadcast(Handle);
						}

						EndTask();

					}		
				}
			}
		}
	}
}

UFindClimbLocation* UFindClimbLocation::FindClimbLocation(UGameplayAbility* OwningAbility)
{
	UFindClimbLocation* MyObj = NewAbilityTask<UFindClimbLocation>(OwningAbility);
	return MyObj;
}
