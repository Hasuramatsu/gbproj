// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTask.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "Abilities/GameplayAbilityTargetTypes.h"

#include "FindClimbLocation.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnFindClimbLocationDelegate, const FGameplayAbilityTargetDataHandle&, TargetData);
/**
 * 
 */
UCLASS()
class GBPROJ_API UFindClimbLocation : public UAbilityTask
{
	GENERATED_UCLASS_BODY()



	UPROPERTY(BlueprintAssignable)
	FOnFindClimbLocationDelegate OnFindClimbLocation;

	virtual void TickTask(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category="Ability|Tasks", meta=(HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "TRUE"))
	static UFindClimbLocation* FindClimbLocation(UGameplayAbility* OwningAbility);

private:
	bool bIsFinished = false;
};
