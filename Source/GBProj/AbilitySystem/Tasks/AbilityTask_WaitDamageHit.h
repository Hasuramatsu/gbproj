// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "GBProj/Func/Types.h"
#include "AbilityTask_WaitDamageHit.generated.h"


//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWaitDamageHitDelegate, FDamageHitInfo, Info);
/**
 * 
 */
UCLASS()
class GBPROJ_API UAbilityTask_WaitDamageHit : public UAbilityTask
{
	GENERATED_BODY()
	
public:
	UAbilityTask_WaitDamageHit();

	//UPROPERTY(BlueprintAssignable)
	//FWaitDamageHitDelegate OnHitHandled;

	virtual void Activate() override;

	//UFUNCTION()
	//void HitHandled(FDamageHitInfo Info);

	UFUNCTION(BlueprintCallable, Category = "Ability|Tasks", meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "TRUE"))
	static UAbilityTask_WaitDamageHit* WaitDamageHit(UGameplayAbility* OwningAbility, bool InTriggerOnce);

protected:
	
	bool TriggerOnce;
	
	virtual void OnDestroy(bool bInOwnerFinished) override;
};


