// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityTask_RotateCharacter.h"

UAbilityTask_RotateCharacter::UAbilityTask_RotateCharacter()
{
	bTickingTask = true;
	bSimulatedTask = true;
	bIsFinished = false;
}

UAbilityTask_RotateCharacter* UAbilityTask_RotateCharacter::RotateCharacter(UGameplayAbility* OwningAbility, float RotationTime)
{
	UAbilityTask_RotateCharacter* MyObj = NewAbilityTask<UAbilityTask_RotateCharacter>(OwningAbility);
	MyObj->RotationRate = 180 / RotationTime;

	if(MyObj->GetAvatarActor()->GetActorRotation().Yaw > 0)
	{
		MyObj->RotationRate *= -1;
	}
	
	return MyObj;
}

void UAbilityTask_RotateCharacter::TickTask(float DeltaTime)
{
	if(bIsFinished)
	{
		return;	
	}
	
	Super::TickTask(DeltaTime);

	AActor* MyActor = GetAvatarActor();
	if(MyActor)
	{
		float DeltaYaw = DeltaTime * RotationRate;
		//DeltaYaw = TargetRotation.Yaw > 0 ? DeltaYaw : DeltaYaw * -1;

		MyActor->AddActorWorldRotation(FRotator(0, DeltaYaw, 0));

		//float ActorsYaw = MyActor->GetActorRotation().Yaw;
		// MyActor->SetActorRotation(FMath::RInterpTo(MyActor->GetActorRotation(), TargetRotation, DeltaTime, InterpSpeed));
		// if((TargetRotation.Yaw > 0 && ActorsYaw > 0 && ActorsYaw >= TargetRotation.Yaw) || (TargetRotation.Yaw < 0 && ActorsYaw < 0 && ActorsYaw <= TargetRotation.Yaw))
		// {
		// 	MyActor->SetActorRotation(TargetRotation);
		// 	bIsFinished = true;
		// 	OnRotationFinished.Broadcast();
		// 	EndTask();
		// }
		
		if(FMath::Abs(MyActor->GetActorRotation().Yaw) >= 90)
		{
			//MyActor->SetActorRotation(TargetRotation);
			bIsFinished = true;
			OnRotationFinished.Broadcast();
			EndTask();
		}
	}
}
