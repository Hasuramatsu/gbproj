// Fill out your copyright notice in the Description page of Project Settings.


#include "GBAttributeSet.h"
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"
#include "GBProj/Actors/GBCharacterBase.h"
#include "Net/UnrealNetwork.h"


UGBAttributeSet::UGBAttributeSet()
	: Health(1)
	, MaxHealth(1)
	, MovementSpeed(1)
	, Stamina(1)
	, MaxStamina(1)
	, Damage(0)
{
	
}


void UGBAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UGBAttributeSet, Health);
	DOREPLIFETIME(UGBAttributeSet, MaxHealth);
	DOREPLIFETIME(UGBAttributeSet, MovementSpeed);
	DOREPLIFETIME(UGBAttributeSet, Stamina);
	DOREPLIFETIME(UGBAttributeSet, MaxStamina);
	DOREPLIFETIME(UGBAttributeSet, Damage);
	
}

void UGBAttributeSet::OnRep_Health(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGBAttributeSet, Health, OldValue);
}

void UGBAttributeSet::OnRep_MaxHealth(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGBAttributeSet, MaxHealth, OldValue);
}

void UGBAttributeSet::OnRep_MoveSpeed(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGBAttributeSet, MovementSpeed, OldValue);
}

void UGBAttributeSet::OnRep_Stamina(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGBAttributeSet, Stamina, OldValue);
}

void UGBAttributeSet::OnRep_MaxStamina(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGBAttributeSet, MaxStamina, OldValue);
}

void UGBAttributeSet::OnRep_Damage(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGBAttributeSet, Damage, OldValue);
}


void UGBAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	Super::PreAttributeChange(Attribute, NewValue);

	if(Attribute == GetMaxHealthAttribute())
	{
		AdjustAttributeForMaxChange(Health, MaxHealth, NewValue, GetHealthAttribute());
	}
	if(Attribute == GetMaxStaminaAttribute())
	 {
	 	AdjustAttributeForMaxChange(Stamina, MaxStamina, NewValue, GetStaminaAttribute());
	 }
}

void UGBAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
	UAbilitySystemComponent* Source = Context.GetOriginalInstigatorAbilitySystemComponent();
	const FGameplayTagContainer& SourceTags = *Data.EffectSpec.CapturedSourceTags.GetAggregatedTags();

	float DeltaValue = 0;
	if(Data.EvaluatedData.ModifierOp == EGameplayModOp::Type::Additive)
	{
		DeltaValue = Data.EvaluatedData.Magnitude;
	}

	AActor* TargetActor = nullptr;
	AController* TargetController = nullptr;
	AGBCharacterBase* TargetCharacter = nullptr;
	if(Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->AvatarActor.IsValid())
	{
		TargetActor = Data.Target.AbilityActorInfo->AvatarActor.Get();
		TargetController = Data.Target.AbilityActorInfo->PlayerController.Get();
		TargetCharacter = Cast<AGBCharacterBase>(TargetActor);
	}

	if(Data.EvaluatedData.Attribute == GetDamageAttribute())
	{
		AActor* SourceActor = nullptr;
		AController* SourceController = nullptr;
		AGBCharacterBase* SourceCharacter = nullptr;
		if(Source && Source->AbilityActorInfo.IsValid() && Source->AbilityActorInfo->AvatarActor.IsValid())
		{
			SourceActor = Source->AbilityActorInfo->AvatarActor.Get();
			SourceController = Source->AbilityActorInfo->PlayerController.Get();
			if(SourceController == nullptr && SourceActor != nullptr)
			{
				if(APawn* Pawn = Cast<APawn>(SourceActor))
				{
					SourceController = Pawn->GetController();
				}
			}

			if(SourceController)
			{
				SourceCharacter = Cast<AGBCharacterBase>(SourceController->GetPawn());
			}
			else
			{
				SourceCharacter = Cast<AGBCharacterBase>(SourceActor);
			}

			if(Context.GetEffectCauser())
			{
				SourceActor = Context.GetEffectCauser();
			}
		}

		FHitResult HitResult;
		if(Context.GetHitResult())
		{
			HitResult = *Context.GetHitResult();
		}

		const float LocalDamageDone = GetDamage();
		SetDamage(0.f);

		if(TargetCharacter && TargetCharacter->IsAlive() && LocalDamageDone)
		{
			const float OldHealth = GetHealth();
			SetHealth(FMath::Clamp(OldHealth - LocalDamageDone, 0.0f, GetMaxHealth()));
			
					// TODO HandleCharacterDamage

					// FGameplayTagContainer ActorTags;
					// FGameplayTagContainer SpecTags;
					// Context.GetOwnedGameplayTags(ActorTags, SpecTags);
				
			if(SourceTags.HasAny(TargetCharacter->CharacterData.DamageReactionTags))
			{
				OnDamageHitTaken.Broadcast(LocalDamageDone, LocalDamageDone);
			}
			if(!TargetCharacter->IsAlive())
			{
				// TODO On kill logic SourceCharacter->OnKill(TargetCharacter)
			}
		}

	}

	else if(Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		SetHealth(FMath::Clamp(GetHealth(), 0.0f, GetMaxHealth()));
	}
	else if(Data.EvaluatedData.Attribute == GetStaminaAttribute())
	{
		SetStamina(FMath::Clamp(GetStamina(), 0.0f, GetMaxStamina()));
	}
	else if(Data.EvaluatedData.Attribute == GetMovementSpeedAttribute())
	{
		
	}
}


void UGBAttributeSet::AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute,
                                                  const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty)
{
	UAbilitySystemComponent* AbilityComponent = GetOwningAbilitySystemComponent();
	const float CurrentMaxValue = MaxAttribute.GetCurrentValue();
	if(!FMath::IsNearlyEqual(CurrentMaxValue, NewMaxValue) && AbilityComponent)
	{
		const float CurrentValue = AffectedAttribute.GetCurrentValue();
		float NewDelta = (CurrentMaxValue > 0.f) ? (CurrentValue * NewMaxValue / CurrentMaxValue) - CurrentValue : NewMaxValue;

		AbilityComponent->ApplyModToAttributeUnsafe(AffectedAttributeProperty, EGameplayModOp::Additive, NewDelta);
	}
}
