// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "GB_GameplayAbility.generated.h"

class AGBCharacterBase;
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAbilityEnded);
/**
 * 
 */
UCLASS()
class GBPROJ_API UGB_GameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()


public:
	UGB_GameplayAbility();


	UPROPERTY(BlueprintAssignable)
	FOnAbilityEnded OnAbilityEnded;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Debug")
	bool bDebug = false;

	void SetupTags(const UObject* WorldContextObject);

	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
	virtual bool CommitAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) override;

	virtual void OnGiveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;

	virtual void PreActivate(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, FOnGameplayAbilityEnded::FDelegate* OnGameplayAbilityEndedDelegate) override;
protected:

	UPROPERTY(BlueprintReadOnly, Category = "Data")
	AGBCharacterBase* OwningCharacter = nullptr;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Target")
	bool bUseOnTarget = false;

	// Load tags presets with given tags on construction.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Data")
	TArray<FName> TagsPresetsToLoad;
};
