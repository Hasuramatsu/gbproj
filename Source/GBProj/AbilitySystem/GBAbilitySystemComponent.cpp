// Fill out your copyright notice in the Description page of Project Settings.


#include "GBAbilitySystemComponent.h"

#include "AbilitySystemInterface.h"
#include "GameplayTagsManager.h"

UGBAbilitySystemComponent::UGBAbilitySystemComponent()
{
	CancelOnStunTags.AddTag(UGameplayTagsManager::Get().RequestGameplayTag("Ability.Property.CancelOnStun"));
}
//
// void UGBAbilitySystemComponent::HandleDamageHit(FDamageHitInfo Info)
// {
// 	OnHandleDamageHit.Broadcast(Info);
// }


void UGBAbilitySystemComponent::Stunned(float Duration)
{
	CancelAbilities(&CancelOnStunTags);
	OnStunned.Broadcast(Duration);
	Stunned_BP(Duration);
}

void UGBAbilitySystemComponent::ReceiveAbilityDamageHit(FDamageHitInfo Info, UGBAbilitySystemComponent* ASCSource)
{
	if(false)
	{
		//OnDamageHitReceived.Broadcast(ReturnInfo);
	}
	else
	{
		ASCSource->ApplyAbilityDamageToTarget(Info, this);
	}
}

void UGBAbilitySystemComponent::ApplyAbilityDamageHitToTarget(FDamageHitInfo Info, UGBAbilitySystemComponent* ASCTarget)
{
	ASCTarget->ReceiveAbilityDamageHit(Info, this);
}

void UGBAbilitySystemComponent::ApplyAbilityDamageHitToActor(FDamageHitInfo Info, const AActor* Target)
{
	if(const IAbilitySystemInterface* TargetASI = Cast<IAbilitySystemInterface>(Target))
	{
		if(UGBAbilitySystemComponent* TargetASC = Cast<UGBAbilitySystemComponent>(TargetASI->GetAbilitySystemComponent()))
		{
			ApplyAbilityDamageHitToTarget(Info, TargetASC);
		}
	}
}

void UGBAbilitySystemComponent::ApplyAbilityDamageToTarget(FDamageHitInfo Info, UAbilitySystemComponent* ASCTarget)
{
	FGameplayEffectContextHandle Context = MakeEffectContext();

	FGameplayEffectSpecHandle SpecHandle = MakeOutgoingSpec(Info.HitEffect, 1, Context);

	FGameplayEffectSpec Spec = *SpecHandle.Data.Get();

	Spec.SetSetByCallerMagnitude(UGameplayTagsManager::Get().RequestGameplayTag(FName("Data.Damage")), Info.Damage);

	ApplyGameplayEffectSpecToTarget(Spec, ASCTarget);
}

bool UGBAbilitySystemComponent::CreateAndApplyEffectWithModifiedDuration(TSubclassOf<UGameplayEffect> EffectClass,  float NewDuration,
	UAbilitySystemComponent* ASCTarget)
{
	FGameplayEffectContextHandle Context = MakeEffectContext();

	FGameplayEffectSpecHandle SpecHandle = MakeOutgoingSpec(EffectClass, 1, Context);

	FGameplayEffectSpec Spec = *SpecHandle.Data.Get();
	
	Spec.SetDuration(NewDuration, true);

	UAbilitySystemComponent* ASC = ASCTarget ? ASCTarget : this;

	return ASC->ApplyGameplayEffectSpecToSelf(Spec).IsValid();

}


void UGBAbilitySystemComponent::OnReceiveDamage(float DamageTaken, float UnmitigatedDamage)
{
	OnDamageTaken.Broadcast(DamageTaken, UnmitigatedDamage);
}

void UGBAbilitySystemComponent::GetGameplayAbilitiesWithMatchingTags(const FGameplayTagContainer& GameplayTagContainer,
                                                                     TArray<UGB_GameplayAbility*>& ActiveAbilities)
{
	TArray<FGameplayAbilitySpec*> AbilitiesToActivate;
	GetActivatableGameplayAbilitySpecsByAllMatchingTags(GameplayTagContainer, AbilitiesToActivate, false);

	for(auto It : AbilitiesToActivate)
	{
		TArray<UGameplayAbility*> AbilityInstances = It->GetAbilityInstances();

		for(auto Ability : AbilityInstances)
		{
			ActiveAbilities.Add(Cast<UGB_GameplayAbility>(Ability));
			
		}
	}
}
