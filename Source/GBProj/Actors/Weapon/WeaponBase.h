// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameplayAbilitySpec.h"
#include "GameFramework/Actor.h"
#include "GBProj/AbilitySystem/GBAbilitySystemComponent.h"
#include "GBProj/Func/Types.h"


#include "WeaponBase.generated.h"



UCLASS()
class GBPROJ_API AWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponBase();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Components
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	USceneComponent* SceneComponent = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	UStaticMeshComponent* StaticMesh = nullptr;

	//Offset for spawn
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FTransform OffsetTransform;

	//If true will prefer use weapon skill as Primary attack rather combo.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	bool bForceWeaponSkill = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TMap<FString,TSubclassOf<UGameplayAbility>> WeaponAbilities;
	UPROPERTY(BlueprintReadOnly, Category = "Ability")
	TMap<FString, FGameplayAbilitySpecHandle>  AddedAbilities;
	
	//Hide and block all weapon functions
	UFUNCTION(BlueprintCallable)
	virtual void ActivateWeapon(const bool Flag);

	// Return bIsActive
	UFUNCTION(BlueprintPure)
	FORCEINLINE bool IsActive() const {return bIsActive;}

	// Getter bAttacking
	UFUNCTION(BlueprintPure)
	FORCEINLINE bool IsAttacking() const {return bAttacking;}

	UFUNCTION(BlueprintCallable)
	virtual void StartAttack();
	UFUNCTION(BlueprintCallable)
	virtual void EndAttack();



	// WeaponInfo Getters
	UFUNCTION(BlueprintPure)
	FORCEINLINE FName GetWeaponID() const;
	UFUNCTION(BlueprintPure)
	FORCEINLINE FName GetWeaponClassID() const;
	UFUNCTION(BlueprintPure)
	FORCEINLINE EWeaponType GetWeaponType() const; 
	UFUNCTION(BlueprintPure)
	FORCEINLINE UTexture2D* GetWeaponIcon() const;

	UFUNCTION(BlueprintPure)
	virtual float GetDamage() const {return Damage;}

	// Control attachment of this weapon to actor
	UFUNCTION(BlueprintCallable)
	virtual void SetupAttachment(UPrimitiveComponent* ToComponent);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties")
	FWeaponInfo WeaponInfo;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties")
	float Damage = 10.f;

	UFUNCTION()
	virtual void OnDamageHit(AActor* HitActor);

	UFUNCTION(BlueprintImplementableEvent)
	void OnDamageHit_Event(AActor* HitActor);
	
	// Setter bAttacking
	UFUNCTION(BlueprintCallable)
	void SetAttacking(const bool Flag) {bAttacking = Flag;}

	UPROPERTY(BlueprintReadOnly)
	UGBAbilitySystemComponent* OwnerASC = nullptr;

private:
	bool bIsActive = true;
	bool bAttacking = false;

};
