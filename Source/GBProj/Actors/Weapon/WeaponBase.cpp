// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBase.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"
#include "GameplayTagsManager.h"

// Sets default values
AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetCollisionProfileName("NoCollision");
	StaticMesh->SetGenerateOverlapEvents(false);
	StaticMesh->SetupAttachment(RootComponent);
}

void AWeaponBase::SetupAttachment(UPrimitiveComponent* ToComponent)
{
	const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
	AttachToComponent(ToComponent, Rule, FName("WeaponSocket"));
	SetActorRelativeTransform(OffsetTransform);
	ActivateWeapon(false);
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();

	IAbilitySystemInterface* AbilityOwner = Cast<IAbilitySystemInterface>(GetOwner());
	if(AbilityOwner)
	{
		UGBAbilitySystemComponent* ASC = Cast<UGBAbilitySystemComponent>(AbilityOwner->GetAbilitySystemComponent());
		if(ASC)
		{
			OwnerASC = ASC;
			for(auto& It : WeaponAbilities)
			{
				FGameplayAbilitySpecHandle& SpecHandle = AddedAbilities.FindOrAdd(It.Key);

				if(!SpecHandle.IsValid())
				{
					SpecHandle = ASC->GiveAbility(It.Value);
				}
			}
		}
	}
}

void AWeaponBase::OnDamageHit(AActor* HitActor)
{
	IAbilitySystemInterface* User = Cast<IAbilitySystemInterface>(GetOwner());
	if(User)
	{
		UGBAbilitySystemComponent* ASC = Cast<UGBAbilitySystemComponent>(User->GetAbilitySystemComponent());
		if(ASC)
		{
			FGameplayTag Tag = UGameplayTagsManager::Get().RequestGameplayTag(FName("Event.Hit.Damage"));
			FGameplayEventData Data;
			Data.Target = HitActor;
			
			ASC->HandleGameplayEvent(Tag, &Data);
		}
	}
	
	OnDamageHit_Event(HitActor);
}

void AWeaponBase::StartAttack()
{
}

void AWeaponBase::EndAttack()
{
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeaponBase::ActivateWeapon(const bool Flag)
{
	bIsActive = Flag;
	SetActorHiddenInGame(!Flag);
}

FName AWeaponBase::GetWeaponID() const
{
	return WeaponInfo.WeaponID;
}

FName AWeaponBase::GetWeaponClassID() const
{
	return WeaponInfo.WeaponClassID;
}

EWeaponType AWeaponBase::GetWeaponType() const
{
	return WeaponInfo.WeaponType;
}

UTexture2D* AWeaponBase::GetWeaponIcon() const
{
	return WeaponInfo.WeaponIcon;
}

