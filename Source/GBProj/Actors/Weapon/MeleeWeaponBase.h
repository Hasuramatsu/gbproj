// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "WeaponBase.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "GBProj/Func/Types.h"

#include "MeleeWeaponBase.generated.h"

class USphereComponent;
UCLASS()
class GBPROJ_API AMeleeWeaponBase : public AWeaponBase
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AMeleeWeaponBase();


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FMeleeInfo MeleeInfo;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;


	UFUNCTION(BlueprintCallable)
	void AddCombo();
	UFUNCTION(BlueprintCallable)
	void EndCombo();

	// In base variant activate all collisions of weapon
	virtual void StartAttack() override;

	// In base variant deactivate all collisions of weapon
	virtual void EndAttack() override;
	
	UFUNCTION(BlueprintCallable)
	void ResetAttack();

	UFUNCTION(BlueprintCallable)
	void SetCurrentCombo(const int32 Value) {CurrentCombo = Value;}
	UFUNCTION(BlueprintPure)
	FORCEINLINE int32 GetMaxCombo() const {return MaxCombo;}

	UFUNCTION(BlueprintCallable)
	void SetWeaponCollision(FName CollisionName);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<APawn> IgnoreClass;

	UFUNCTION()
	void SetBaseCollision(const FName CollisionName) {BaseCollision = CollisionName;}

	UFUNCTION()
	void RegisterCollision(UPrimitiveComponent* Collision);

	UFUNCTION(BlueprintCallable)
	FName GetBaseCollision() const {return BaseCollision;}
	

private:

	UPROPERTY()
	TArray<AActor*> ActorsHit;
	
	int32 CurrentCombo = 0;
	int32 MaxCombo = 1;

	FName BaseCollision;

	UPROPERTY()
	TArray<UPrimitiveComponent*> WeaponCollisions;
};

UCLASS()
class GBPROJ_API AMeleeWeapon_OneCollision : public AMeleeWeaponBase
{
	GENERATED_BODY()

public:

	AMeleeWeapon_OneCollision();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	UBoxComponent* CollisionBox = nullptr;

private:
	
};

UCLASS()
class GBPROJ_API AMeleeWeapon_TwoCollision : public AMeleeWeaponBase
{
	GENERATED_BODY()

public:

	AMeleeWeapon_TwoCollision();

	UFUNCTION(BlueprintCallable)
	void StartAttack_FirstSphere(const bool Flag);

	UFUNCTION(BlueprintCallable)
	void StartAttack_SecondSphere(const bool Flag);
	

	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	USphereComponent* FirstCollisionSphere = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	USphereComponent* SecondCollisionSphere = nullptr;

	virtual void SetupAttachment(UPrimitiveComponent* ToComponent) override;
private:
	
};