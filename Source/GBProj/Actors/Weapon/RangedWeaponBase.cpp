// Fill out your copyright notice in the Description page of Project Settings.


#include "RangedWeaponBase.h"
#include "AbilitySystemComponent.h"
#include "GameplayTagsManager.h"


// Sets default values
ARangedWeaponBase::ARangedWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	// RootComponent = SceneComponent;

	// StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	// StaticMesh->SetGenerateOverlapEvents(false);
	// StaticMesh->SetCollisionProfileName(TEXT("NoCollision"));
	// StaticMesh->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
	
	AmmoChange.AddDynamic(this, &ARangedWeaponBase::EventOnAmmoChange);
}

void ARangedWeaponBase::OnDamageHit(AActor* HitActor)
{
	Super::OnDamageHit(HitActor);
}

// Called when the game starts or when spawned
void ARangedWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	CurrentAmmo = RangedWeaponInfo.MaxClipAmmo;
	FireTimer = RangedWeaponInfo.FireRate;

	GlowMaterialInstance = StaticMesh->CreateDynamicMaterialInstance(1);
}

void ARangedWeaponBase::SetCurrentAmmo(const int32 Value)
{
	CurrentAmmo = Value;
}

void ARangedWeaponBase::AddDispersion()
{
	if(CurrentDispersion < RangedWeaponInfo.MaxDispersion)
	{
		CurrentDispersion += RangedWeaponInfo.DispersionByShot;
		if(CurrentDispersion > RangedWeaponInfo.MaxDispersion)
		{
			CurrentDispersion = RangedWeaponInfo.MaxDispersion;
		}
	}
	if(!GetWorld()->GetTimerManager().IsTimerActive(DispersionReduceTimeHandle))
	{
		GetWorld()->GetTimerManager().SetTimer(DispersionReduceTimeHandle, this, &ARangedWeaponBase::RemoveDispersion, DispersionTickTime, true);
	}
}

void ARangedWeaponBase::RemoveDispersion()
{
	CurrentDispersion -= RangedWeaponInfo.DispersionDecreaseRate*DispersionTickTime;
	if(CurrentDispersion <= 0)
	{
		CurrentDispersion = 0;
		GetWorld()->GetTimerManager().ClearTimer(DispersionReduceTimeHandle);
	}
}

void ARangedWeaponBase::ContinueDispersionTimer()
{
	if(GetWorld()->GetTimerManager().IsTimerPaused(DispersionReduceTimeHandle))
	{
		GetWorld()->GetTimerManager().UnPauseTimer(DispersionReduceTimeHandle);
	}
}

void ARangedWeaponBase::ApplyDispersion(FRotator& SpawnRotation)
{
	const float Dispersion = FMath::RandRange(-CurrentDispersion, CurrentDispersion);
	SpawnRotation += FRotator(Dispersion, 0, 0);
}

// Called every frame
void ARangedWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FireTick(DeltaTime);
//	ReloadTick(DeltaTime);
}

void ARangedWeaponBase::FireTick(float DeltaTime)
{
	if(bIsFiring && FireTimer >= RangedWeaponInfo.FireRate)
	{
		Fire();
		FireTimer = 0;
	}
	else if(FireTimer < RangedWeaponInfo.FireRate)
	{
		FireTimer+=DeltaTime;
	}
}

/*
void ARangedWeaponBase::ReloadTick(float DeltaTime)
{
	if(bCanReload)
	{
		if(ReloadTimer >= WeaponInfo.ReloadTime)
		{
			ReloadComplete();
		}
		else
		{
			ReloadTimer += DeltaTime;
		}
	}
}
*/

void ARangedWeaponBase::ChangeActive(const bool Flag)
{
//	bActive = Flag;
	SetActorHiddenInGame(!Flag);
	if(!Flag)
	{
		bIsFiring = false;
		ReloadCancel();
		if(!GetWorld()->GetTimerManager().IsTimerActive(ReloadTimer) && !GetWorld()->GetTimerManager().IsTimerActive(ReloadDelayTimer))
		{
			ReloadDelayDelegate.BindUFunction(this, FName("StartReload"), false);
			//TODO Change
			GetWorld()->GetTimerManager().SetTimer(ReloadDelayTimer, ReloadDelayDelegate, RangedWeaponInfo.ReloadDelay, false);
		}
	}
}

void ARangedWeaponBase::Fire(FVector TargetLocation)
{
	if(CurrentAmmo > 0)
	{
		if(true) //RangedWeaponInfo.Projectile
		{
			OnFire.Broadcast(RangedWeaponInfo.OnFireAnimMontage);
			if(GetWorld()->GetTimerManager().IsTimerActive(ReloadTimer))
			{
				GetWorld()->GetTimerManager().ClearTimer(ReloadTimer);
			}
			CurrentAmmo--;
			AmmoChange.Broadcast(CurrentAmmo);

			if(OwnerASC)
			{
				FGameplayEventData Data;
				Data.Instigator = GetOwner();
				OwnerASC->HandleGameplayEvent(UGameplayTagsManager::Get().RequestGameplayTag(FName("Event.Weapon.Shot")), &Data);
			}
			// AddDispersion();
			//
			// UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), RangedWeaponInfo.WeaponSounds.FireSound, ShootLocation->GetComponentTransform(), true);
			// UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), RangedWeaponInfo.FireFX, ShootLocation->GetComponentTransform());
			//
			// FVector SpawnLocation = ShootLocation->GetComponentLocation();
			// SpawnLocation.X = 0;
			// FVector EndLocation;
			// if(TargetLocation == FVector::ZeroVector)
			// {
			// 	const bool Result = (Cast<IInterfaceCharacter>(GetOwner())->GetTargetLocation_Implementation() - DeadZoneComponent->GetComponentLocation()).Size() > DistanceForIgnore;
			// 	EndLocation = Result ? Cast<IInterfaceCharacter>(GetOwner())->GetTargetLocation_Implementation() : (ShootLocation->GetForwardVector() * 500) + GetActorLocation();
			// }
			// else
			// {
			// 	EndLocation = TargetLocation;
			// }	
			// FVector Direction = EndLocation - SpawnLocation;
			// Direction.Normalize();
			// const FMatrix MyMatrix(Direction, FVector::ZeroVector, FVector(0, 0, 1), FVector(1, 0, 0));
			// FRotator SpawnRotation = MyMatrix.Rotator();
			// if(ShootLocation->GetComponentRotation().Yaw > 0)
			// {
			// 	SpawnRotation.Yaw = 90;
			// }
			// else
			// {
			// 	SpawnRotation.Yaw = -90;
			// }
			//
			// ApplyDispersion(SpawnRotation);
			//
			// FActorSpawnParameters SpawnParams;
			// SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			// SpawnParams.Owner = GetOwner();
			// SpawnParams.Instigator = GetInstigator();
			//
			// GetWorld()->SpawnActor(RangedWeaponInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams);
			// //AProjectileBase* myProjectile = Cast<AProjectileBase>(GetWorld()->SpawnActor(WeaponInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
			if(GetWorld()->GetTimerManager().IsTimerActive(DispersionReduceTimeHandle) && !GetWorld()->GetTimerManager().IsTimerPaused(DispersionReduceTimeHandle))
			{
				GetWorld()->GetTimerManager().PauseTimer(DispersionReduceTimeHandle);
			}
			GetWorld()->GetTimerManager().SetTimer(DispersionDelayTimerHandle, this, &ARangedWeaponBase::ContinueDispersionTimer, RangedWeaponInfo.DispersionDelayTime);
			if(CurrentAmmo <= 0)
			{
				bIsFiring = false;
				GetWorld()->GetTimerManager().ClearTimer(ReloadDelayTimer);
				StartReload(true);
			}
			else if(!GetWorld()->GetTimerManager().IsTimerActive(ReloadTimer))
			{
				ReloadDelayDelegate.BindUFunction(this, FName("StartReload"), false);
				GetWorld()->GetTimerManager().SetTimer(ReloadDelayTimer, ReloadDelayDelegate, RangedWeaponInfo.ReloadDelay, false);
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ARangedWeaponBase::Fire() -- No Projectile detected"));
		}
	}
	else
	{
		bIsFiring = false;
		ReloadStart();
		//TODO Why skip
	}
}

bool ARangedWeaponBase::MakeSingleShot(FVector TargetLocation)
{
	if(FireTimer >= RangedWeaponInfo.FireRate && CheckCanFire())
	{
		Fire(TargetLocation);
		FireTimer = 0;
		return true;
	}
	else
	{
		return false;
	}
}

void ARangedWeaponBase::StartReload(bool Forced)
{
	const float ReloadRate = !Forced ? RangedWeaponInfo.ReloadTime : RangedWeaponInfo.ForcedReloadTime;
	if(Forced)
	{
		bIsFiring = false;
	}
	bForcedReload = Forced;
	GetWorld()->GetTimerManager().SetTimer(ReloadTimer, this, &ARangedWeaponBase::ReloadTick, ReloadRate, true);
}

void ARangedWeaponBase::ReloadTick()
{
	CurrentAmmo++;
	
	if(CurrentAmmo >= RangedWeaponInfo.MaxClipAmmo)
	{
		CurrentAmmo = RangedWeaponInfo.MaxClipAmmo;
		ReloadComplete();
	}
	AmmoChange.Broadcast(CurrentAmmo);
}

void ARangedWeaponBase::AltFire_Implementation()
{

 }

void ARangedWeaponBase::SetIsFiring(bool IsFiring)
{
	//TODO think about cancel fire ability
	if(CheckCanFire())
	{
		//if(bIsReloading)
		//{
		//	ReloadCancel();
		//}
		bIsFiring = IsFiring;
	}
	else
	{
		bIsFiring = false;
	}
}

void ARangedWeaponBase::UseAltFire_Implementation(bool Flag)
{

}

void ARangedWeaponBase::ReloadStart()
{
	if(CurrentAmmo < RangedWeaponInfo.MaxClipAmmo)
	{
		StartReload(true);
	}
	else
	{
		//TODO
		UE_LOG(LogTemp, Warning, TEXT("ARangedWeaponBase::ReloadStart() -- Ammo full"));
	}
}

void ARangedWeaponBase::ReloadCancel()
{
	GetWorld()->GetTimerManager().ClearTimer(ReloadTimer);
	bForcedReload = false;
	ReloadResult.Broadcast(false);
}

void ARangedWeaponBase::ReloadComplete()
{
	GetWorld()->GetTimerManager().ClearTimer(ReloadTimer);
	bForcedReload = false;
	ReloadResult.Broadcast(true);
	CurrentDispersion = 0;
}

bool ARangedWeaponBase::CheckCanFire_Implementation()
{
	const bool CanFire = CurrentAmmo > 0 &&  !bForcedReload; //bActive &&
	return CanFire;
}

bool ARangedWeaponBase::CheckCanAltFire_Implementation()
{
	//const bool CanFire = CurrentAmmo == WeaponInfo.AmmoForChargeShot;
	return true;
}

void ARangedWeaponBase::EventOnAmmoChange_Implementation(const int32 Value)
{
	if(GlowMaterialInstance)
	{
		const float GlowValue = Value * 100 / RangedWeaponInfo.MaxClipAmmo;
		GlowMaterialInstance->SetScalarParameterValue("Glow", GlowValue);
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, TEXT("ARangedWeaponBase::EventOnAmmoChange -- Material nullptr"));
	}
}