// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "WeaponBase.h"
#include "Components/ArrowComponent.h"
#include "GBProj/Actors/Projectiles/ProjectileBase.h"
#include "GameFramework/Actor.h"
#include "GBProj/Func/Types.h"
#include "RangedWeaponBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAmmoChange, int32, Ammo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FReloadResult, bool, Result);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnFire, UAnimMontage*, Montage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnReloadStart, UAnimMontage*, Montage);

UCLASS()
class GBPROJ_API ARangedWeaponBase : public AWeaponBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARangedWeaponBase();

	// UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	// USceneComponent* SceneComponent = nullptr;
	// UPROPERTY(VisibleAnywhere, BlueprintReadOnly ,meta = (AllowPrivateAccess = "true"), Category = Components)
	// UStaticMeshComponent* StaticMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly ,meta = (AllowPrivateAccess = "true"), Category = Components)
	UArrowComponent* ShootLocation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponStats")
	FRangedWeaponInfo RangedWeaponInfo;


	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FAmmoChange AmmoChange;
	UPROPERTY(BlueprintAssignable)
	FReloadResult ReloadResult;
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FOnFire OnFire;
	UPROPERTY(BlueprintAssignable)
	FOnReloadStart OnReloadStart;

	
	virtual void OnDamageHit(AActor* HitActor) override;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Material")
	UMaterialInstanceDynamic* GlowMaterialInstance = nullptr;

	UFUNCTION(BlueprintNativeEvent)
	void EventOnAmmoChange(const int32 Value);
	
	
private:

	UPROPERTY()
	bool bCanReload = true;
	UPROPERTY()
	bool bForcedReload = false;
	UPROPERTY()
	bool bIsFiring = false;
	
	//bool bIsCharging = false;
	UPROPERTY(BlueprintReadWrite, meta= (AllowPrivateAccess = "true"))
	int32 CurrentAmmo = 0;
	float CurrentDispersion = 0.f;
	float DistanceForIgnore = 150.f;

	float FireTimer = 0;
	//float ReloadTimer = 0;

	//Reload
	const float TESTRELOADTIMER = 5.f;
	FTimerDelegate ReloadDelayDelegate;
	FTimerHandle ReloadDelayTimer;
	FTimerHandle ReloadTimer;
	UPROPERTY()
	const USceneComponent* DeadZoneComponent;

	//Dispersion
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "ActorTick")
	float DispersionTickTime = 0.1f;

	FTimerHandle DispersionReduceTimeHandle;
	FTimerHandle DispersionDelayTimerHandle;
	//FTimerHandle ChargeAttackTimerHandle;

	//Increase current dispersion by const value
	void AddDispersion();
	//Decrease current dispersion by const value
	void RemoveDispersion();
	//Unpause dispersion timer
	void ContinueDispersionTimer();

	//Recalculate rotation with dispersion
	void ApplyDispersion(FRotator& SpawnRotation);

	
	void ReloadComplete();


	//Fire projectile
	void Fire(FVector TargetLocation = FVector::ZeroVector);

	//Reload
	UFUNCTION()
	void StartReload(bool Forced = false);
	UFUNCTION()
	void ReloadTick();
	//
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void FireTick (float DeltaTime);
	//void ReloadTick(float DeltaTime);

	void ChangeActive(const bool Flag);

	//Fire func
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AltFire();
	virtual void AltFire_Implementation();

	
	//Toggle firing flag
	UFUNCTION(BlueprintCallable)
	void SetIsFiring(bool IsFiring);
	UFUNCTION(BlueprintCallable)
	bool MakeSingleShot(FVector TargetLocation = FVector::ZeroVector);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void UseAltFire(bool Flag);
	virtual void UseAltFire_Implementation(bool Flag);

	
	//Turn off fire flag and turn on reload flag
	UFUNCTION(BlueprintCallable)
	void ReloadStart();
	//
	UFUNCTION(BlueprintCallable)
	void ReloadCancel();
	
	//Check that weapon have ammo and not reloading
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, BlueprintPure)
	bool CheckCanFire();
	virtual bool CheckCanFire_Implementation();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, BlueprintPure)
	bool CheckCanAltFire();
	virtual bool CheckCanAltFire_Implementation();


	//Gettes
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetCurrentAmmo() const {return CurrentAmmo;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCurrentDispersion() const {return CurrentDispersion;}
	//UFUNCTION(BlueprintCallable, BlueprintPure)
	//float GetReloadTimer() const {return ReloadTimer;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetFireTimer() const {return FireTimer;}
	//UFUNCTION(BlueprintCallable, BlueprintPure)
	//bool IsReloading() const {return bIsReloading;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsFiring() const {return bIsFiring;}


	//Setters
	UFUNCTION(BlueprintCallable)
	void SetCurrentAmmo(const int32 Value);
	//UFUNCTION(BlueprintCallable)
	//void SetIsReloading(const bool Flag) {bIsReloading = Flag;}
	UFUNCTION()
	void SetDeadZoneComponent(const USceneComponent* Component) {DeadZoneComponent = Component;}
};
