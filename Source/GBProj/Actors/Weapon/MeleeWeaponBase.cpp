// Fill out your copyright notice in the Description page of Project Settings.


#include "MeleeWeaponBase.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"
#include "GameplayTagsManager.h"
#include "GraphEditor/Private/GraphEditorCommon.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AMeleeWeaponBase::AMeleeWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
}

// Called when the game starts or when spawned
void AMeleeWeaponBase::BeginPlay()
{
	Super::BeginPlay();

	SetWeaponCollision("NoCollision");
	
	MaxCombo = MeleeInfo.MeleeComboMontage.Num() - 1;	
}

void AMeleeWeaponBase::RegisterCollision(UPrimitiveComponent* Collision)
{
	WeaponCollisions.Add(Collision);
}

void AMeleeWeaponBase::SetWeaponCollision(FName CollisionName)
{
	for(auto& It : WeaponCollisions)
	{
		It->SetCollisionProfileName(CollisionName);
	}
}


void AMeleeWeaponBase::NotifyActorBeginOverlap(AActor* OtherActor)
{
	if(IsAttacking()) //bIsActive
	{
		if(OtherActor != GetOwner() && !OtherActor->GetClass()->IsChildOf(IgnoreClass) && !ActorsHit.Contains(OtherActor))
		{
			// IAbilitySystemInterface* User = Cast<IAbilitySystemInterface>(GetOwner());
			// if(User)
			// {
			// 	UAbilitySystemComponent* ASC = User->GetAbilitySystemComponent();
			// 	if(ASC)
			// 	{
			// 		FGameplayEventData Data;
			// 		Data.Instigator = this;
			// 		Data.Target = OtherActor;
			// 		
			// 		ASC->HandleGameplayEvent(UGameplayTagsManager::Get().RequestGameplayTag(TEXT("Event.Hit.Damage")), &Data);
			// 	}

			OnDamageHit(OtherActor);

			//UGameplayStatics::ApplyDamage(OtherActor, MeleeInfo.MeleeComboMontage[CurrentCombo].Damage, GetInstigatorController(), this, UDamageType::StaticClass());
			ActorsHit.Add(OtherActor);
		}
	}
}


void AMeleeWeaponBase::StartAttack()
{
	SetAttacking(true);

	SetWeaponCollision(BaseCollision);
}

void AMeleeWeaponBase::EndAttack()
{
	ActorsHit.Empty();
	SetAttacking(false);

	SetWeaponCollision(FName("NoCollision"));
}

void AMeleeWeaponBase::ResetAttack()
{
	ActorsHit.Empty();
}


void AMeleeWeaponBase::AddCombo()
{
	if(CurrentCombo < MaxCombo)
	{
		CurrentCombo++;
	}
	else
	{
		EndCombo();
	}
}

void AMeleeWeaponBase::EndCombo()
{
	EndAttack();
	CurrentCombo = 0;
}

///////////////////////////////////////////////////////
//////////  One Collision /////////////////////////////
///////////////////////////////////////////////////////


AMeleeWeapon_OneCollision::AMeleeWeapon_OneCollision()
{
	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	CollisionBox->SetCollisionProfileName("OverlapAll");
	CollisionBox->SetupAttachment(RootComponent);

	SetBaseCollision(CollisionBox->GetCollisionProfileName());
	RegisterCollision(CollisionBox);
}




///////////////////////////////////////////////////////
//////////  Two Collisions ////////////////////////////
///////////////////////////////////////////////////////


AMeleeWeapon_TwoCollision::AMeleeWeapon_TwoCollision()
{
	FirstCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("FirstCollisionSphere"));
	FirstCollisionSphere->SetCollisionProfileName("OverlapAll");
	FirstCollisionSphere->SetupAttachment(RootComponent);
	RegisterCollision(FirstCollisionSphere);

	SecondCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("SecondCollisionSphere"));
	SecondCollisionSphere->SetCollisionProfileName("OverlapAll");
	SecondCollisionSphere->SetupAttachment(RootComponent);
	RegisterCollision(SecondCollisionSphere);

	SetBaseCollision(FirstCollisionSphere->GetCollisionProfileName());
}

void AMeleeWeapon_TwoCollision::StartAttack_FirstSphere(const bool Flag)
{
	SetAttacking(Flag);
	if(Flag)
	{
		FirstCollisionSphere->SetCollisionProfileName(GetBaseCollision());
	}
	else
	{
		FirstCollisionSphere->SetCollisionProfileName(FName("NoCollision"));
	}
}

void AMeleeWeapon_TwoCollision::StartAttack_SecondSphere(const bool Flag)
{
	SetAttacking(Flag);
	if(Flag)
	{
		SecondCollisionSphere->SetCollisionProfileName(GetBaseCollision());
	}
	else
	{
		SecondCollisionSphere->SetCollisionProfileName(FName("NoCollision"));
	}
}

void AMeleeWeapon_TwoCollision::SetupAttachment(UPrimitiveComponent* ToComponent)
{
	const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
	AttachToComponent(ToComponent, Rule, FName("WeaponSocket"));
	SetActorRelativeTransform(OffsetTransform);
	ActivateWeapon(false);

	FirstCollisionSphere->AttachToComponent(ToComponent, Rule, FName("WeaponSocket"));
	SecondCollisionSphere->AttachToComponent(ToComponent, Rule, FName("OffhandSocket"));
	
}
