// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

//
#include "GameplayEffectTypes.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "GBProj/Func/Types.h"

#include "ProjectileBase.generated.h"


class ARangedWeaponBase;

UCLASS()
class GBPROJ_API AProjectileBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectileBase();
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FProjectileInfo ProjectileInfo;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;

	//virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	UFUNCTION(BlueprintCallable)
	void SetParentWeapon(ARangedWeaponBase* Weapon) { ParentWeapon = Weapon;}

	UFUNCTION()
	void CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	//Getters
	FORCEINLINE USphereComponent* GetCollisionSphere() const {return CollisionSphere;}
	FORCEINLINE UProjectileMovementComponent* GetMovementComponent() const {return ProjectileMovementComponent;}

	// Set GameplayEffectSpecHandle for future apply
	//void SetEffectHandle(const FGameplayEffectSpecHandle Handle) {EffectHandle = Handle;}

	UFUNCTION(BlueprintCallable)
	void SetDamageHitInfo(FDamageHitInfo Info) {DamageHitInfo = Info;}
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Component)
	USphereComponent* CollisionSphere = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Component)
	UStaticMeshComponent* StaticMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Component)
	UProjectileMovementComponent* ProjectileMovementComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Component)
	UParticleSystemComponent* TrailFX = nullptr;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Destroyed() override;
	
private:		
	FGameplayEffectSpecHandle EffectHandle;

	UPROPERTY()
	ARangedWeaponBase* ParentWeapon = nullptr;

	FDamageHitInfo	DamageHitInfo;
};
