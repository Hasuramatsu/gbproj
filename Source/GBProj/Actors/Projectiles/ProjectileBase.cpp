// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileBase.h"

#include "GameplayEffectTypes.h"
#include "GBProj/Actors/Weapon/RangedWeaponBase.h"
#include "GBProj/Func/GBLib.h"

#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
AProjectileBase::AProjectileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetSphereRadius(10.f);
	CollisionSphere->bReturnMaterialOnMove = true;
	CollisionSphere->SetCanEverAffectNavigation(false);
	CollisionSphere->SetSimulatePhysics(false);
	CollisionSphere->SetEnableGravity(false);
	CollisionSphere->SetCollisionProfileName(TEXT("OverlapAll"));
	CollisionSphere->SetGenerateOverlapEvents(true);
	CollisionSphere->SetConstraintMode(EDOFMode::YZPlane);
	RootComponent = CollisionSphere;
	
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaicMesh"));
	StaticMesh->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMesh->SetGenerateOverlapEvents(false);
	StaticMesh->SetCanEverAffectNavigation(false);
	StaticMesh->SetupAttachment(RootComponent);

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ProjectileMovementComponent->UpdatedComponent = RootComponent;
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->bShouldBounce = false;

	TrailFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("TrailFX"));
	TrailFX->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();

	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileBase::CollisionSphereBeginOverlap);
}

void AProjectileBase::Destroyed()
{
	OnDestroyed.Broadcast(this);
	Super::Destroyed();
}

// Called every frame
void AProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileBase::NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp,
	bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
	
	this->Destroy();
}


void AProjectileBase::CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor != GetOwner() && OtherActor->GetOwner() != GetOwner())
	{
		// if(ParentWeapon)
		// {
		// 	ParentWeapon->OnDamageHit(OtherActor);
		// }
			// IAbilitySystemInterface* User = Cast<IAbilitySystemInterface>(GetOwner());
			// if(User)
			// {
			// 	UGBAbilitySystemComponent* ASC = Cast<UGBAbilitySystemComponent>(User->GetAbilitySystemComponent());
			// 	if(ASC)
			// 	{
			// 		FDamageHitInfo DamageHitInfo;
			// 		DamageHitInfo.Dealer = this;
			// 		DamageHitInfo.DealerOwner = GetOwner();
			// 		DamageHitInfo.TargetActor = OtherActor;
			//
			// 		ASC->HandleDamageHit(DamageHitInfo);
		
		UGBLib::ApplyAbilityDamageHit(DamageHitInfo, OtherActor, GetOwner());
		
		if(OtherActor && SweepResult.PhysMaterial.IsValid())
		{
			const EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType((SweepResult));
			if(ProjectileInfo.HitEffects.HitDecals.Contains(MySurfaceType))
			{
				UMaterialInterface* MyMaterial = ProjectileInfo.HitEffects.HitDecals[MySurfaceType];
				if(MyMaterial && OtherComp)
				{
					UGameplayStatics::SpawnDecalAttached(MyMaterial, FVector(20.f), OtherComp, NAME_None, SweepResult.ImpactPoint, SweepResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition);
				}
			}
			if(ProjectileInfo.HitEffects.HitFX.Contains(MySurfaceType))
			{
				UParticleSystem* MyParticle = ProjectileInfo.HitEffects.HitFX[MySurfaceType];
				if(MyParticle && OtherComp)
				{
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), MyParticle, FTransform(SweepResult.ImpactNormal.Rotation(), SweepResult.ImpactPoint, FVector(1.f)));
				}
			}
			// if(ProjectileInfo.HitEffects.HitSound.Contains(MySurfaceType))
			// {
			// 	UFMODEvent* MySound = ProjectileInfo.HitEffects.HitSound[MySurfaceType];
			// 	if(MySound && OtherComp)
			// 	{
			// 	
			// 		UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), MySound, FTransform(SweepResult.ImpactNormal.Rotation(), SweepResult.ImpactPoint, FVector(1.f)), true);
			// 	}
			// }
		}
	
		if(ProjectileInfo.bCanPierce)
		{
			if(ProjectileInfo.PierceCount == 0)
			{
				this->Destroy();
			}
			ProjectileInfo.PierceCount--;
		}
		else
		{
			this->Destroy();
		}
	}

}

