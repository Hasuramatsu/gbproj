// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyBase.h"
#include "EnemyWerewolf.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API AEnemyWerewolf : public AEnemyBase
{
	GENERATED_BODY()

public:

	//Constructor
	AEnemyWerewolf();
protected:
//vars

//func
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void Landed(const FHitResult& Hit) override;

private:

};



