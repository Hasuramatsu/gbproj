// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GBProj/Actors/GBCharacterBase.h"

#include "EnemyBase.generated.h"

UCLASS()
class GBPROJ_API AEnemyBase : public AGBCharacterBase
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	float MaxFollowDistance = 1000.f;

	virtual FVector GetObjectLocation() const override;

	
protected:

	// Overrides
	virtual void Tick(float DeltaTime) override;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:



};


