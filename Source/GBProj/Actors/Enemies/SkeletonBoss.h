// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyBoss.h"
#include "GBProj/Interfaces/InterfaceChargeable.h"

#include "SkeletonBoss.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API ASkeletonBoss : public AEnemyBoss, public IInterfaceChargeable
{
	GENERATED_BODY()

public:

	//Func
	ASkeletonBoss();

protected:



	//Func
	virtual void BeginPlay() override;
	
private:

};
