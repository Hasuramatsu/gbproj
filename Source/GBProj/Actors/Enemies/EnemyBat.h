// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyBase.h"
#include "EnemyBat.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API AEnemyBat : public AEnemyBase
{
	GENERATED_BODY()
public:

	AEnemyBat();
	
protected:

//func
	virtual void BeginPlay() override;
	//virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	virtual void NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;
	virtual void Tick(float DeltaSeconds) override;

	
	virtual void OnMovementSpeedAttributeChanged(const FOnAttributeChangeData& Data) override;
private:


};

