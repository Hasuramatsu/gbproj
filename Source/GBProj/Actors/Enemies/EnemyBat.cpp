// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyBat.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"


AEnemyBat::AEnemyBat()
{
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);
}

void AEnemyBat::BeginPlay()
{
	Super::BeginPlay();

	if(GetCharacterMovement())
	{
		//Bind Movement Speed attribute changes to Character Movement component
		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMovementSpeedAttribute()).AddUObject(this, &AEnemyBat::OnMovementSpeedAttributeChanged);
	}
}


void AEnemyBat::NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved,
	FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
}

void AEnemyBat::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AEnemyBat::OnMovementSpeedAttributeChanged(const FOnAttributeChangeData& Data)
{
	GetCharacterMovement()->MaxFlySpeed = Data.NewValue;	
}