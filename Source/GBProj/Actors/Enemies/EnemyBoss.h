// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyBase.h"
#include "GBProj/Interfaces/InterfaceSkillUser.h"


#include "EnemyBoss.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API AEnemyBoss : public AEnemyBase, public IInterfaceSkillUser
{
	GENERATED_BODY()

public:
	//Vars

	//Func
	AEnemyBoss();

protected:

	
private:



};

