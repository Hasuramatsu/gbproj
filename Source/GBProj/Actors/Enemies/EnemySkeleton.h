// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "EnemyBase.h"
#include "EnemySkeleton.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API AEnemySkeleton : public AEnemyBase
{
	GENERATED_BODY()

public:
	AEnemySkeleton();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<AWeaponBase> WeaponClass;
	
protected:
	
	virtual void BeginPlay() override;

	
private:


};
