// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyBase.h"
#include "GBProj/Actors/Weapon/MeleeWeaponBase.h"
#include "EnemyVampire.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API AEnemyVampire : public AEnemyBase
{
	GENERATED_BODY()

public:
//vars
	AEnemyVampire();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<AMeleeWeapon_TwoCollision> HandWeapon;
	
protected:
//Components

	
//vars

//func
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
private:
//vars

//func

};
