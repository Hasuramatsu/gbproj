// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyBase.h"


#include "BehaviorTree/BlackboardComponent.h"
#include "Components/CapsuleComponent.h"
#include "GBProj/Game/GBAIControllerBase.h"


// Sets default values
AEnemyBase::AEnemyBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	GetMesh()->SetCollisionProfileName("NoCollision");
	GetCapsuleComponent()->SetCollisionProfileName("EnemyPawn");
	
}

// Called when the game starts or when spawned
void AEnemyBase::BeginPlay()
{
	Super::BeginPlay();

}


FVector AEnemyBase::GetObjectLocation() const
{
	if(AGBAIControllerBase* EnemyController = Cast<AGBAIControllerBase>(GetController()))
	{
		AActor* TargetActor = Cast<AActor>(EnemyController->GetBlackboardComponent()->GetValueAsObject(FName("TargetCharacter")));
		if(TargetActor)
		{
			return TargetActor->GetActorLocation();
		}
		
	}

	return GetActorLocation() + GetActorForwardVector() * 1000;
}

// Called every frame
void AEnemyBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}







