// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyVampire.h"

#include "GameplayTagsManager.h"


AEnemyVampire::AEnemyVampire()
{

}


void AEnemyVampire::BeginPlay()
{
	Super::BeginPlay();

	if(HandWeapon)
	{
		CurrentWeapon = InitWeapon(HandWeapon);
		if(CurrentWeapon)
		{
			CurrentWeapon->ActivateWeapon(true);
		}
	}
}

void AEnemyVampire::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

}

void AEnemyVampire::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if(OtherActor->ActorHasTag(FName("Player")))
	{
		FGameplayTag Tag = UGameplayTagsManager::Get().RequestGameplayTag(FName("Develop.Helper.Hit"));
		FGameplayEventData Data;
		Data.Instigator = this;
		Data.Target = OtherActor;
		AbilitySystemComponent->HandleGameplayEvent(Tag, &Data);
	}
}

