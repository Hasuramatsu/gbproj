// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyBoss.h"


#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

AEnemyBoss::AEnemyBoss()
{
	GetCapsuleComponent()->SetCollisionProfileName("Boss");
}

