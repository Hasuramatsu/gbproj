// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySkeleton.h"

#include "GBProj/Actors/Weapon/WeaponBase.h"


AEnemySkeleton::AEnemySkeleton()
{
	
}

void AEnemySkeleton::BeginPlay()
{
	Super::BeginPlay();


	CurrentWeapon = InitWeapon(WeaponClass);
	CurrentWeapon->ActivateWeapon(true);

}


