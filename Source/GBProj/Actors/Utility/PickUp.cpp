// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUp.h"

#include "GBProj/Actors/MainCharacter.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APickUp::APickUp()
{

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = Mesh;
	Mesh->SetCollisionProfileName("OverlapAllDynamic");
	Mesh->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	Mesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetupAttachment(RootComponent);
	CollisionSphere->SetSphereRadius(100.f);
	CollisionSphere->SetCollisionProfileName("OverlapAllDynamic");

}

void APickUp::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);


	if(AMainCharacter* MC = Cast<AMainCharacter>(OtherActor))
	{
		MC->AddInteractable(this);
	}
	
}

// Called when the game starts or when spawned
void APickUp::BeginPlay()
{
	Super::BeginPlay();
	
}

void APickUp::Interact_Implementation(AActor* Interactor, bool bFlag)
{

	if(AMainCharacter* MC = Cast<AMainCharacter>(Interactor))
	{
		OnPickUp(MC);

		Destroy();
	}
}

FDisplayInfo APickUp::GetDisplayInfo_Implementation()
{
	FDisplayInfo Info;
	Info.InteractionType = EInteractionType::PickupType;
	Info.ObjectName = PickupName;
	
	return Info;
}

void APickUp::OnPickUp_Implementation(const AMainCharacter* PickupBy)
{
	
}


