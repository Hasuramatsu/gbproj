// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GBProj/Actors/ActorComponents/DropComponent.h"
#include "GBProj/Actors/Enemies/EnemyBase.h"


#include "Spawner.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSpawn, AEnemyBase*, Enemy);

UCLASS()
class GBPROJ_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

	bool bEnemySpawner = true;
	//Range for disable spawn
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DeadRange = 500.f;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite)
	// float SpawnTimer = 30.f;
	
	FOnSpawn OnSpawn;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool CheckCanSpawn() const;
	// UFUNCTION(BlueprintCallable)
	// bool AttemptToSpawn();

	UFUNCTION(BlueprintCallable)
	void SetZone(const FName NewZone) {Zone = NewZone;}

	//Func new
	UFUNCTION(BlueprintPure)
	FORCEINLINE bool IsRespawnSpawner() const {return bSpawnOnRespawn;}

	UFUNCTION(BlueprintCallable)
	bool Spawn();
	//Perform check and then call spawn
	UFUNCTION(BlueprintCallable)
	bool TriggerSpawn();
protected:
	//Parameters
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName Zone;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(EditCondition="!bTriggerableSpawn"))
	bool bSpawnOnRespawn = true;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(EditCondition="!bSpawnOnRespawn"))
	bool bTriggerableSpawn = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(EditCondition="bTriggerableSpawn"))
	bool bTriggerOnce = false;

	//Func
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Components, meta =(AllowPrivateAccess = "true"))
	UDropComponent* DropComponent;

	bool bSpawned = true;

	//FTimerHandle SpawnTimerHandle;
	

	// UFUNCTION()
	// TSubclassOf<AActor> GetValidClassForSpawn(int32 Counter = 0) const;
	// UFUNCTION()
	// void ReadySpawn() {bReadyForSpawn = true;}
};
