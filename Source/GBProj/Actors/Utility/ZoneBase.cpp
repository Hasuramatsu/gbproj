// Fill out your copyright notice in the Description page of Project Settings.


#include "ZoneBase.h"


// #include "GBProj/Game/GBAIControllerBase.h"
// #include "GBProj/Game/GBCharacter.h"
// #include "GBProj/Game/GBGameStateBase.h"
// #include "GBProj/Game/GBProjGameModeBase.h"
// #include "Kismet/GameplayStatics.h"

// // Sets default values
AZoneBase::AZoneBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	CollisionBox->SetupAttachment(RootComponent);

}
//
// // Called when the game starts or when spawned
void AZoneBase::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> Actors;
	GetOverlappingActors(Actors);
	for(auto& It : Actors)
	{
		ASpawner* Spawner = Cast<ASpawner>(It);
		if(Spawner)
		{
			Spawners.Add(Spawner);
			//Spawner->SetZone(ZoneName);
			//Spawner->OnSpawn.AddDynamic(this, &AZoneBase::RegisterEnemy);
		}
	}
}
//
// void AZoneBase::RegisterEnemy(AEnemyBase* NewEnemy)
// {
// 	Enemies.Add(NewEnemy);
// 	NewEnemy->OnEnemyDead.AddDynamic(this, &AZoneBase::RemoveEnemy);
// }
//
// void AZoneBase::RemoveEnemy(AEnemyBase* Enemy)
// {
// 	Enemies.Remove(Enemy);
// }
//
// void AZoneBase::Disengage()
// {
// 	if(!bIsActive)
// 	{
// 		for(auto& It : Enemies)
// 		{
// 			AGBAIControllerBase* Controller = Cast<AGBAIControllerBase>(It->GetController());
// 			if(Controller)
// 			{
// 				Controller->ClearTarget();
// 			}
// 		}
// 		GetWorld()->GetTimerManager().SetTimer(DestroyEnemiesTimerHandle, this, &AZoneBase::EraseEnemies, EraseTime, false);
// 	}
// }
//
// void AZoneBase::EraseEnemies()
// {
// 	if(!bIsActive)
// 	{
// 		TArray<AEnemyBase*> TempArray;
// 		for(auto& It : Enemies)
// 		{
// 			TempArray.Add(It);
// 		}
// 		for(auto& It : TempArray)
// 		{
// 			It->Execute_DeadEvent(It);
// 		}
// 	}
// }
//
// // Called every frame
	void AZoneBase::Tick(float DeltaTime)
	{
		Super::Tick(DeltaTime);
	}

// void AZoneBase::ActivateSpawner()
// {
// 	bool Result = false;
// 	int32 Counter = 0;
// 	if(bIsActive)
// 	{
// 		AGBProjGameModeBase* MyGameMode = Cast<AGBProjGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
// 		if(MyGameMode && Spawners.Num() > 0 && MyGameMode->CheckCountForSpawn())
// 		{	
// 			while(!Result)
// 			{
// 				const int32 Random = FMath::RandRange(0, Spawners.Num() - 1);
// 				if(Spawners[Random]->CheckCanSpawn())
// 				{
// 					Result = Spawners[Random]->AttemptToSpawn();
// 				}
// 				Counter++;
// 				if(Counter > 30)
// 				{
// 					UE_LOG(LogTemp, Warning, TEXT("AZoneBase::ActivateSpawner -- SpawnFailed"));
// 					Result = true;
// 				}
// 			}
// 		}
// 	}
// 	else
// 	{
// 		GetWorld()->GetTimerManager().ClearTimer(SpawnTimerHandle);
// 	}
// }
//
// void AZoneBase::NotifyActorBeginOverlap(AActor* OtherActor)
// {
// 	AGBCharacter* Character = Cast<AGBCharacter>(OtherActor);
// 	if(Character)
// 	{
// 		AGBGameStateBase* MyGameState = Cast<AGBGameStateBase>(UGameplayStatics::GetGameState(GetWorld()));
// 		if(MyGameState)
// 		{
// 			bIsActive = true;
// 			MyGameState->SetCurrentPlayerZone(ZoneName);
// 			GetWorld()->GetTimerManager().SetTimer(SpawnTimerHandle, this, &AZoneBase::ActivateSpawner, SpawnRate, true);
// 		}
// 	}
// }
//
// void AZoneBase::NotifyActorEndOverlap(AActor* OtherActor)
// {
// 	AGBCharacter* Character = Cast<AGBCharacter>(OtherActor);
// 	if(Character)
// 	{
// 		AGBGameStateBase* MyGameState = Cast<AGBGameStateBase>(UGameplayStatics::GetGameState(GetWorld()));
// 		if(MyGameState)
// 		{
// 			bIsActive = false;
// 			MyGameState->ClearCurrentPlayerZone();
//
// 			GetWorld()->GetTimerManager().SetTimer(StopChasingTimerHandle, this, &AZoneBase::Disengage, DisengageTime, false);
// 		}
// 	}
// }

