// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"


#include "GBProj/Game/GBProjGameModeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	DropComponent = CreateDefaultSubobject<UDropComponent>(TEXT("DropComponent"));
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

bool ASpawner::Spawn()
{
	bool Result = false;
	
	FVector Location = GetActorLocation();
	FRotator Rotation = GetActorRotation();
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	AActor* TempActor = Cast<AActor>(GetWorld()->SpawnActor(DropComponent->GetDropClass(), &Location, &Rotation, SpawnParameters));
	if(TempActor)
	{
		Result = true;
		
		if(bTriggerableSpawn && bTriggerOnce)
		{
			bSpawned = true;
		}
		
		AEnemyBase* TempEnemy = Cast<AEnemyBase>(TempActor);
		if(TempEnemy)
		{
			OnSpawn.Broadcast(TempEnemy);
			//GetWorld()->GetTimerManager().SetTimer(SpawnTimerHandle, this, &ASpawner::ReadySpawn, SpawnTimer, false);
		}
	}
	return Result;
}

bool ASpawner::TriggerSpawn()
{
	if(CheckCanSpawn())
	{
		return Spawn();
	}
	return false;
}

// TSubclassOf<AActor> ASpawner::GetValidClassForSpawn(int32 Counter) const
// {
// 	const int32 Loop = Counter + 1;
// 	TSubclassOf<AActor> TempClass;
// 	if(DropComponent && Loop < 10)
// 	{
// 		TempClass = DropComponent->GetDropClass();
// 		if(TempClass->IsChildOf(AEnemyBase::StaticClass()))
// 		{
// 			const TSubclassOf<AEnemyBase> EnemyClass = *TempClass;
// 			AGBProjGameModeBase* MyGameMode = Cast<AGBProjGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
// 			if(MyGameMode)
// 			{
// 				const bool Result = MyGameMode->IsValidEnemyForSpawn(EnemyClass);
// 				if(!Result)
// 				{
// 					TempClass = GetValidClassForSpawn(Loop);
// 				}
// 			}
//
// 		}
// 	}
// 	return TempClass;
// }

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool ASpawner::CheckCanSpawn() const
{
	bool Result = true;
	if(UGameplayStatics::GetPlayerPawn(GetWorld(), 0))
	{
		const float TempDistance = (UGameplayStatics::GetPlayerPawn(GetWorld(), 0)->GetActorLocation() - GetActorLocation()).Size();
		if((TempDistance < DeadRange) || bSpawned)
		{
			Result = false;
		}
	}
	return Result;
}

// bool ASpawner::AttemptToSpawn() 
// {
// 	bool Result = false;
// 	if(DropComponent)
// 	{
// 		const TSubclassOf<AActor> TempClass = GetValidClassForSpawn();
// 		if(TempClass)
// 		{
// 			Result = Spawn(TempClass);
// 		}
// 		else
// 		{
// 			UE_LOG(LogTemp, Warning, TEXT("ASpawner::AttemptToSpawn -- miss class template."))
// 		}
// 	}
// 	
// 	return Result;
// }

