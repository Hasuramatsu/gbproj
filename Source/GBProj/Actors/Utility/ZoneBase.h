// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "Spawner.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "ZoneBase.generated.h"

UCLASS()
class GBPROJ_API AZoneBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AZoneBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName ZoneName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DisengageTime = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EraseTime = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SpawnRate = 5.f;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	USceneComponent* SceneComponent = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent* CollisionBox = nullptr;
private:
	bool bIsActive = false;
	
	UPROPERTY()
	TArray<ASpawner*> Spawners;
	UPROPERTY()
	TArray<AEnemyBase*> Enemies;

	FTimerHandle StopChasingTimerHandle;
	FTimerHandle DestroyEnemiesTimerHandle;
	FTimerHandle SpawnTimerHandle;
	
	// UFUNCTION()
	// void RegisterEnemy(AEnemyBase* NewEnemy);
	// UFUNCTION()
	// void RemoveEnemy(AEnemyBase* Enemy);
	// UFUNCTION()
	// void Disengage();
	// UFUNCTION()
	// void EraseEnemies();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// UFUNCTION(BlueprintCallable)
	// void ActivateSpawner();
	//
	// virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	// virtual void NotifyActorEndOverlap(AActor* OtherActor) override;

	//Getters
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsActive() const {return bIsActive;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<ASpawner*> GetSpawners() const {return Spawners;}
};
