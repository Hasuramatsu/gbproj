// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "GBProj/Interfaces/InteractInterface.h"
#include "PickUp.generated.h"

class AMainCharacter;
UCLASS()
class GBPROJ_API APickUp : public AActor, public IInteractInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickUp();

	virtual  void NotifyActorBeginOverlap(AActor* OtherActor) override;
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
	UStaticMeshComponent* Mesh = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
	USphereComponent* CollisionSphere = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Info")
	FName PickupName;

	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Interact_Implementation(AActor* Interactor, bool Flag) override;

	virtual FDisplayInfo GetDisplayInfo_Implementation() override;

	UFUNCTION(BlueprintNativeEvent)
	void OnPickUp(const AMainCharacter* PickupBy);
};
