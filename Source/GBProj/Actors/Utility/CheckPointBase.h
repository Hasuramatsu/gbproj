// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "CheckPointBase.generated.h"

UCLASS()
class GBPROJ_API ACheckPointBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACheckPointBase();

	UFUNCTION(BlueprintPure)
	bool IsActivated() const {return bActive;};
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USceneComponent* SceneComponent = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCapsuleComponent* SpawnCapsule = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* StaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USphereComponent* CollisionSphere = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Flask")
	float FlaskChargeGainCoefficient = 0.2f;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
private:	
	bool bFirstImpact = true;

	bool bActive = false;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	//Getters
	UCapsuleComponent* GetSpawnCapsule() const {return SpawnCapsule;}

	//
	UFUNCTION(BlueprintImplementableEvent)
	void OnActivation();

};
