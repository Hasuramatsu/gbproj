// Fill out your copyright notice in the Description page of Project Settings.


#include "CheckPointBase.h"

#include "GBProj/Actors/MainCharacter.h"
#include "GBProj/Game/GBCharacter.h"
#include "GBProj/Game/GBPlayerStateBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACheckPointBase::ACheckPointBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetupAttachment(RootComponent);
	CollisionSphere->SetCollisionProfileName("OverlapOnlyPawn");
	CollisionSphere->SetSphereRadius(100.f);

	SpawnCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("SpawnCapsule"));
	SpawnCapsule->SetupAttachment(RootComponent);
	SpawnCapsule->SetCollisionProfileName("NoCollision");
	SpawnCapsule->SetGenerateOverlapEvents(false);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(SpawnCapsule);
	StaticMesh->SetCollisionProfileName("BlockAll");
	StaticMesh->SetGenerateOverlapEvents(false);

	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ACheckPointBase::OnOverlap);
}

// Called when the game starts or when spawned
void ACheckPointBase::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> Actors;
	TArray<AActor*> ActorsToIgnore;
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
	ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));
	UKismetSystemLibrary::SphereOverlapActors(GetWorld(), GetActorLocation(), 50.f, ObjectTypes, AMainCharacter::StaticClass(), ActorsToIgnore, Actors);

	if(Actors.Num() > 0)
	{
		AMainCharacter* Character = Cast<AMainCharacter>(Actors[0]);
		if(Character && !bActive)
		{
			AGBPlayerStateBase* CharacterPlayerState = Cast<AGBPlayerStateBase>(Character->GetPlayerState());
			if(CharacterPlayerState)
			{
				CharacterPlayerState->SetLastCheckPoint(this);
				bActive = true;
				OnActivation();
				if(bFirstImpact)
				{
					bFirstImpact = false;
					CharacterPlayerState->FlaskComponent->AddFlaskCharges(CharacterPlayerState->FlaskComponent->GetFlaskMaxCharge() * FlaskChargeGainCoefficient);
				}
			}
		}
	}
}

// Called every frame
void ACheckPointBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACheckPointBase::OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
 AMainCharacter* Character = Cast<AMainCharacter>(OtherActor);
	if(Character && !bActive)
	{
		AGBPlayerStateBase* CharacterPlayerState = Cast<AGBPlayerStateBase>(Character->GetPlayerState());
		if(CharacterPlayerState)
		{
			CharacterPlayerState->SetLastCheckPoint(this);
			bActive = true;
			OnActivation();
			if(bFirstImpact)
			{
				bFirstImpact = false;
				CharacterPlayerState->FlaskComponent->AddFlaskCharges(CharacterPlayerState->FlaskComponent->GetFlaskMaxCharge() * FlaskChargeGainCoefficient);
			}
		}
	}
	
}

