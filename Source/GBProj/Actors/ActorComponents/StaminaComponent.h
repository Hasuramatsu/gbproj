// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GBProj/AbilitySystem/GBAttributeSet.h"

#include "StaminaComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStaminaChange, float, Value);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GBPROJ_API UStaminaComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UStaminaComponent();

	//Vars

	//Delegate to call on stamina change. Parameter is final stamina value;
	UPROPERTY(BlueprintAssignable)
	FOnStaminaChange OnStaminaChange;

	//Functions
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// //Decrease stamina on value up to 0. Return false if 0;
	// bool DrainStamina(const float Value, float *LeechStamina = nullptr);
	//
	// //Decrease stamina on value if possible, otherwise return false;
	// bool PayStamina(const float Value);
	//
	// //Modify recovery time by rate -- CurrentRecoveryTime * Rate;
	// void ModifyRecoveryTimeByRate(float Rate);
	
	//Getters
	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetMaxStamina() const {return MaxStamina;}
	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetCurrentStamina() const {return CurrentStamina;}

	//Setters
	void SetStaminaAttribute(const FOnAttributeChangeData& Data);
	void SetMaxStaminaAttribute(const FOnAttributeChangeData& Data);

	
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stamina")
	float MaxStamina = 100.f;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
	// float RecoveryTime = 0.2f;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
	// float RecoveryAmount = 5.f;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
	// float RecoveryCooldown = 0.6f;

	// List of effects applied to the owner on BeginPlay
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Effects")
	TArray<TSubclassOf<UGameplayEffect>> PassiveEffects;
	
private:
	//Current amount of stamina. Init on begin play with MaxStamina;
	float CurrentStamina = MaxStamina;


	// //Set Stamina by value and broadcast it;
	// void SetCurrentStamina(float Value);
	//
	// //Stop regen and activate timer to start regen;
	// void InterruptRegen();
	
	//Timer for prevent regen;
	FTimerHandle CooldownTimer;
	//Allow regen
	void ActivateRegen() {SetComponentTickEnabled(true);}

	
};

