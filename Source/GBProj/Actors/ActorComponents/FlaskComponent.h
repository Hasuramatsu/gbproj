// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GBProj/Game/GBCharacter.h"
#include "Niagara/Public/NiagaraComponent.h"
#include "FlaskComponent.generated.h"

class UGameplayAbility;
class AGBCharacterBase;
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeactivate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnValueChange, float, Value);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GBPROJ_API UFlaskComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFlaskComponent();

	UPROPERTY(BlueprintAssignable)
	FOnDeactivate OnDeactivate;
	UPROPERTY(BlueprintAssignable)
	FOnValueChange OnValueChange;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Flask")
	float FlaskMaxCharge = 100.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Flask")
	float FlaskHealPerSecond = 30.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Flask")
	float FlaskChargeConsumeRate = 1.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Flask")
	float ConsumeChargePerTick = 1.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Flask")
	float FlaskChargeRespawnCoefficient = 0.2f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Flask")
	float FlaskTickRate = 0.05f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	UAnimMontage* GetFlaskMontage = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FX")
	UNiagaraSystem* HealFX = nullptr;
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability")
	TMap<FString, TSubclassOf<UGameplayAbility>> GrantedAbilities;
private:
	float CurrentFlaskCharges = FlaskMaxCharge;
	
	FTimerHandle FlaskTimerHandle;
	
	bool bFlaskHealing = false;
	UPROPERTY()
	AGBCharacterBase* User = nullptr;

	UPROPERTY()
	UNiagaraComponent* TempFX = nullptr;

	UFUNCTION()
	void FlaskTick();
	UFUNCTION()
    void DeactivateFlask();
public:	

	//Setters
	UFUNCTION(BlueprintCallable)
	void SetCurrentFlaskCharges(float Value) {CurrentFlaskCharges = Value; OnValueChange.Broadcast(CurrentFlaskCharges);}

	//Getters
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCurrentFlaskCharges() const {return CurrentFlaskCharges;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
    bool IsMaxCharges() const {return CurrentFlaskCharges >= FlaskMaxCharge;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
    float GetFlaskMaxCharge() const {return FlaskMaxCharge;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
    float GetFlaskChargeRespawnCoefficient() const {return FlaskChargeRespawnCoefficient;}

	//Func
	UFUNCTION(BlueprintCallable)
	void ActivateFlaskHeal(const bool Flag);
	UFUNCTION(BlueprintCallable)
    void AddFlaskCharges(float Value);

	UFUNCTION()
	void SetUser(APawn* NewUser);

	UFUNCTION()
	void RegisterAbility();
};
