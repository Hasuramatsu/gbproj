// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GBProj/AbilitySystem/GBAttributeSet.h"

#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFullHealth);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHealthChange, float, Health);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GBPROJ_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(BlueprintAssignable)
	FOnDead OnDead;
	UPROPERTY(BlueprintAssignable)
	FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintCallable)
	FOnFullHealth OnFullHealth;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DamageReaction")
	UAnimMontage* DamageReactionMontage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DamageReaction")
	float DamageThreshold = 40.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DamageReaction")
	int ReactionChance = 100;

	// Called when the game starts
	virtual void BeginPlay() override;


private:
	//Health
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Life", meta = (AllowPrivateAccess = "true"))
	float MaxHealth;
	float CurrentHealth;
	//Immune
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Life", meta = (AllowPrivateAccess = "true"))
	bool bCanBeImmune = true;
	bool bIsImmune = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Life", meta = (AllowPrivateAccess = "true", EditCondition="bCanPierce", ClampMin="0"))
	float ImmuneDuration = 0.5f;


	FTimerHandle ImmunityTimerHandle;
	
	void ModifyCurrentHealth(float Value);
	void EndImmune();
	void DamageReaction() const;
public:	

	UFUNCTION(BlueprintCallable)
	void BecomeImmune(float Duration);

	UFUNCTION(BlueprintCallable)
	bool ReceiveDamage(float Value);
	UFUNCTION(BlueprintCallable)
	void HealDamage(float Value);
	
	//Getters
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCurrentHealth() const {return CurrentHealth;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetMaxHealth() const {return MaxHealth;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetImmuneDuration() const {return ImmuneDuration;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsMaxHealth() const {return CurrentHealth >= MaxHealth;}

	//Setters
	void SetHealthAttribute(const FOnAttributeChangeData& Data);
	void SetMaxHealthAttribute(const FOnAttributeChangeData& Data);
};

