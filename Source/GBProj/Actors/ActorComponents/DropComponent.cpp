// Fill out your copyright notice in the Description page of Project Settings.


#include "DropComponent.h"

// Sets default values for this component's properties
UDropComponent::UDropComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UDropComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	CalculateTotalWeight();
}


void UDropComponent::CalculateTotalWeight()
{
	if(DropTable.Num() != 0)
	{
		for(auto& It : DropTable)
		{
			TotalWeight += It.Value;
		}
	}
}

// Called every frame
void UDropComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

TSubclassOf<AActor> UDropComponent::GetDropClass() const
{
	if(DropTable.Num() != 0)
	{
		float RandomValue = FMath::RandRange(0.f, TotalWeight);
		for(auto& It : DropTable)
		{
			if(RandomValue < It.Value)
			{
				return It.Key;
			}
			else
			{
				RandomValue -= It.Value;
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UDropComponent::GetDropActor -- No elements in drop table."));
	}
	return nullptr;
}
