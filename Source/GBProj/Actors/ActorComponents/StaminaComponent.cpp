// Fill out your copyright notice in the Description page of Project Settings.


#include "StaminaComponent.h"

#include "AbilitySystemInterface.h"

// Sets default values for this component's properties
UStaminaComponent::UStaminaComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	//SetComponentTickInterval(RecoveryTime);
	// ...
}


void UStaminaComponent::SetStaminaAttribute(const FOnAttributeChangeData& Data)
{
	CurrentStamina = Data.NewValue;
	OnStaminaChange.Broadcast(CurrentStamina);
}

void UStaminaComponent::SetMaxStaminaAttribute(const FOnAttributeChangeData& Data)
{
	MaxStamina = Data.NewValue;
	OnStaminaChange.Broadcast(CurrentStamina);
}

// Called when the game starts
void UStaminaComponent::BeginPlay()
{
	Super::BeginPlay();

	UAbilitySystemComponent* ASC = Cast<UAbilitySystemComponent>(GetOwner()->FindComponentByClass(UAbilitySystemComponent::StaticClass()));
	if(ASC)
	{
		for(auto& It : PassiveEffects)
		{
			FGameplayEffectContextHandle EffectContext = ASC->MakeEffectContext();
			EffectContext.AddSourceObject(this);
			FGameplayEffectSpecHandle NewHandle = ASC->MakeOutgoingSpec(It, 1, EffectContext);
			if(NewHandle.IsValid())
			{
				FActiveGameplayEffectHandle ActiveGEHandle = ASC->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), ASC);
			}
		
		}
	}


	//CurrentStamina = MaxStamina;
	
}

// void UStaminaComponent::SetCurrentStamina(float Value)
// {
// 	CurrentStamina = Value;
// 	OnStaminaChange.Broadcast(CurrentStamina);
// }
//
// void UStaminaComponent::InterruptRegen()
// {
// 	SetComponentTickEnabled(false);
// 	GetWorld()->GetTimerManager().SetTimer(CooldownTimer, this, &UStaminaComponent::ActivateRegen, RecoveryCooldown, false);
// }


// Called every frame
void UStaminaComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	//
	// if(CurrentStamina < MaxStamina)
	// {
	// 	SetCurrentStamina(CurrentStamina + RecoveryAmount);
	// 	if(CurrentStamina >= MaxStamina)
	// 	{
	// 		if(CurrentStamina > MaxStamina)
	// 		{
	// 			SetCurrentStamina(MaxStamina);
	// 		}
	// 		SetComponentTickEnabled(false);
	// 	}
	// }

}

// bool UStaminaComponent::DrainStamina(const float Value, float* LeechStamina)
// {
// 	if(CurrentStamina - Value > 0)
// 	{
// 		SetCurrentStamina(CurrentStamina - Value);
// 		InterruptRegen();
// 		if(LeechStamina)
// 		{
// 			*LeechStamina += Value;
// 		}
// 		return true;
// 	}
// 	else
// 	{
// 		if(LeechStamina)
// 		{
// 			*LeechStamina += CurrentStamina;
// 		}
// 		SetCurrentStamina(0);
// 		InterruptRegen();
// 		return false;
// 	}
// }
//
// bool UStaminaComponent::PayStamina(const float Value)
// {
// 	if(CurrentStamina - Value > 0)
// 	{
// 		SetCurrentStamina(CurrentStamina - Value);
// 		InterruptRegen();
// 		return true;
// 	}
// 	return false;
// }
//
// void UStaminaComponent::ModifyRecoveryTimeByRate(float Rate)
// {
// 	SetComponentTickInterval(RecoveryTime * Rate);
// }