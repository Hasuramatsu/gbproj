// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{

	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


int32 UInventoryComponent::AddInitedWeapon(AWeaponBase* Weapon)
{
	return InitializedWeapons.Add(Weapon);
}

AWeaponBase* UInventoryComponent::GetWeaponByIndex(int32 Index) const
{
	return InitializedWeapons.Num() > Index ? InitializedWeapons[Index] : nullptr;
}

int32 UInventoryComponent::GetIndexByWeapon(AWeaponBase* Weapon)
{
	return InitializedWeapons.Contains(Weapon) ? InitializedWeapons.Find(Weapon) : INDEX_NONE;
}

void UInventoryComponent::DestroyAllWeapon()
{
	for(auto& It : InitializedWeapons)
	{
		if(It)
		{
			It->Destroy();
		}
	}
	InitializedWeapons.Empty();
}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

}


