// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SetupBTComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GBPROJ_API USetupBTComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USetupBTComponent();

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AISetup")
	float SightRadius = 1200.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AISetup")
	float LoseSightRadius = 1500.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AISetup")
	float SightAngle = 90.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AISetup")
	float HearingRange = 200.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FlyingNearCharacter")
	float DistanceForCharacter = 400.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "MoveTo")
	float ForceStrength = 5000.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "MoveTo")
	float DeadTime = 10.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "MoveTo")
	float DeadZone = 35.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FindTargetLocation")
	float MinTravelDistance = 100.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FindTargetLocation")
	float MaxTravelDistance = 300.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FindTargetLocation")
	float MinHeightFromFloor = 50.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FindTargetLocation")
	float MaxHeightFromFloor = 500.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FindTargetLocation")
	float MaxDistanceToRoof = 50.f;
	
	
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
