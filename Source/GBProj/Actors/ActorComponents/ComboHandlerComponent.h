// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameplayAbilitySpec.h"
#include "Components/ActorComponent.h"
#include "GBProj/Func/Types.h"
#include "ComboHandlerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInputHandled, FGameplayAbilitySpecHandle, AbilitySpecHandle);




UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GBPROJ_API UComboHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UComboHandlerComponent();

	UPROPERTY(BlueprintAssignable)
	FOnInputHandled OnInputHandled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	float DisableInputTime = 0.5f;
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Register weapon abilities for owner if it implement AbilityInterface
	UFUNCTION(BlueprintCallable)
	void RegisterWeapon(AWeaponBase* Primary, AWeaponBase* Secondary);

	//Culled by owner input functions
	UFUNCTION(BlueprintCallable)
	void HandleInput(EInputType Input);

	UFUNCTION(BlueprintPure)
	TArray<FComboHandle> GetPossibleCombos() const {return PossibleCombos;}

	UFUNCTION(BlueprintPure)
	bool IsInCombo() const;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ComboTime")
	float TimeToEndCombo = 2.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool bDebug = false;
private:

	bool bCanHandle = true;

	TMap<FComboHandle,FGameplayAbilitySpecHandle> AddedAbilities;
	
	TArray<EInputType> CurrentCombo;

	TArray<FComboHandle> PossibleCombos; 
	
	// Current combo count
	int32 ComboCount = 0;

	// Time since input
	float LastInputTime = 0.f;

	FTimerHandle DisablerHandle;
	
	// Nullify LastInputTime, ComboCount and disable tick
	void EndCombo();

	// Enable input
	void EnableHandle();
};
