// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GBProj/Actors/Weapon/MeleeWeaponBase.h"
#include "GBProj/Actors/Weapon/RangedWeaponBase.h"

#include "InventoryComponent.generated.h"



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GBPROJ_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

	// Weapon carried by character
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapons")
	TArray<TSubclassOf<AWeaponBase>> HoldWeapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapons")
	TSubclassOf<AMeleeWeaponBase> MeleeWeapon = nullptr;

	// Add inited weapon in array for future access
	UFUNCTION(BlueprintCallable)
	int32 AddInitedWeapon(AWeaponBase* Weapon);
	
	//Return inited weapon ptr or nullptr if index invalid
	UFUNCTION(BlueprintPure)
	AWeaponBase* GetWeaponByIndex(int32 Index) const;

	// Return index of weapon in array or INDEX_None if weapon not found
	UFUNCTION(BlueprintCallable)
	int32 GetIndexByWeapon(AWeaponBase* Weapon);

	UFUNCTION(BlueprintCallable)
	void DestroyAllWeapon();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


private:
	
	UPROPERTY()
	TArray<AWeaponBase*> InitializedWeapons;




};

