// Fill out your copyright notice in the Description page of Project Settings.


#include "FlaskComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "GBProj/Actors/GBCharacterBase.h"

#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UFlaskComponent::UFlaskComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UFlaskComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	CurrentFlaskCharges = FlaskMaxCharge;
}


void UFlaskComponent::FlaskTick()
{
	const bool CaseOne = CurrentFlaskCharges > 0 && bFlaskHealing;
	//const bool CaseTwo = Character && Character->HealthComponent;
	//const bool CaseThree = !Character->HealthComponent->IsMaxHealth();
	if(CaseOne) // && CaseTwo && CaseThree
	{
		if(CurrentFlaskCharges - FlaskHealPerSecond*FlaskTickRate*FlaskChargeConsumeRate > 0)
		{
			//Character->HealthComponent->HealDamage(FlaskHealPerSecond*FlaskTickRate*FlaskChargeConsumeRate);
			CurrentFlaskCharges -=  FlaskHealPerSecond*FlaskTickRate*FlaskChargeConsumeRate;
			OnValueChange.Broadcast(CurrentFlaskCharges);
		}
		else
		{
			//Character->HealthComponent->HealDamage(CurrentFlaskCharges);
			CurrentFlaskCharges = 0;
			OnValueChange.Broadcast(CurrentFlaskCharges);
			DeactivateFlask();
			return;
		}
		// if(Character->HealthComponent->IsMaxHealth())
		// {
		// 	DeactivateFlask();
		// }
	}
	else
	{
		DeactivateFlask();
	}
}

void UFlaskComponent::DeactivateFlask()
{
	bFlaskHealing = false;
	if(TempFX)
	{
		TempFX->DestroyComponent();
	}
	GetWorld()->GetTimerManager().ClearTimer(FlaskTimerHandle);
	//Character = nullptr;
	OnDeactivate.Broadcast();
}


void UFlaskComponent::ActivateFlaskHeal(const bool Flag)
{
	if(Flag)
	{
		AGBCharacterBase* MyCharacter = Cast<AGBCharacterBase>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		if(MyCharacter && MyCharacter->HealthComponent)
		{
			//Character = MyCharacter;
			bFlaskHealing = true;
			TempFX = UNiagaraFunctionLibrary::SpawnSystemAttached(HealFX, MyCharacter->GetMesh(), FName("FlaskSocket"), FVector::ZeroVector, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			GetWorld()->GetTimerManager().SetTimer(FlaskTimerHandle, this, &UFlaskComponent::FlaskTick, FlaskTickRate, true);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("AGBPlayerStateBase::ActivateFlaskHeal -- can't cast to GBCharacter"));
		}
	}
	else
	{
		DeactivateFlask();
	}
}

void UFlaskComponent::AddFlaskCharges(float Value)
{
	CurrentFlaskCharges = FMath::Clamp(CurrentFlaskCharges + Value, 0.f, FlaskMaxCharge);
	if(CurrentFlaskCharges < ConsumeChargePerTick)
	{
		OnDeactivate.Broadcast();
	}
	OnValueChange.Broadcast(CurrentFlaskCharges);
}

void UFlaskComponent::SetUser(APawn* NewUser)
{
	User = Cast<AGBCharacterBase>(NewUser);
}

void UFlaskComponent::RegisterAbility()
{
	if(User)
	{
		User->RegisterNewAbilities(GrantedAbilities);
	}
}

