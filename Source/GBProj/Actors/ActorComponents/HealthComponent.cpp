// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"

#include "GameFramework/Character.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

    CurrentHealth = MaxHealth;
	OnHealthChange.Broadcast(MaxHealth);
	// ...
	
}


void UHealthComponent::BecomeImmune(float Duration)
{
	bIsImmune = true;
	GetWorld()->GetTimerManager().SetTimer(ImmunityTimerHandle, this, &UHealthComponent::EndImmune, Duration);
}

bool UHealthComponent::ReceiveDamage(float Value)
{
	if(!bIsImmune)
	{
		ModifyCurrentHealth(-Value);
		if(Value >= DamageThreshold)
		{
			if(FMath::RandRange(0, 100) <= ReactionChance)
			{
				DamageReaction();
			}
		}
		if(bCanBeImmune)
		{
			BecomeImmune(ImmuneDuration);
		}
	}
	else
	{
		return false;
	}
	return true;
}

void UHealthComponent::HealDamage(float Value)
{
	ModifyCurrentHealth(Value);
}

void UHealthComponent::SetHealthAttribute(const FOnAttributeChangeData& Data)
{
	CurrentHealth = Data.NewValue;
	OnHealthChange.Broadcast(CurrentHealth);
	if(IsMaxHealth())
	{
		OnFullHealth.Broadcast();
	}
	if(CurrentHealth == 0)
	{
		OnDead.Broadcast();
	}
}

void UHealthComponent::SetMaxHealthAttribute(const FOnAttributeChangeData& Data)
{
	MaxHealth = Data.NewValue;
	OnHealthChange.Broadcast(CurrentHealth);
}

void UHealthComponent::ModifyCurrentHealth(float Value)
{
	CurrentHealth += Value;
	if(CurrentHealth > MaxHealth)
	{
		CurrentHealth = MaxHealth;
	}
	if(CurrentHealth <= 0)
	{
		CurrentHealth = 0;
		OnDead.Broadcast();
	}
	OnHealthChange.Broadcast(CurrentHealth);
}

void UHealthComponent::EndImmune()
{
	bIsImmune = false;
}

void UHealthComponent::DamageReaction() const
{
	ACharacter* MyCharacter = (ACharacter*)(GetOwner());
	if(MyCharacter && DamageReactionMontage)
	{
		MyCharacter->PlayAnimMontage(DamageReactionMontage);
	}
}

