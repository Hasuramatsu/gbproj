// Fill out your copyright notice in the Description page of Project Settings.


#include "ComboHandlerComponent.h"
#include "AbilitySystemInterface.h"
#include "AbilitySystemComponent.h"
#include "GameplayAbilitySpec.h"
#include "GBProj/Game/GBGameInstance.h"


// Sets default values for this component's properties
UComboHandlerComponent::UComboHandlerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	

	// ...
}


bool UComboHandlerComponent::IsInCombo() const
{
	return ComboCount > 0;
}

// Called when the game starts
void UComboHandlerComponent::BeginPlay()
{
	Super::BeginPlay();


	SetComponentTickEnabled(false);
	// ...
	
}

void UComboHandlerComponent::EndCombo()
{
	SetComponentTickEnabled(false);
	LastInputTime = 0;
	ComboCount = 0;
	CurrentCombo.Empty();
	PossibleCombos.Empty();

	bCanHandle = false;
	GetWorld()->GetTimerManager().SetTimer(DisablerHandle, this, &UComboHandlerComponent::EnableHandle, DisableInputTime, false);
}

void UComboHandlerComponent::EnableHandle()
{
	bCanHandle = true;
}


// Called every frame
void UComboHandlerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(LastInputTime < TimeToEndCombo)
	{
		LastInputTime += DeltaTime;
	}
	else
	{
		EndCombo();
	}
}

void UComboHandlerComponent::RegisterWeapon(AWeaponBase* Primary, AWeaponBase* Secondary)
{
	if(Primary && Secondary && GetOwner()->GetClass()->ImplementsInterface(UAbilitySystemInterface::StaticClass()))
	{
		UAbilitySystemComponent* ASC = Cast<IAbilitySystemInterface>(GetOwner())->GetAbilitySystemComponent();
		if(ASC)
		{
			// Clear old abilities from owner
			for(auto& It : AddedAbilities)
			{
				ASC->SetRemoveAbilityOnEnd(It.Value);
			}
			
			// Get valid game instance
			UGBGameInstance* MyGI = Cast<UGBGameInstance>(GetWorld()->GetGameInstance());
			if(MyGI)
			{
				//Create ability pool
				TMap<FComboHandle, TSubclassOf<UGameplayAbility>> WeaponAbilities;

				// Pull abilities from data table
				if(MyGI->GetComboInfo(Primary->GetClass(), Secondary->GetClass(), WeaponAbilities))
				{
					// Create abilities
					TMap<FComboHandle, FGameplayAbilitySpec> NewSpecs;
					for(auto& It : WeaponAbilities)
					{
						if(It.Value.Get())
						{
							NewSpecs.Add(It.Key, FGameplayAbilitySpec(It.Value, 1, INDEX_NONE, this));
						}
					}

					// Grant abilities to character
					for(auto& It : NewSpecs)
					{
						FGameplayAbilitySpecHandle& SpecHandle = AddedAbilities.FindOrAdd(It.Key);

						if(!SpecHandle.IsValid())
						{
							SpecHandle = ASC->GiveAbility(It.Value);
						}
					}
				}
			}
		}
	}
}

void UComboHandlerComponent::HandleInput(EInputType Input)
{
	if(bCanHandle) // TODO limit during attack
	{
		LastInputTime = 0.f;
		SetComponentTickEnabled(true);
		CurrentCombo.Add(Input);
		ComboCount++;
		const FComboHandle NewCombo(CurrentCombo);

		if(bDebug)
		{
			FString String;
			for(auto& It : CurrentCombo)
			{
				String += FString::Printf(TEXT("%hhd "), It);
			}
			GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Red, String);
		}

		if(AddedAbilities.Contains(NewCombo))
		{
			OnInputHandled.Broadcast(AddedAbilities[NewCombo]);
		}
		else if(bDebug)
		{
			GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Red, TEXT("UComboHandlerComponent::HandleInput -- Wrong InputCombo"));
		}

		if(ComboCount == 1)
		{
			AddedAbilities.GetKeys(PossibleCombos);
		}

		//TODO while loop with for iterators with remove

		bool IsComplete = false;
		while(!IsComplete)
		{
			IsComplete = true;
			int32 Iterator = -1;
			for(auto& It : PossibleCombos)
			{
				if(It.Combo.Num() <= ComboCount || It.Combo[ComboCount - 1] != Input)
				{				
					Iterator = PossibleCombos.Find(It);
					IsComplete = false;
					break;
				}
			}

			if(Iterator >= 0)
			{
				PossibleCombos.RemoveAt(Iterator);
			}
		}

		if(PossibleCombos.Num() == 0)
		{
			EndCombo();
		}
		// TArray<int32> IndexesToRemove;
		// for(auto& It : PossibleCombos)
		// {
		// 	if(It.Combo.Num() <= ComboCount || It.Combo[ComboCount - 1] != Input)
		// 	{				
		// 		IndexesToRemove.Add(PossibleCombos.Find(It)); // Do this because .Remove(It) throw execution(but still work)
		// 	}
		// }
		//
		// for(int32 It : IndexesToRemove)
		// {
		// 	PossibleCombos.RemoveAt(It);
		// 	if(PossibleCombos.Num() == 0)
		// 	{
		// 		EndCombo();
		// 	}
		// }
	}
}

