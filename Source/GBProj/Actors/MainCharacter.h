// Main Character class.

#pragma once

#include "CoreMinimal.h"
#include "GBCharacterBase.h"
#include "ActorComponents/ComboHandlerComponent.h"
#include "ActorComponents/InventoryComponent.h"
#include "ActorComponents/StaminaComponent.h"
//#include "GBProj/Game/GBPlayerControllerBase.h"

#include "MainCharacter.generated.h"

//class AGBPlayerController;
DECLARE_DELEGATE_OneParam(FBoolInput, const bool);
DECLARE_DELEGATE_OneParam(FIntInput, const int32);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponSwap);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCombatStateChanged, ECombatState, NewState);

//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCharacterDead);


/**
 * 
 */
UCLASS()
class GBPROJ_API AMainCharacter : public AGBCharacterBase
{
	GENERATED_BODY()

public:

	//Constructor
	AMainCharacter();

	//Components
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	UStaminaComponent* StaminaComponent = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	UInventoryComponent* InventoryComponent = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = Components)
	UComboHandlerComponent * ComboHandlerComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* GBCamera;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	//Camera settings
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	float CameraArmLength = 600.f;

	// Setup control
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	//Getters

	//Return pitch angle for aim
	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetPitchAngle() const {return PitchAngle;}
	
	//Return current interaction actor
	UFUNCTION(BlueprintPure)
	FORCEINLINE AActor* GetCurrentInteractable() const {return InteractionTarget;}

	UFUNCTION(BlueprintPure)
	AWeaponBase* GetPrimaryWeaponRef() const {return PrimaryWeapon;}
	UFUNCTION(BlueprintPure)
	AWeaponBase* GetSecondaryWeaponRef() const {return SecondaryWeapon;}
	
	// Can actor interact?
	UFUNCTION(BlueprintPure)
	FORCEINLINE bool CanInteract() const {return bCanInteract;}

	// Return AGBPlayerController
	UFUNCTION(BlueprintPure)
	FORCEINLINE class AGBPlayerController* GetMyController() const {return MyController;}

	// Return current combat state
	UFUNCTION(BlueprintPure)
	FORCEINLINE ECombatState GetCombatState() const {return CombatState;}

	// Set combat state
	UFUNCTION(BlueprintCallable)
	void SetCombatState(const ECombatState State);

	//Debug
	UFUNCTION(BlueprintPure)
	float GetAxisY() const {return AxisY;}

	// Animation things

		// Pushing
	UFUNCTION(BlueprintPure, meta=(BlueprintThreadSafe))
	FORCEINLINE float GetPushDirection() const {return PushDirection;}

	UFUNCTION(BlueprintCallable)
	void SetGBController(AGBPlayerController* GBController) {MyController = GBController;}


	// Input functions
	UFUNCTION(BlueprintCallable)
	void InputAttack(const bool Flag);
	UFUNCTION(BlueprintCallable)
	void Reload();
	UFUNCTION(BlueprintCallable)
	void InputAxisY(float Value);
	UFUNCTION(BlueprintCallable)
	void Interact(const bool Flag);
	UFUNCTION(BlueprintCallable)
	void InputJump(const bool Flag);
	UFUNCTION(BlueprintCallable)
	void DownInput();
	UFUNCTION(BlueprintCallable)
	void SecondaryAction(const bool Flag);
	UFUNCTION(BlueprintCallable)
	void Dodge();
	UFUNCTION(BlueprintCallable)
	void InputFlaskUse(const bool Flag);

	//Interaction
	UFUNCTION(BlueprintCallable)
	void AddInteractable(AActor* Interactable);
	UFUNCTION(BlueprintCallable)
	void RemoveInteractable(AActor* Interactable);
		//Set New interactable without check
	UFUNCTION(BlueprintCallable)
	void ForceSetInteractable(AActor* Interactable);

	//Weapon change func
	UFUNCTION(BlueprintCallable)
	void SwapCurrentWeapon(AWeaponBase* NewWeapon);
	
	// Delegate for character death
	//UPROPERTY(BlueprintAssignable)
	//FCharacterDead CharacterDead;

	UPROPERTY(BlueprintAssignable)
	FOnWeaponSwap OnWeaponSwap;

	// Called when change between aim and normal stance
	UPROPERTY(BlueprintAssignable)
	FOnCombatStateChanged OnCombatStateChanged;
	
	// Overriden function for get FlaskComponent from PlayerState
	virtual UFlaskComponent* GetFlaskComponent() const override;

	virtual bool CanJumpInternal_Implementation() const override;

	virtual void OnJumped_Implementation() override;

	//virtual AWeaponBase* GetCurrentWeapon() const override;

	virtual FVector GetObjectLocation() const override;

protected:
	// Called when the game starts or when spawned
	UFUNCTION()
	virtual void BeginPlay() override;
	// Called every frame
	UFUNCTION()
	virtual void Tick(float DeltaTime) override;

	//Tick functions
	// Control character rotation
	void RotationTick(float DeltaTime);
	// Control character movement
	void MovementTick(float DeltaTime);


	virtual void DeadEvent() override;

	virtual void AttackStart() override;
	virtual void AttackEnd() override;


	// Delay for input count as hold
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	float DelayHold = 0.2f;
	
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* SceneComponent;

	//Handle movement input;
	float AxisY;
	float PitchAngle;

	// Animation things
	float PushDirection = 0.f;

	// Weapon ptr
	UPROPERTY()
	AWeaponBase* PrimaryWeapon = nullptr;
	UPROPERTY()
	AWeaponBase* SecondaryWeapon = nullptr;

	int32 CurrentWeaponIndex = 0;

	UFUNCTION(BlueprintCallable)
	void SetPrimaryWeapon(AWeaponBase* NewWeapon);

	UFUNCTION(BlueprintCallable)
	void SetSecondaryWeapon(AWeaponBase* NewWeapon);

	//Combat state for aim stance
	ECombatState CombatState = ECombatState::MeleeState;

	//Keep player controller for fast access
	UPROPERTY()
	class AGBPlayerController* MyController = nullptr;

	//Weapon
	// Slot of active weapon
	UPROPERTY()
	ECurrentWeaponType CurrentWeaponType = ECurrentWeaponType::Primary;

	// Interaction
	// Current interaction target
	UPROPERTY()
	AActor* InteractionTarget = nullptr;

	// List of current interaction targets
	UPROPERTY()
	TArray<AActor*> InteractionArray;

	//Flag for interaction possibility
	bool bCanInteract = true;

	UPROPERTY()
	FVector CursorLocation;

	// Some input logic

	// Booleans for prevent timer override
	bool bBlockPrimaryInput = false;
	bool bBlockSecondaryInput = false;
		
	FTimerHandle InputTimerHandler;
	void PrimaryTimerFunc();
	void SecondaryTimerFunc();
	
	//Called on death
	UFUNCTION()
	void DestroyAllWeapon();

	virtual void Destroyed() override;
	

	//Initializating weapon
	bool InitPrimaryWeapon();
	bool InitSecondaryWeapon(int32 WeaponIndex);


	//Called on FlaskComponent OutOfCharge delegate
	UFUNCTION()
	void OnEmptyFlask();

	//Called on HealthComponent OnFullHealth delegate
	UFUNCTION()
	void OnFullHealth();

	//Override landed function
	virtual void Landed(const FHitResult& Hit) override;

	virtual void OnBlockAllTag(const FGameplayTag Tag, int32 Count) override;

	// Use ability in response to ComboHandle deleagte
	UFUNCTION()
	void UseAttack(FGameplayAbilitySpecHandle Handle);


	// Set interaction target and execute additional logic
	void UpdateInteractionTarget(AActor* Interactable);
	
};

