// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GBCharacterBase.h"
#include "Animation/AnimInstance.h"
#include "GBAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API UGBAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure)
	AGBCharacterBase* TryGetOwnerCharacter() const;

	UFUNCTION(BlueprintImplementableEvent)
	void OnStun(float Duration);

	// Is owner character alive?
	UFUNCTION(BlueprintPure, meta=(BlueprintThreadSafe))
	FORCEINLINE bool IsAlive() const {return bIsAlive;}

	
protected:

	// Base Character
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	AGBCharacterBase* OwnerCharacter = nullptr;

	virtual void NativeBeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void OwnerDead_BP();

	
private:
	
	bool bIsAlive = true;

	UFUNCTION()
	void OwnerDead(AGBCharacterBase* DeadChar);
	
};
