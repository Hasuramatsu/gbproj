// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacter.h"

#include "GameplayTagsManager.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/GameModeBase.h"
#include "GameFramework/SpringArmComponent.h"
#include "GBProj/Game/GBHUD.h"
#include "GBProj/Game/GBPlayerControllerBase.h"
#include "GBProj/Game/GBPlayerStateBase.h"
#include "GBProj/Interfaces/InteractInterface.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

class AGBPlayerStateBase;

AMainCharacter::AMainCharacter()
{

	// Setup spring arm component
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = CameraArmLength;
	CameraBoom->bDoCollisionTest = false;

	// Setup camera
	GBCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	GBCamera->SetupAttachment(CameraBoom);
	GBCamera->bUsePawnControlRotation = false;

	//Scene component
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SceneComponent->SetupAttachment(RootComponent);

	
	StaminaComponent = CreateDefaultSubobject<UStaminaComponent>(TEXT("StaminaComponent"));
	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));

	ComboHandlerComponent = CreateDefaultSubobject<UComboHandlerComponent>(TEXT("ComboHandlerComponent"));
	ComboHandlerComponent->OnInputHandled.AddDynamic(this, &AMainCharacter::UseAttack);
	
}

UFlaskComponent* AMainCharacter::GetFlaskComponent() const
{
	UFlaskComponent* Component = nullptr;
	AGBPlayerStateBase* PS = Cast<AGBPlayerStateBase>(GetPlayerState());
	if(PS)
	{
		Component = PS->FlaskComponent;
	}
	return Component;
}

bool AMainCharacter::CanJumpInternal_Implementation() const
{
	if(!Super::CanJumpInternal_Implementation())
	{
		return false;
	}

	return true;
}

void AMainCharacter::OnJumped_Implementation()
{
	Super::OnJumped_Implementation();

}


FVector AMainCharacter::GetObjectLocation() const
{
	return CursorLocation;
}

void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	if(HealthComponent)
	{
		HealthComponent->OnFullHealth.AddDynamic(this, &AMainCharacter::OnFullHealth);
	}
	
	if(StaminaComponent)
	{
		//Bind attributes to stamina component
		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetStaminaAttribute()).AddUObject(StaminaComponent, &UStaminaComponent::SetStaminaAttribute);
		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxStaminaAttribute()).AddUObject(StaminaComponent, &UStaminaComponent::SetMaxStaminaAttribute);
	}

	SetPrimaryWeapon(InitWeapon(InventoryComponent->MeleeWeapon));
	if(PrimaryWeapon)
	{
		SwapCurrentWeapon(PrimaryWeapon);
	}

	for(auto& It : InventoryComponent->HoldWeapon)
	{
		AWeaponBase* NewWeapon = InitWeapon(It);
		if(NewWeapon)
		{
			InventoryComponent->AddInitedWeapon(NewWeapon);
		}
	}
	SetSecondaryWeapon(InventoryComponent->GetWeaponByIndex(0));

	//GetFlaskComponent()->OnDeactivate.AddDynamic(this, &AMainCharacter::OnEmptyFlask);
}

void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	RotationTick(DeltaTime);
	MovementTick(DeltaTime);
}

void AMainCharacter::RotationTick(float DeltaTime)
{
	if(MyController && IsAlive())
	{
		//TODO pushing and climbing case
		if(CanRotate())
		{
			if(CombatState == ECombatState::AimState)
			{
				//Check cursor position and set pitch angle and character rotation 
				FHitResult HitResult;
				MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, HitResult);
				CursorLocation = HitResult.Location;
				const float Yaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), CursorLocation).Yaw;
				const float CurrentYaw = GetActorRotation().Yaw;
				const float NewYaw = FMath::FInterpTo(CurrentYaw, Yaw, DeltaTime, 20);
				SetActorRotation(FRotator(0.f, NewYaw, 0.f));
				PitchAngle = UKismetMathLibrary::FindLookAtRotation(SceneComponent->GetComponentLocation(), CursorLocation).Pitch;
			}
			else
			{
				const float CurrentYaw = GetActorRotation().Yaw;
				float TargetYaw = CurrentYaw;
				if(AxisY > 0)
				{
					TargetYaw = 90;
				}
				else if(AxisY < 0)
				{
					TargetYaw = -90;
				}
				const float NewYaw = FMath::FInterpTo(CurrentYaw, TargetYaw, DeltaTime, 100);
				SetActorRotation(FRotator(0, NewYaw, 0));
				PitchAngle = 0.f;
			}
		}
	}
}

void AMainCharacter::MovementTick(float DeltaTime)
{
	if(IsAlive())
	{
		if(AbilitySystemComponent->HasMatchingGameplayTag(UGameplayTagsManager::Get().RequestGameplayTag(FName("InAction.Push"))))
		{
			if(AbilitySystemComponent->HasMatchingGameplayTag(UGameplayTagsManager::Get().RequestGameplayTag(FName("InAction.Grab"))) && InteractionTarget)
			{
				const float Offset = AxisY * DeltaTime * GetCharacterMovement()->MaxWalkSpeed;
				InteractionTarget->AddActorWorldOffset(FVector(0.f, Offset, 0.f), true);
				if(bDebug)
				{
					GEngine->AddOnScreenDebugMessage(-1, 0.f, FColor::Yellow, TEXT("AMainCharacter::MovementTick -- Grabbing"));
				}
			}
			else if(InteractionTarget && AxisY * GetActorForwardVector().Y > 0)
			{
				const float Offset = AxisY * DeltaTime * GetCharacterMovement()->MaxWalkSpeed;
				InteractionTarget->AddActorWorldOffset(FVector(0.f, Offset, 0.f), true);
				if(bDebug)
				{
					GEngine->AddOnScreenDebugMessage(-1, 0.f, FColor::Green, TEXT("AMainCharacter::MovementTick -- Pushing"));
				}
			}
			else
			{
				if(bDebug)
				{
					GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, TEXT("AMainCharacter::MovementTick -- Cancel"));
				}
				const FGameplayTag Tag = UGameplayTagsManager::Get().RequestGameplayTag(FName("Event.Push.Cancel"));
				const FGameplayEventData Data;
				AbilitySystemComponent->HandleGameplayEvent(Tag, &Data);
			}

			//Choose right pushing animation //TODO Maybe move into ability
			if(AxisY != 0)
			{
				if((AxisY > 0) == (GetActorForwardVector().Y > 0))
				{
					PushDirection = 1.f;
				}
				else
				{
					PushDirection = -1.f;
				}
			}
			else
			{
				PushDirection = 0.f;	
			}
		}
		else
		{
			if(CanMove())
			{
				AddMovementInput(FVector(0, 1, 0), AxisY);
			}
		}
	}
}



void AMainCharacter::DestroyAllWeapon()
{
	if(PrimaryWeapon)
	{
		PrimaryWeapon->Destroy();
	}
	
	InventoryComponent->DestroyAllWeapon();
}

void AMainCharacter::Destroyed()
{
	DestroyAllWeapon();
	
	Super::Destroyed();
}

void AMainCharacter::SwapCurrentWeapon(AWeaponBase* NewWeapon)
{
	if(NewWeapon)
	{
		if(CurrentWeapon == nullptr)
		{
			CurrentWeapon = NewWeapon;
			CurrentWeapon->ActivateWeapon(true);
		}
		else if(CurrentWeapon != NewWeapon)
		{
			CurrentWeapon->ActivateWeapon(false);
			if(CurrentWeapon->AddedAbilities.Contains("PrimaryAbility"))
			{
				AbilitySystemComponent->CancelAbilityHandle(CurrentWeapon->AddedAbilities["PrimaryAbility"]);
			}
			
			CurrentWeapon = NewWeapon;
			CurrentWeapon->ActivateWeapon(true);
			OnWeaponSwap.Broadcast();
		}
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, TEXT("AMainCharacter::SwapCurrentWeapon -- NewWeapon nullptr"));
	}
}

// bool AMainCharacter::InitPrimaryWeapon()
// {
// 	if(InventoryComponent)
// 	{
// 		FVector SpawnLocation = FVector(0);
// 		FRotator SpawnRotation = FRotator(0);
//
// 		FActorSpawnParameters SpawnParameters;
// 		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
// 		SpawnParameters.Owner = this;
// 		SpawnParameters.Instigator = GetInstigator();
//
// 		AMeleeWeaponBase* MyWeapon = Cast<AMeleeWeaponBase>(GetWorld()->SpawnActor(InventoryComponent->MeleeWeapon, &SpawnLocation, &SpawnRotation, SpawnParameters));
// 		if(MyWeapon)
// 		{
// 			const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
// 			MyWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocket"));
// 			PrimaryWeapon = MyWeapon;
// 			PrimaryWeapon->SetActorRelativeTransform(PrimaryWeapon->OffsetTransform);
// 			PrimaryWeapon->ActivateWeapon(false);
// 			ComboHandlerComponent->RegisterWeapon(PrimaryWeapon, SecondaryWeapon);
// 			return true;
// 		}
// 	}
// 	return false;
// }

// bool AMainCharacter::InitSecondaryWeapon(int32 WeaponIndex)
// {
// 	bool bIsSuccess = false;
// 	if(InventoryComponent)
// 	{
// 			const TSubclassOf<AWeaponBase> Weapon = InventoryComponent->GetWeaponByIndex(WeaponIndex);
// 			FVector SpawnLocation = FVector(0);
// 			FRotator SpawnRotation = FRotator(0);
//
// 			FActorSpawnParameters SpawnParameters;
// 			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
// 			SpawnParameters.Owner = this;
// 			SpawnParameters.Instigator = GetInstigator();
//
// 			AWeaponBase* MyWeapon = Cast<AWeaponBase>(GetWorld()->SpawnActor(Weapon, &SpawnLocation, &SpawnRotation, SpawnParameters));
// 			if(MyWeapon)
// 			{
// 				//StopPush.Broadcast(); TODO
// 				const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
// 				MyWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocket"));
// 				SecondaryWeapon = MyWeapon;
// 				SecondaryWeapon->SetActorRelativeTransform(CurrentWeapon->OffsetTransform);
// 				bIsSuccess = true;
// 				CurrentWeaponIndex = WeaponIndex;
// 				if(ARangedWeaponBase* RangeWeapon = Cast<ARangedWeaponBase>(SecondaryWeapon))
// 				{
// 					//RangeWeapon->OnFire.AddDynamic(this, &AGBCharacter::OnFire); TODO
// 					//RangeWeapon->OnReloadStart.AddDynamic(this, &AGBCharacter::OnReload);
// 					//RangeWeapon->ReloadResult.AddDynamic(this, &AGBCharacter::OnReloadEnd);
// 				
// 					RangeWeapon->SetCurrentAmmo(InventoryComponent->GetWeaponAmmo(RangeWeapon->GetClass()));
// 					RangeWeapon->SetDeadZoneComponent(SceneComponent);
// 				}
// 	
// 				//OnWeaponInit.Broadcast(CurrentWeapon); TODO
// 				SecondaryWeapon->ActivateWeapon(false);
// 				ComboHandlerComponent->RegisterWeapon(PrimaryWeapon, SecondaryWeapon);
// 			}
// 	}
// 	return bIsSuccess;
// }

void AMainCharacter::OnEmptyFlask()
{
	FGameplayEventData Data;
	Data.Instigator = this;
	AbilitySystemComponent->HandleGameplayEvent(UGameplayTagsManager::Get().RequestGameplayTag(TEXT("Flask.OutOfCharge")), &Data);

	if(bDebug)
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, TEXT(" AMainCharacter::OnEmptyFlask - Called"));
	}
}

void AMainCharacter::OnFullHealth()
{
	FGameplayEventData Data;
	Data.Instigator = this;
	AbilitySystemComponent->HandleGameplayEvent(UGameplayTagsManager::Get().RequestGameplayTag(TEXT("Flask.FullHealth")), &Data);

	if(bDebug)
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, TEXT(" AMainCharacter::OnFullHealth - Called"));
	}
}

void AMainCharacter::Landed(const FHitResult& Hit)
{
	if(OwningAbilities.Contains("Jump"))
	{
		AbilitySystemComponent->CancelAbilityHandle(OwningAbilities["Jump"]);
	}

}

void AMainCharacter::OnBlockAllTag(const FGameplayTag Tag, int32 Count)
{
	Super::OnBlockAllTag(Tag, Count);

	if(Count == 0)
	{
		//TODO more logic
		bCanInteract = true;
	}
	else
	{
		bCanInteract = false;
	}
}

void AMainCharacter::UseAttack(FGameplayAbilitySpecHandle Handle)
{
	AbilitySystemComponent->TryActivateAbility(Handle);
}




void AMainCharacter::DeadEvent()
{
	
	DestroyAllWeapon();
	
	Super::DeadEvent();
}

void AMainCharacter::AttackStart()
{

	
	Super::AttackStart();
}

void AMainCharacter::AttackEnd()
{

	
	Super::AttackEnd();
}

void AMainCharacter::SetPrimaryWeapon(AWeaponBase* NewWeapon)
{
	PrimaryWeapon = NewWeapon;
	ComboHandlerComponent->RegisterWeapon(PrimaryWeapon, SecondaryWeapon);
}

void AMainCharacter::SetSecondaryWeapon(AWeaponBase* NewWeapon)
{
	SecondaryWeapon = NewWeapon;

	CurrentWeaponIndex = InventoryComponent->GetIndexByWeapon(SecondaryWeapon);
	
	ComboHandlerComponent->RegisterWeapon(PrimaryWeapon, SecondaryWeapon);
}

void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
			Super::SetupPlayerInputComponent(PlayerInputComponent);

		PlayerInputComponent->BindAxis(TEXT("Move"), this, &AMainCharacter::InputAxisY);
		PlayerInputComponent->BindAction<FBoolInput>(TEXT("Fire"), EInputEvent::IE_Pressed, this, &AMainCharacter::InputAttack, true);
		PlayerInputComponent->BindAction<FBoolInput>(TEXT("Fire"), EInputEvent::IE_Released, this, &AMainCharacter::InputAttack, false);
		PlayerInputComponent->BindAction<FBoolInput>(TEXT("AimState"), EInputEvent::IE_Pressed, this, &AMainCharacter::SecondaryAction, true);
		PlayerInputComponent->BindAction<FBoolInput>(TEXT("AimState"), EInputEvent::IE_Released, this, &AMainCharacter::SecondaryAction, false);
		PlayerInputComponent->BindAction(TEXT("Reload"), EInputEvent::IE_Pressed, this, &AMainCharacter::Reload);
		PlayerInputComponent->BindAction(TEXT("Dodge"), EInputEvent::IE_Pressed, this, &AMainCharacter::Dodge);
		//PlayerInputComponent->BindAction<FSlotInput>(TEXT("FirstSlot"), EInputEvent::IE_Pressed, this, &AMainCharacter::SwitchWeaponByIndex, 0);
		//PlayerInputComponent->BindAction<FSlotInput>(TEXT("SecondSlot"), EInputEvent::IE_Pressed, this, &AMainCharacter::SwitchWeaponByIndex, 1);
		//PlayerInputComponent->BindAction(TEXT("MeleeAttack"), EInputEvent::IE_Pressed, this, &AMainCharacter::EnterBlockStance);
		//PlayerInputComponent->BindAction(TEXT("MeleeAttack"), EInputEvent::IE_Released, this, &AMainCharacter::InputExitBlockStance);
		PlayerInputComponent->BindAction<FBoolInput>(TEXT("Interact"), EInputEvent::IE_Pressed, this, &AMainCharacter::Interact, true);
		PlayerInputComponent->BindAction<FBoolInput>(TEXT("Interact"), EInputEvent::IE_Released, this, &AMainCharacter::Interact, false);
		PlayerInputComponent->BindAction<FBoolInput>(TEXT("Jump"), EInputEvent::IE_Pressed, this, &AMainCharacter::InputJump, true);
		PlayerInputComponent->BindAction<FBoolInput>(TEXT("Jump"), EInputEvent::IE_Released, this, &AMainCharacter::InputJump, false);
		PlayerInputComponent->BindAction(TEXT("Down"), EInputEvent::IE_Pressed, this, &AMainCharacter::DownInput);
		PlayerInputComponent->BindAction<FBoolInput>(TEXT("FlaskHeal"), EInputEvent::IE_Pressed, this, &AMainCharacter::InputFlaskUse, true);
		PlayerInputComponent->BindAction<FBoolInput>(TEXT("FlaskHeal"), EInputEvent::IE_Released, this, &AMainCharacter::InputFlaskUse, false);
}

void AMainCharacter::SetCombatState(const ECombatState State)
{
	CombatState = State;
	OnCombatStateChanged.Broadcast(CombatState);
}

void AMainCharacter::InputAttack(const bool Flag)
{
	if(CanAttack())
	{
		// Check weapon force us use weapon skill rather than combo
		if(CurrentWeapon->bForceWeaponSkill)
		{
			if(CurrentWeapon->AddedAbilities.Contains("PrimaryAbility"))
			{
				if(Flag)
				{
					AbilitySystemComponent->TryActivateAbility(CurrentWeapon->AddedAbilities["PrimaryAbility"]);
				}
				else
				{
					AbilitySystemComponent->CancelAbilityHandle(CurrentWeapon->AddedAbilities["PrimaryAbility"]);
				}
			}
			else 
			{
				FString ErrorString = TEXT("AMainCharacter::InputAttack -- Weapon forced ability not found");
				if(bDebug)
				{
					GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, ErrorString);
				}
				UE_LOG(LogTemp, Error, TEXT("AMainCharacter::InputAttack -- Weapon forced ability not found"));
			}
				
		}
		else if(!bBlockPrimaryInput)
		{
			bBlockSecondaryInput = true;
			if(Flag)
			{
				if(GetCharacterMovement()->IsFalling())
				{
					ComboHandlerComponent->HandleInput(EInputType::Primary_InAir);
				}
				else
				{
					GetWorld()->GetTimerManager().SetTimer(InputTimerHandler, this, &AMainCharacter::PrimaryTimerFunc, DelayHold, false);
				}
			}
			else
			{
				if(GetWorld()->GetTimerManager().IsTimerActive(InputTimerHandler))
				{
					GetWorld()->GetTimerManager().ClearTimer(InputTimerHandler);
				}
				ComboHandlerComponent->HandleInput(EInputType::Primary);
				bBlockSecondaryInput = false;
			}
		}
	}
}

void AMainCharacter::SecondaryAction(const bool Flag)
{
	// TODO press, hold, in air logic then check is in combo, if true send hold input, else go to aim state
	if(!bBlockSecondaryInput)
	{
		bBlockPrimaryInput = true;
		if(Flag)
		{
			if(GetCharacterMovement()->IsFalling())
			{
				ComboHandlerComponent->HandleInput(EInputType::Secondary_InAir);
			}
			else
			{
				GetWorld()->GetTimerManager().SetTimer(InputTimerHandler, this, &AMainCharacter::SecondaryTimerFunc, DelayHold, false);
			}
		}
		else
		{
			if(GetWorld()->GetTimerManager().IsTimerActive(InputTimerHandler))
			{
				GetWorld()->GetTimerManager().ClearTimer(InputTimerHandler);
				ComboHandlerComponent->HandleInput(EInputType::Secondary);			
			}
			else
			{
				if(SecondaryWeapon->AddedAbilities.Contains("SecondaryAbility"))
				{
					AbilitySystemComponent->CancelAbilityHandle(SecondaryWeapon->AddedAbilities["SecondaryAbility"]);
				}
				else
				{
					FString ErrorString = TEXT("AMainCharacter::SecondaryAction -- Weapon ability not found");
					if(bDebug)
					{
						GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, ErrorString);
					}
					UE_LOG(LogTemp, Error, TEXT("AMainCharacter::SecondaryAction -- Weapon ability not found"));
				}
			}
			bBlockPrimaryInput = false;
		}
	}

}

void AMainCharacter::PrimaryTimerFunc()
{
	ComboHandlerComponent->HandleInput(EInputType::Primary_Hold);
	bBlockSecondaryInput = false;
}

void AMainCharacter::SecondaryTimerFunc()
{
	if(ComboHandlerComponent->IsInCombo())
	{
		ComboHandlerComponent->HandleInput(EInputType::Secondary_Hold);
	}
	else
	{
		if(SecondaryWeapon && SecondaryWeapon->AddedAbilities.Contains("SecondaryAbility"))
		{
			AbilitySystemComponent->TryActivateAbility(SecondaryWeapon->AddedAbilities["SecondaryAbility"]);
		}
		else
		{
			FString ErrorString = TEXT("AMainCharacter::SecondaryTimerFunc -- Weapon ability not found");
			if(bDebug)
			{
				GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, ErrorString);
			}
			UE_LOG(LogTemp, Error, TEXT("AMainCharacter::SecondaryTimerFunc -- Weapon ability not found"));
		}
	}
	bBlockPrimaryInput = false;
}

void AMainCharacter::Reload()
{
}

void AMainCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void AMainCharacter::Interact(const bool Flag)
{
	if(InteractionTarget)
	{
		if(InteractionTarget->GetClass()->ImplementsInterface(UInteractInterface::StaticClass()))
		{
			IInteractInterface::Execute_Interact(InteractionTarget, this, Flag);
		}
		else
		{

		}
	}
}

void AMainCharacter::InputJump(const bool Flag)
{
	if(Flag)
	{
	
		if(!AbilitySystemComponent->HasMatchingGameplayTag(UGameplayTagsManager::Get().RequestGameplayTag("Effect.StickWall")))
		{
			if(OwningAbilities.Contains("Jump"))
			{
				AbilitySystemComponent->TryActivateAbility(OwningAbilities["Jump"]);
			}
		}
		else
		{
			if(GetActorForwardVector().Y * AxisY >= 0)
			{
				FGameplayEventData Data;
				Data.Instigator = this;
				AbilitySystemComponent->HandleGameplayEvent(UGameplayTagsManager::Get().RequestGameplayTag(TEXT("Event.StickWall.ClimbUp")), &Data);
			}
			else
			{
				FGameplayEventData Data;
				Data.Instigator = this;
				AbilitySystemComponent->HandleGameplayEvent(UGameplayTagsManager::Get().RequestGameplayTag(TEXT("Event.StickWall.BackJump")), &Data);
			}
		}
	}
}

void AMainCharacter::DownInput()
{
	if(AbilitySystemComponent->HasMatchingGameplayTag(UGameplayTagsManager::Get().RequestGameplayTag("Effect.StickWall")))
	{
		FGameplayEventData Data;
		Data.Instigator = this;
		AbilitySystemComponent->HandleGameplayEvent(UGameplayTagsManager::Get().RequestGameplayTag(TEXT("Event.StickWall.Down")), &Data);
	}
}



void AMainCharacter::Dodge()
{
	if(OwningAbilities.Contains("Dodge"))
	{
		AbilitySystemComponent->TryActivateAbility(OwningAbilities["Dodge"]);
	}
}

void AMainCharacter::InputFlaskUse(const bool Flag)
{
	if(Flag && OwningAbilities.Contains("FlaskHeal"))
	{
		AbilitySystemComponent->TryActivateAbility(OwningAbilities["FlaskHeal"]);
	}
	else
	{
		FGameplayEventData Data;
		Data.Instigator = this;
		AbilitySystemComponent->HandleGameplayEvent(UGameplayTagsManager::Get().RequestGameplayTag(TEXT("Flask.Stop")), &Data);

		if(bDebug)
		{
			GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, TEXT(" AMainCharacter::InputFlaskUse - Called"));
		}
	}
}

void AMainCharacter::UpdateInteractionTarget(AActor* Interactable)
{
	if(InteractionTarget != Interactable && Interactable != nullptr)
	{
		if(InteractionTarget)
		{
			IInteractInterface::Execute_Highlight(InteractionTarget, false);
		}
		
		InteractionTarget = Interactable;

		IInteractInterface::Execute_Highlight(InteractionTarget, true);

		FDisplayInfo DisplayInfo = IInteractInterface::Execute_GetDisplayInfo(InteractionTarget);

		if(AGBHUD* MyHUD = Cast<AGBHUD>(GetMyController()->GetHUD()))
		{
			MyHUD->ShowInteractAction(DisplayInfo, true);
		}
	}
}

void AMainCharacter::AddInteractable(AActor* Interactable)
{
	// If target is Interactable add it to array
	if(!InteractionArray.Contains(Interactable) && Interactable->GetClass()->ImplementsInterface(UInteractInterface::StaticClass()))
	{
		InteractionArray.Add(Interactable);
		// If can interact set new interactable as current interactable
		if(!AbilitySystemComponent->HasMatchingGameplayTag(UGameplayTagsManager::Get().RequestGameplayTag(FName("Effect.BlockAction.Interaction"))))
		{
			UpdateInteractionTarget(Interactable);
		}
	}
}


void AMainCharacter::ForceSetInteractable(AActor* Interactable)
{
	if(!InteractionArray.Contains(Interactable) && Interactable->GetClass()->ImplementsInterface(UInteractInterface::StaticClass()))
	{
		InteractionArray.Add(Interactable);

		UpdateInteractionTarget(Interactable);
	}
}

void AMainCharacter::RemoveInteractable(AActor* Interactable)
{
	// Find and remove actor from interactable list
	if(InteractionArray.Contains(Interactable))
	{
		InteractionArray.Remove(Interactable);
		IInteractInterface::Execute_Highlight(Interactable, false);
		
	}
	// If removed actor is current interactable change current to next if able
	if(InteractionTarget == Interactable && InteractionArray.Num() > 0)
	{
		InteractionTarget = InteractionArray[InteractionArray.Num() - 1];
		IInteractInterface::Execute_Highlight(InteractionTarget, true);
		
	}
	else
	{
		//InteractionTarget = nullptr;
		if(AGBHUD* MyHUD = Cast<AGBHUD>(GetMyController()->GetHUD()))
		{
			MyHUD->ShowInteractAction(FDisplayInfo{}, false);
		}
	}
}
