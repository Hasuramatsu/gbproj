// Base class for game characters

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GBProj/Actors/ActorComponents/HealthComponent.h"
#include "AbilitySystemInterface.h"
#include "AbilitySystemComponent.h"
#include "ActorComponents/FlaskComponent.h"
#include "GBProj/AbilitySystem/GBAbilitySystemComponent.h"
#include "GBProj/AbilitySystem/GBAttributeSet.h"
#include "GBProj/AbilitySystem/GB_GameplayAbility.h"


#include "GBCharacterBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCharacterDeath, AGBCharacterBase*, DeadCharacter);

UCLASS()
class GBPROJ_API AGBCharacterBase : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	///////////////////////////////////////////////////
	/////////  Constructor and overrides     //////////
	///////////////////////////////////////////////////

	// Sets default values for this character's properties
	AGBCharacterBase();

	// Delegate for Character Death
	UPROPERTY(BlueprintAssignable)
	FOnCharacterDeath CharacterDead;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void PostInitProperties() override;
	
	//Ability Interface override
	FORCEINLINE virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override {return AbilitySystemComponent;}

	UFUNCTION(BlueprintCallable)
	void GetActiveAbitiesWithTags(FGameplayTagContainer AbilityTags, TArray<UGB_GameplayAbility*>& ActiveAbilities);

	///////////////////////////////////////////////////
	/////////           Components           //////////
	///////////////////////////////////////////////////

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	UHealthComponent* HealthComponent = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
	UGBAbilitySystemComponent* AbilitySystemComponent = nullptr;

	///////////////////////////////////////////////////
	/////////              Debug             //////////
	///////////////////////////////////////////////////
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool bDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug", meta=(EditCondition=bDebug))
	float DebugFloatValue = 45.f;

	///////////////////////////////////////////////////
	/////////            Attributes          //////////
	///////////////////////////////////////////////////

	// UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character")
	// FRotator StartingRotation;
	//
	// //Life span when character dead
	// UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character")
	// float CorpseDuration;
	//
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|Settings")
	// FGameplayTagContainer DamageReactionTags;

	UPROPERTY(EditDefaultsOnly, Category = "CharacterData")
	FName CharacterInfoName;

	UPROPERTY(EditInstanceOnly, Category = "CharacterData")
	FCharacterData CharacterData;

	UFUNCTION(BlueprintCallable)
	void InitCharacter();
		
	///////////////////////////////////////////////////
	/////////             Functions          //////////
	///////////////////////////////////////////////////


	//Getters
	// Return IsAlive boolean
	UFUNCTION(BlueprintPure)
	FORCEINLINE bool IsAlive() const {return bIsAlive;}

	//Overridable function for detect cursor or player location
	UFUNCTION(BlueprintPure)
	virtual FVector GetObjectLocation() const;

	//Overridable function for get Character Weapon
	UFUNCTION(BlueprintPure)
	FORCEINLINE AWeaponBase* GetCurrentWeapon() const { return CurrentWeapon;}

	//Setters
	UFUNCTION(BlueprintCallable)
	void SetAlive(const bool Flag) {bIsAlive = Flag;}
	
	
	//Blueprint Implementable functions
	UFUNCTION(BlueprintImplementableEvent)
	void OnDead_Event();

	// Return bDodging
	UFUNCTION(BlueprintPure)
	FORCEINLINE bool IsDodging() const {return bDodging;}

	UFUNCTION(BlueprintPure)
	FORCEINLINE bool CanMove() const {return bCanMove;}

	UFUNCTION(BlueprintPure)
	FORCEINLINE bool CanRotate() const {return bCanRotate;}

	UFUNCTION(BlueprintPure)
	FORCEINLINE bool CanAttack() const {return bCanAttack;}

	
	UFUNCTION(BlueprintCallable)
	void SetCanAttack(bool Flag) {bCanAttack = Flag;}
	

	// Grant character abilities
	UFUNCTION(BlueprintCallable)
	void RegisterNewAbilities(TMap<FString, TSubclassOf<UGameplayAbility>>& NewAbilities);

	// Get character owning abilities
	UFUNCTION(BlueprintCallable)
	void GetOwningAbilities(TMap<FString, FGameplayAbilitySpecHandle>& Abilities);

	// Return flask component ptr. Need override
	UFUNCTION(BlueprintPure)
	virtual UFlaskComponent* GetFlaskComponent() const { return nullptr;}

	//Function for control hit boxes activation
	UFUNCTION(BlueprintCallable)
	virtual void AttackStart();
	UFUNCTION(BlueprintImplementableEvent)
	void AttackStart_Event();

	//Function for control hit boxes deactivation
	UFUNCTION(BlueprintCallable)
	virtual void AttackEnd();
	UFUNCTION(BlueprintImplementableEvent)
	void AttackEnd_Event();

	//Set Capsule ComponentCollision to base value, return result collision name
	UFUNCTION(BlueprintCallable)
	FName ResetCollisionToBase();

	// Set speed attribute at new value
	UFUNCTION(BlueprintCallable)
	void SetMaxMovementSpeed(float NewSpeed);

	// Stun Testing TODO change and move somewhere else

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StunTest")
	bool bCanBeStunned = true;
	
	// UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StunTest")
	// FGameplayTagContainer CancelOnStunTags;

	// UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StunTest")
	// TSubclassOf<UGameplayEffect> StunEffect;
	//
	// UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StunTest")
	// float StunDuration = 0.5f;
	//
	// UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StunTest")
	// float StunImmuneDuration = 0.5f;
	//
	// UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StunTest")
	// TSubclassOf<UGameplayEffect> StunImmuneEffect;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	
	//Attribute set
	UPROPERTY()
	UGBAttributeSet* AttributeSet;

	//Weapon hold by character
	UPROPERTY(BlueprintReadWrite)
	AWeaponBase* CurrentWeapon = nullptr;

	// // Starting character's abilities
	// UPROPERTY(EditDefaultsOnly, Category = "Abilities")
	// TMap<FString, TSubclassOf<UGameplayAbility>> StartingAbilities;

	// List of already granted
	UPROPERTY()
	TMap<FString, FGameplayAbilitySpecHandle> OwningAbilities;

	// // Effects that affect player at the BeginPlay
	// UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Abilities")
	// TArray<TSubclassOf<UGameplayEffect>> PassiveGameplayEffects;



	// Init Weapon
	UFUNCTION(BlueprintCallable)
	virtual AWeaponBase* InitWeapon(TSubclassOf<AWeaponBase> WeaponClass, FName InSocket = FName("WeaponSocket"));

	//DeadEvent called when life is zero
	UFUNCTION()
	virtual void DeadEvent();

	virtual void Destroyed() override;

	//Functions for tags changed
	UFUNCTION()
	virtual void OnDodgeTagChanged(const FGameplayTag Tag, int32 Count);

	UFUNCTION()
	virtual void OnBlockMovementTag(const FGameplayTag Tag, int32 Count);

	UFUNCTION()
	virtual void OnBlockRotationTag(const FGameplayTag Tag, int32 Count);

	UFUNCTION()
	virtual void OnBlockAttackTag(FGameplayTag Tag, int32 Count);

	UFUNCTION()
	virtual void OnBlockAllTag(const FGameplayTag Tag, int32 Count);

	UFUNCTION(BlueprintImplementableEvent)
	void DamageHitTaken(float DamageTaken, float UnmitigatedDamage);

	void Stun();
	UFUNCTION(BlueprintImplementableEvent)
	void OnStun();

	virtual void OnMovementSpeedAttributeChanged(const FOnAttributeChangeData& Data);
private:	

	UPROPERTY()
	bool bIsAlive = true;
	UPROPERTY()
	bool bDodging = false;
	UPROPERTY()
	bool bCanMove = true;
	UPROPERTY()
	bool bCanRotate = true;
	UPROPERTY()
	bool bCanAttack = true;

	FName BaseCollision;

	// Called on periodic effects execution
	UFUNCTION()
	void OnPeriodicEffectExecute(UAbilitySystemComponent* Source, const FGameplayEffectSpec& Spec, FActiveGameplayEffectHandle Handle);

	// Called when any effect applied
	UFUNCTION()
	void OnEffectApplied(UAbilitySystemComponent* Source, const FGameplayEffectSpec& Spec, FActiveGameplayEffectHandle Handle);

	UFUNCTION()
	virtual void OnDamageHitTaken(float DamageTaken, float UnmitigatedDamage);
};





