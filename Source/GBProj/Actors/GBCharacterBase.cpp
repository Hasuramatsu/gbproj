// Fill out your copyright notice in the Description page of Project Settings.


#include "GBCharacterBase.h"

#include "GameplayTagsManager.h"
#include "GBAnimInstance.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GBProj/AbilitySystem/FlaskHealEffect.h"
#include "GBProj/AbilitySystem/GBAbilitySystemComponent.h"
#include "GBProj/Func/GBLib.h"
#include "Kismet/GameplayStatics.h"
#include "Weapon/WeaponBase.h"

// Sets default values
AGBCharacterBase::AGBCharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.f);
	BaseCollision = GetCapsuleComponent()->GetCollisionProfileName();

	//Configure Character movement
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	GetCharacterMovement()->SetPlaneConstraintNormal(FVector(1, 0, 0));

	// Load character data from table

	
	//Components
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	HealthComponent->OnDead.AddDynamic(this, &AGBCharacterBase::DeadEvent);

	AbilitySystemComponent = CreateDefaultSubobject<UGBAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	AttributeSet = CreateDefaultSubobject<UGBAttributeSet>(TEXT("AttributeSet"));
	AttributeSet->OnDamageHitTaken.AddDynamic(this, &AGBCharacterBase::OnDamageHitTaken);
	AttributeSet->OnDamageHitTaken.AddDynamic(AbilitySystemComponent, &UGBAbilitySystemComponent::OnReceiveDamage);

	SetInstigator(this);
}

void AGBCharacterBase::InitCharacter()
{
	UGBGameInstance* MyGI = Cast<UGBGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if(MyGI)
	{
		MyGI->GetCharacterDataByName(CharacterInfoName, CharacterData);
	}
}

FVector AGBCharacterBase::GetObjectLocation() const
{
	return FVector::ZeroVector;
}

void AGBCharacterBase::RegisterNewAbilities(TMap<FString, TSubclassOf<UGameplayAbility>>& NewAbilities)
{
	TMap<FString, FGameplayAbilitySpec> NewSpecs;
	
	for(auto& It : NewAbilities)
	{
		if(It.Value.Get())
		{
			NewSpecs.Add(It.Key, FGameplayAbilitySpec(It.Value, 1, INDEX_NONE, this));
		}
	}
	
	for(auto& It : NewSpecs)
	{
		FGameplayAbilitySpecHandle& SpecHandle = OwningAbilities.FindOrAdd(It.Key);

		if(!SpecHandle.IsValid())
		{
			SpecHandle = AbilitySystemComponent->GiveAbility(It.Value);
		}
	}
}

void AGBCharacterBase::GetOwningAbilities(TMap<FString, FGameplayAbilitySpecHandle>& Abilities)
{
	Abilities = OwningAbilities;
}

void AGBCharacterBase::AttackStart()
{
	AttackStart_Event();
}

void AGBCharacterBase::AttackEnd()
{
	AttackEnd_Event();
}

// Called in response to damage taken in attribute
void AGBCharacterBase::OnDamageHitTaken(float DamageTaken, float UnmitigatedDamage)
{
	// if character can be stunned at all and dont have stun immune at current moment
	if(bCanBeStunned && !AbilitySystemComponent->HasMatchingGameplayTag(UGameplayTagsManager::Get().RequestGameplayTag("Status.StunImmune")))
	{
		Stun();
	}

	DamageHitTaken(DamageTaken, UnmitigatedDamage);
}

void AGBCharacterBase::Stun()
{
	if(AbilitySystemComponent->CreateAndApplyEffectWithModifiedDuration(CharacterData.StunEffect, CharacterData.StunDuration))
	{
		AbilitySystemComponent->CreateAndApplyEffectWithModifiedDuration(CharacterData.StunImmuneEffect, CharacterData.StunImmuneDuration);
		AbilitySystemComponent->Stunned(CharacterData.StunDuration);

		OnStun();
	}

	if(UGBAnimInstance* MyAnimInstance = Cast<UGBAnimInstance>(GetMesh()->GetAnimInstance()))
	{
		MyAnimInstance->OnStun(CharacterData.StunDuration);
	}
}

FName AGBCharacterBase::ResetCollisionToBase()
{

	GetCapsuleComponent()->SetCollisionProfileName(BaseCollision);
	
	return GetCapsuleComponent()->GetCollisionProfileName();
}

void AGBCharacterBase::SetMaxMovementSpeed(float NewSpeed)
{
	AbilitySystemComponent->SetNumericAttributeBase(AttributeSet->GetMovementSpeedAttribute(), NewSpeed);
}


void AGBCharacterBase::PostInitProperties()
{
	
	
	Super::PostInitProperties();
}

// Called when the game starts or when spawned
void AGBCharacterBase::BeginPlay()
{
	Super::BeginPlay();
	

	InitCharacter();
	
	Tags.Add(FName("Alive"));

	
	if(AbilitySystemComponent)
	{
		if(HealthComponent)
		{
			//Bind health attribute changes to health component attributes
			AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetHealthAttribute()).AddUObject(HealthComponent, &UHealthComponent::SetHealthAttribute);
			AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxHealthAttribute()).AddUObject(HealthComponent, &UHealthComponent::SetMaxHealthAttribute);
		}
		if(GetCharacterMovement())
		{
			//Bind Movement Speed attribute changes to Character Movement component
			AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMovementSpeedAttribute()).AddUObject(this, &AGBCharacterBase::OnMovementSpeedAttributeChanged);
		}

		
		RegisterNewAbilities(CharacterData.StartingAbilities);

		for(auto& It : CharacterData.PassiveGameplayEffects)
		{
			FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
			EffectContext.AddSourceObject(this);

			FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(It, 1, EffectContext);
			if(NewHandle.IsValid())
			{
				FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), AbilitySystemComponent);
			}
			
		}

		AbilitySystemComponent->OnPeriodicGameplayEffectExecuteDelegateOnSelf.AddUObject(this, &AGBCharacterBase::OnPeriodicEffectExecute);

		AbilitySystemComponent->OnGameplayEffectAppliedDelegateToSelf.AddUObject(this, &AGBCharacterBase::OnEffectApplied);

		//Bind tags
		AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag("InAction.Dodge")).AddUObject(this, &AGBCharacterBase::OnDodgeTagChanged);
		AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag("Effect.BlockAction.Movement")).AddUObject(this, &AGBCharacterBase::OnBlockMovementTag);
		AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag("Effect.BlockAction.Rotation")).AddUObject(this, &AGBCharacterBase::OnBlockRotationTag);
		AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag("Effect.BlockAction.Attack")).AddUObject(this, &AGBCharacterBase::OnBlockAttackTag);
		AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag("Effect.BlockAction.All")).AddUObject(this, &AGBCharacterBase::OnBlockAllTag);

	}


	SetActorRotation(CharacterData.StartingRotation);
}



void AGBCharacterBase::OnMovementSpeedAttributeChanged(const FOnAttributeChangeData& Data)
{
	GetCharacterMovement()->MaxWalkSpeed = Data.NewValue;
}

AWeaponBase* AGBCharacterBase::InitWeapon(TSubclassOf<AWeaponBase> WeaponClass, FName InSocket)
{
	FVector SpawnLocation = FVector(0);
	FRotator SpawnRotation = FRotator(0);

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParameters.Owner = this;
	SpawnParameters.Instigator = GetInstigator();

	AWeaponBase* MyWeapon = Cast<AWeaponBase>(GetWorld()->SpawnActor(WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParameters));
	if(MyWeapon)
	{
		MyWeapon->SetupAttachment(GetMesh());
	}
	return MyWeapon;
}

void AGBCharacterBase::DeadEvent()
{
	SetAlive(false);
	GetCapsuleComponent()->SetCollisionProfileName("DeadBody");
	SetLifeSpan(CharacterData.CorpseDuration);

	Tags.Remove(FName("Alive"));

	if(CurrentWeapon)
	{
		CurrentWeapon->Destroy();
	}
	//Calling blueprint function
	OnDead_Event();

	CharacterDead.Broadcast(this);
}

void AGBCharacterBase::Destroyed()
{
	if(CurrentWeapon)
	{
		CurrentWeapon->Destroy();
	}
	
	Super::Destroyed();
}

// Called every frame
void AGBCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AGBCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AGBCharacterBase::GetActiveAbitiesWithTags(FGameplayTagContainer AbilityTags,
	TArray<UGB_GameplayAbility*>& ActiveAbilities)
{
	if (AbilitySystemComponent)
	{
		AbilitySystemComponent->GetGameplayAbilitiesWithMatchingTags(AbilityTags, ActiveAbilities);
	}
}

void AGBCharacterBase::OnDodgeTagChanged(const FGameplayTag, int32 Count)
{
	bDodging = Count > 0 ? true : false;
}

void AGBCharacterBase::OnBlockMovementTag(const FGameplayTag Tag, int32 Count)
{
	bCanMove = Count > 0 ? false : true;
}

void AGBCharacterBase::OnBlockRotationTag(const FGameplayTag Tag, int32 Count)
{
	bCanRotate = Count > 0 ? false : true;
}

void AGBCharacterBase::OnBlockAttackTag(const FGameplayTag Tag, int32 Count)
{
	bCanAttack = Count > 0 ? false : true;
}

void AGBCharacterBase::OnBlockAllTag(const FGameplayTag Tag, int32 Count)
{
	if(Count == 0)
	{
		bCanMove = true;
		bCanRotate = true;
		bCanAttack = true;
	}
	else
	{
		bCanMove = false;
		bCanRotate = false;
		bCanAttack = false;
	}
}

void AGBCharacterBase::OnPeriodicEffectExecute(UAbilitySystemComponent* Source, const FGameplayEffectSpec& Spec,
                                               FActiveGameplayEffectHandle Handle)
{
	if(auto Effect = Cast<UFlaskHealEffect>(Spec.Def))
	{
		if(GetFlaskComponent())
		{
			GetFlaskComponent()->AddFlaskCharges(-Effect->ChargeConsumePerTick);
		}
	}
}

void AGBCharacterBase::OnEffectApplied(UAbilitySystemComponent* Source, const FGameplayEffectSpec& Spec,
	FActiveGameplayEffectHandle Handle)
{
	if(auto Effect = Cast<UFlaskComponent>(Spec.Def))
	{
		if(GetFlaskComponent())
		{
			GetFlaskComponent()->ConsumeChargePerTick = Effect->ConsumeChargePerTick;
		}
	}
}