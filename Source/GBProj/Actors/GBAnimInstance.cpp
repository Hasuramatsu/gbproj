// Fill out your copyright notice in the Description page of Project Settings.


#include "GBAnimInstance.h"

AGBCharacterBase* UGBAnimInstance::TryGetOwnerCharacter() const
{
	return  OwnerCharacter;
}

void UGBAnimInstance::NativeBeginPlay()
{

	AGBCharacterBase* Char = Cast<AGBCharacterBase>(TryGetPawnOwner());
	if(Char)
	{
		OwnerCharacter = Char;
		Char->CharacterDead.AddDynamic(this, &UGBAnimInstance::OwnerDead);
	}
	
	Super::NativeBeginPlay();
}

void UGBAnimInstance::OwnerDead(AGBCharacterBase* DeadChar)
{
	bIsAlive = false;
	
	OwnerDead_BP();
}
