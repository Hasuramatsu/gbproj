// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InterfaceCharacter.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInterfaceCharacter : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class GBPROJ_API IInterfaceCharacter
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FVector GetTargetLocation();

	//Push
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	bool CanPush();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void StartPush(AActor* Actor);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void EndPush();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void GrabPushed(const bool Flag);

	//Damage and health
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DeadEvent();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Heal();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	bool IsAlive() const;
};
