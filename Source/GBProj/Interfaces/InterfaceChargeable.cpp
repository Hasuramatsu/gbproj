// Fill out your copyright notice in the Description page of Project Settings.


#include "InterfaceChargeable.h"

// Add default functionality here for any IInterfaceChargeable functions that are not pure virtual.
void IInterfaceChargeable::OnStartCharge()
{
}

void IInterfaceChargeable::OnEndCharge()
{
}