// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InterfaceAIHelper.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInterfaceAIHelper : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class GBPROJ_API IInterfaceAIHelper
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FVector GetRootLocation() const;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	float GetPatrolRadius() const;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	float GetAttackTime() const;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Attack();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	bool CheckCanAttack();

	
};
