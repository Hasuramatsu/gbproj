// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Types.h"
#include "Abilities/GameplayAbilityTargetTypes.h"
#include "Abilities/GameplayAbilityTypes.h"
#include "GBProj/Game/GBGameInstance.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GBLib.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API UGBLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	static float ApplyAbilityDamageHit(FDamageHitInfo DamageHitInfo, AActor* DamagedActor, AActor* DamageCauser);

	UFUNCTION(BlueprintPure)
	static bool IsFacing(const AActor* Source, const AActor* Target);

	UFUNCTION(BlueprintCallable)
	static float ScalarTwoActors(const AActor* Source, const AActor* Target);

	UFUNCTION()
	static FGameplayAbilityTargetDataHandle MakeTargetDataFromActor(AActor* Actor);

	UFUNCTION(BlueprintCallable)
	static FGameplayEventData MakeDefaultEventData(AActor* Source, AActor* Target, FGameplayTag EventTag, float EventMagnitude, UObject* OptObject, UObject* OptObjectTwo);

	// Return opposite rotation
	UFUNCTION(BlueprintCallable)
	static FRotator GetReverseRotation(FRotator Rotation);

	UFUNCTION(BlueprintPure, meta=(WorldContext="WorldContextObject"))
	static UGBGameInstance* GetGBGameInstance(const UObject* WorldContextObject);

	// Enable or disable outline
	UFUNCTION(BlueprintCallable)
	static void HighlightMash(UStaticMeshComponent* Mesh, bool Flag);
};
