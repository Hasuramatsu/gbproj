// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "GBProj/Actors/Projectiles/ProjectileBase.h"
#include "Kismet/BlueprintFunctionLibrary.h"
//#include "FMODEvent.h"
#include "NiagaraSystem.h"
#include "Engine/DataTable.h"
#include "GameplayTagContainer.h"
//#include "GBProj/Actors/Weapon/WeaponBase.h"


#include "Types.generated.h"


class UGameplayEffect;
class UGameplayAbility;
class AWeaponBase;

UENUM(BlueprintType)
enum class EInteractionType : uint8
{
	PickupType UMETA(DisplayName = "Pickup"),
	InteractType UMETA(DisplayName = "Interact"),
	OpenType UMETA(DisplayName = "Open"),
	None UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class ECombatState : uint8
{
	MeleeState UMETA(DisplayName = "Melee"),
	AimState UMETA(DisplayName = "Aiming"),
	BlockState UMETA(DisplayName = "Blocking"),
	DodgeState UMETA(DisplayName = "Dodge")
};


UENUM(BlueprintType)
enum class EEffectExecutionType : uint8
{
	InstantEffect UMETA(DisplayName = "Instant"),
	DurableEffect UMETA(DisplayName = "DurableEffect"),
	None UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class EEffectType : uint8
{
	HealEffect UMETA(DisplayName = "HealEffect"),
	DamageEffect UMETA(DisplayName = "DamageEffect"),
	BuffEffect UMETA(DisplayName = "BuffEffect"),
	DebuffEffect UMETA(DisplayName = "DebufEffect"),
	MiscEffect UMETA(DisplayName = "MiscEffect"),
	None UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class EGamePhase : uint8
{
	None UMETA(DisplayName = "None"),
	FirstPhase UMETA(DisplayName = "FirstPhase"),
	SecondPhase UMETA(DisplayName = "SecondPhase"),
	ThirdPhase UMETA(DisplayName = "ThirdPhase"),
	BossPhase UMETA(DisplayName = "BossPhase")
	
};

UENUM(BlueprintType)
enum class ECharacterState : uint8
{
	Walking UMETA(DisplayName = "Walking"),
	Climbing UMETA(DisplayName = "Climbing"),
	ClimbUp UMETA(DisplayName = "Climb")
};

UENUM(BlueprintType)
enum class ECurrentWeaponType : uint8
{
	Primary UMETA(DisplayName = "PrimaryWeapon"),
	Secondary UMETA(DisplayName = "SecondaryWeapon"),
	None UMETA(DisplayName = "NoWeapon")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	Melee UMETA(DisplayName = "MeleeWeapon"),
	Ranged UMETA(DisplayName = "RangedWeapon"),
	Utility UMETA(DisplayName = "UtilityWeapon")
};


UENUM(BlueprintType)
enum class EInputType : uint8
{
	Primary UMETA(DisplayName = "Primary"),
	Primary_Hold UMETA(DisplayName = "PrimaryHold"),
	Primary_InAir UMETA(DisplayName = "PrimaryInAir"),
	Secondary UMETA(DisplayName = "Secondary"),
	Secondary_Hold UMETA(DisplayName = "SecondaryHold"),
	Secondary_InAir UMETA(DisplayName = "SecondaryInAir")
};

USTRUCT(BlueprintType)
struct FComboHandle
{
	GENERATED_BODY()

	FComboHandle() {};

	FComboHandle(const TArray<EInputType>& NewCombo) { Combo = NewCombo;}
	
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<EInputType> Combo;

	bool operator== (const FComboHandle& Other) const
	{
		return Combo == Other.Combo;
	}

	friend uint32 GetTypeHash(const FComboHandle& Other) 
	{
		FString Hash;
		for(auto& It : Other.Combo)
		{
			Hash += FString::FromInt(static_cast<int32>(It));
			
		}
		
		return FCString::Atoi(*Hash);
	}
};

USTRUCT(BlueprintType)
struct FWeaponComboInfo : public FTableRowBase
{
	GENERATED_BODY()


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapons")
	TSubclassOf<AWeaponBase> PrimaryWeapon;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Weapons")
	TSubclassOf<AWeaponBase> SecondaryWeapon;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Weapons")
	TMap<FComboHandle, TSubclassOf<UGameplayAbility>> WeaponAbilities;

	
};



USTRUCT(BlueprintType)
struct FHitEffects
{
	GENERATED_BODY()



	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<TSubclassOf<class UEffectBase>> Effects;
	//UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	//TMap<TEnumAsByte<EPhysicalSurface>, UFMODEvent*> HitSound;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFX;
};

USTRUCT(BlueprintType)
struct FCharacterSounds
{
	GENERATED_BODY()

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//TMap<TEnumAsByte<EPhysicalSurface>, UFMODEvent*> StepsSound;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite)
	// UFMODEvent* JumpStartSound = nullptr;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite)
	// UFMODEvent* JumpEndSound = nullptr;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite)
	// UFMODEvent* HitTakenSound = nullptr;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite)
	// UFMODEvent* DeathSound = nullptr;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite)
	// UFMODEvent* FlaskHealSound = nullptr;	
};
USTRUCT(BlueprintType)
struct FRangedWeaponSound
{
	GENERATED_BODY()

	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	// UFMODEvent* FireSound = nullptr;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	// UFMODEvent* ReloadSound = nullptr;
};

// AbilityTags preset
USTRUCT(BlueprintType)
struct FAbilityTagsPreset : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTagContainer AbilityTags;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTagContainer CancelAbilitiesWithTag;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTagContainer BlockAbilitiesWithTag;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTagContainer ActivationOwnedTags;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTagContainer ActivationRequiredTags;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTagContainer ActivationBlockedTags;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTagContainer SourceRequiredTags;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTagContainer SourceBlockedTags;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTagContainer TargetRequiredTags;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTagContainer TargetBlockedTags;

	

};

USTRUCT(BlueprintType)
struct FCharacterData : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "")
	FRotator StartingRotation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float CorpseDuration;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FGameplayTagContainer DamageReactionTags;


	// Stun
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Stun")
	bool bCanBeStunned = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Stun")
	TSubclassOf<UGameplayEffect> StunEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Stun")
	float StunDuration = 0.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Stun")
	float StunImmuneDuration = 0.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Stun")
	TSubclassOf<UGameplayEffect> StunImmuneEffect;


	// Starting abilities and effects
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities")
	TMap<FString, TSubclassOf<UGameplayAbility>> StartingAbilities;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities")
	TArray<TSubclassOf<UGameplayEffect>> PassiveGameplayEffects;
};


USTRUCT(BlueprintType)
struct FWeaponInfo
{
	GENERATED_BODY()
	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName WeaponID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName WeaponClassID;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EWeaponType WeaponType = EWeaponType::Melee;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* WeaponIcon = nullptr;

};

USTRUCT(BlueprintType)
struct FRangedWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<class AProjectileBase> Projectile = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FireRate = 0.7f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 MaxClipAmmo = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ReloadTime = 1.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ForcedReloadTime = 0.2f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ReloadDelay = 1.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ShootingRange = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float DispersionByShot = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float MaxDispersion = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float DispersionDecreaseRate = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float DispersionDelayTime = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "NormalAttackFX")
	UParticleSystem* FireFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ChargeAttackFX")
	UParticleSystem* ChargeFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRangedWeaponSound WeaponSounds;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Animations")
	UAnimMontage* OnFireAnimMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Animations")
	UAnimMontage* ChargeAttackMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Animations")
	UAnimMontage* ReloadAnimMontage = nullptr;
};

USTRUCT(BlueprintType)
struct FMeleeWeaponSound
{
	GENERATED_BODY()

	// UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	// UFMODEvent* SwingSound = nullptr;
};

USTRUCT(BlueprintType)
struct FMeleeCombo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UAnimMontage* MeleeAttackMontage = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float AttackTime = 0.5f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float Damage = 10.f;
};


USTRUCT(BlueprintType)
struct FMeleeInfo
{
	GENERATED_BODY()

	// UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	// int32 Damage = 10;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FMeleeWeaponSound WeaponSound;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FHitEffects HitEffects;
	// UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	// UAnimMontage* MeleeAttackMontage = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<FMeleeCombo> MeleeComboMontage;
};

USTRUCT(BlueprintType)
struct FBlockInfo
{
	GENERATED_BODY()

	//Converting rate for convert damage into stamina consume
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(ClampMin="0"))
	float StaminaConsumeRate = 2.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(ClampMin="0", ClampMax="1"))
	float DamageNegationRate = 1.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(ClampMin="0", ClampMax="1"))
	float StaminaRecoveryRate = 0.3f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float MovementSpeed = 0.f;
};



USTRUCT(BlueprintType)
struct FDisplayInfo
{
	GENERATED_BODY()

	// Name of the interactable
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	FName ObjectName;

	// Type of interactable
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	EInteractionType InteractionType = EInteractionType::None;	
};

USTRUCT(BlueprintType)
struct FEffectInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	EEffectExecutionType ExecutionType = EEffectExecutionType::None;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	EEffectType EffectType = EEffectType::None;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FName EffectName;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UTexture2D* Icon = nullptr;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int32 Damage = 1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Pierce")
	bool bCanPierce = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Pierce", meta = (EditCondition="bCanPierce", ClampMin="0"))
	int32 PierceCount = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FHitEffects HitEffects;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "GameplayEffect")
	TSubclassOf<UGameplayEffect> GameplayEffect;
};

USTRUCT(BlueprintType)
struct FDamageHitInfo
{
	GENERATED_BODY()

	FDamageHitInfo(){}

	FDamageHitInfo(float HitDamage, AActor* HitDealer, bool CanBeBlocked = true, bool CanBeParried = true)
	{
		Damage = HitDamage;
		Dealer = HitDealer;
		bCanBeBlocked = CanBeBlocked;
		bCanBeParried = CanBeParried;
	}
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
	bool bCanBeBlocked = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
	bool bCanBeParried = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
	float Damage = 0.f;

	TSubclassOf<UGameplayEffect> HitEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
	AActor* Dealer = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
	AActor* DealerOwner = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
	AActor* TargetActor = nullptr;
};

class UResultHitInfo;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnComplete, UResultHitInfo*, Info);

UCLASS(BlueprintType)
class UResultHitInfo : public UObject
{
	GENERATED_BODY()

public:
	UResultHitInfo(){}

	UResultHitInfo(float _HitDamage, float _FinalDamage, bool IsHitBlocked = false, bool IsHitParried = false)
	{
		HitDamage = _HitDamage;
		FinalDamage = _FinalDamage;
		bHitBlocked = IsHitBlocked;
		bHitParried = IsHitParried;
	}

	UResultHitInfo(FDamageHitInfo Info)
	{
		HitDamage = Info.Damage;
		FinalDamage = Info.Damage;
		bHitBlocked = false;
		bHitParried = false;

	}

	FDamageHitInfo DamageHitInfo;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
	bool bHitBlocked = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
	bool bHitParried = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
	float HitDamage = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
	float FinalDamage = 0;

	FOnComplete OnComplete;

	void Complete()
	{
		OnComplete.Broadcast(this);
	}
};

USTRUCT(BlueprintType)
struct FEnemiesCount
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 TotalEnemies = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Skeleton = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Werewolves = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Vampires = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Bats = 0;
};

USTRUCT(BlueprintType)
struct FSkillInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SkillInfo")
	FName SkillName;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SkillParameters")
	bool bMovementSkill = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SkillParameters")
	float CastTime = 1.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SkillParameters")
	bool bHaveCooldown = true;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SkillParameters", meta=(EditCondition=bHaveCooldown))
	float SkillCooldown = 1.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SkillFX")
	UNiagaraSystem* SkillFX = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SkillFX")
	UAnimMontage* SkillAnimMontage = nullptr;
};

USTRUCT(BlueprintType)
struct FKickInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attacks")
	UAnimMontage* KickMontage = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attacks")
	float KickDamage = 50.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attacks")
	float KickSpeed = 1.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attacks")
	int32 KickChance = 40;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attacks")
	float KickRange = 300.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Attacks")
	float KickRadius = 50.f;
};

USTRUCT(BlueprintType)
struct FClimbJump
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climb")
	float ClimbJumpForce = 500.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climb")
	float ZJumpForce = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climb")
	float YJumpForce = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climb")
	float ClimbJumpRotationDelta = 0.015;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climb")
	float ClimbJumpInterpSpeed = 4.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climb")
	float ClimbJumpRotationTime = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climb")
	UAnimMontage* ClimbJumpMontage = nullptr;
};

USTRUCT(BlueprintType)
struct FClimbInfo
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climb")
	float StickSpeed = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climb")
	float ClimbUpSpeed = 1.4f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climb")
	UAnimMontage* ClimbUpMontage = nullptr;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climb")
	// UAnimMontage* WallStickMontage = nullptr;
};

UCLASS()
class GBPROJ_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};

