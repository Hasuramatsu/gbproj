// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Types.h"
#include "UObject/NoExportTypes.h"
#include "EffectBase.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API UEffectBase : public UObject
{
	GENERATED_BODY()

	
public:
	UFUNCTION(BlueprintCallable)
	virtual void InitEffect();
	virtual void DestroyEffect();

	virtual void Execute();

	FEffectInfo EffectInfo;

	//Getters
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FName GetEffectName() const {return  EffectInfo.EffectName;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UTexture2D* GetIcon() const {return EffectInfo.Icon;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	EEffectType GetEffectType() const {return EffectInfo.EffectType;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	EEffectExecutionType GetExecutionType() const {return EffectInfo.ExecutionType;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FEffectInfo GetEffectInfo() const {return EffectInfo;}
};

UCLASS()
class GBPROJ_API UEffectInstant : public UEffectBase
{
	GENERATED_BODY()
	
public:
	virtual void InitEffect() override;
	virtual void DestroyEffect() override;

	UFUNCTION(BlueprintCallable)
	virtual void Execute() override;
	
};

UCLASS()
class GBPROJ_API UEffectOverTime : public UEffectBase
{
	GENERATED_BODY()

	FTimerHandle DurationTimerHandle;
	FTimerHandle ExecutionTimerHandle;
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (ClampMin = "0"))
	float Duration = 0.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (ClampMin = "0"))
	float TickRate = 0.f;
	
	virtual void InitEffect() override;
	virtual void DestroyEffect() override;

	virtual void Execute() override;
	
};