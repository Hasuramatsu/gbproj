// Fill out your copyright notice in the Description page of Project Settings.


#include "EffectBase.h"

void UEffectBase::InitEffect()
{
}

void UEffectBase::DestroyEffect()
{
}

void UEffectBase::Execute()
{
}

void UEffectInstant::InitEffect()
{
	Super::InitEffect();
}

void UEffectInstant::DestroyEffect()
{
}

void UEffectInstant::Execute()
{
}

void UEffectOverTime::InitEffect()
{
	Super::InitEffect();
}

void UEffectOverTime::DestroyEffect()
{
}

void UEffectOverTime::Execute()
{
}
