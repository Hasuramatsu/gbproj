// Fill out your copyright notice in the Description page of Project Settings.


#include "GBLib.h"

#include "AbilitySystemInterface.h"
#include "GBProj/AbilitySystem/GBAbilitySystemComponent.h"
#include "Kismet/GameplayStatics.h"

float UGBLib::ApplyAbilityDamageHit(FDamageHitInfo DamageHitInfo, AActor* DamagedActor, AActor* DamageCauser)
{
	IAbilitySystemInterface* ASISource = Cast<IAbilitySystemInterface>(DamageCauser);
	if(ASISource)
	{
		UGBAbilitySystemComponent* ASCSource = Cast<UGBAbilitySystemComponent>(ASISource->GetAbilitySystemComponent());
		if(ASCSource)
		{
			ASCSource->ApplyAbilityDamageHitToActor(DamageHitInfo, DamagedActor);
		}
		
	}
	
	return DamageHitInfo.Damage;
}

bool UGBLib::IsFacing(const AActor* Source, const AActor* Target)
{
	//const float Scalar = ScalarTwoActors(Source, Target);

	return ScalarTwoActors(Source, Target) > 0.6;
}

float UGBLib::ScalarTwoActors(const AActor* Source, const AActor* Target)
{
	const FVector SourceDirection = Source->GetActorForwardVector();
	FVector DirectionFromTarget = Target->GetActorLocation() - Source->GetActorLocation();
	DirectionFromTarget.Normalize();

	return FVector::DotProduct(SourceDirection, DirectionFromTarget);
}

FGameplayAbilityTargetDataHandle UGBLib::MakeTargetDataFromActor(AActor* Actor)
{
	// Construct TargetData
	FGameplayAbilityTargetData_ActorArray*	NewData = new FGameplayAbilityTargetData_ActorArray();
	NewData->TargetActorArray.Add(Actor);
	FGameplayAbilityTargetDataHandle		Handle(NewData);
	return Handle;
}

FGameplayEventData UGBLib::MakeDefaultEventData(AActor* Source, AActor* Target,
	FGameplayTag EventTag = FGameplayTag::EmptyTag, float EventMagnitude = 0.f ,  UObject* OptObject = nullptr,
	UObject* OptObjectTwo = nullptr)
{
	FGameplayEventData Data;

	UAbilitySystemComponent* SourceASC = nullptr;
	UAbilitySystemComponent* TargetASC = nullptr;

	IAbilitySystemInterface* SourceASI = Cast<IAbilitySystemInterface>(Source->GetInstigator());
	if(SourceASI)
	{
		SourceASC = SourceASI->GetAbilitySystemComponent();
	}
	IAbilitySystemInterface* TargetASI = Cast<IAbilitySystemInterface>(Target);
	if(TargetASI)
	{
		TargetASC = SourceASI->GetAbilitySystemComponent();
	}

	Data.EventTag = EventTag;
	Data.Instigator = Source->GetInstigator();
	Data.Target = Target;
	Data.OptionalObject = OptObject;
	Data.OptionalObject2 = OptObjectTwo;
	Data.EventMagnitude = EventMagnitude;
	//Data.TargetData = MakeTargetDataFromActor(Target);

	if(SourceASC)
	{
		 SourceASC->GetOwnedGameplayTags(Data.InstigatorTags);
	}
	if(TargetASC)
	{
		TargetASC->GetOwnedGameplayTags(Data.TargetTags);
	}

	return Data;
}

FRotator UGBLib::GetReverseRotation(FRotator Rotation)
{
	return Rotation.Yaw > 0 ? FRotator{0.f, -90.f, 0.f} : FRotator{0.f, 90.f, 0.f};
}

UGBGameInstance* UGBLib::GetGBGameInstance(const UObject* WorldContextObject)
{
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);

	return World ? Cast<UGBGameInstance>(World->GetGameInstance()) : nullptr;
}

void UGBLib::HighlightMash(UStaticMeshComponent* Mesh, bool Flag)
{
	if(Mesh)
	{
		int32 StencilValue = Flag ? 2 : 0;

		Mesh->SetRenderCustomDepth(Flag);
		Mesh->SetCustomDepthStencilValue(StencilValue);
	}
}
