// Fill out your copyright notice in the Description page of Project Settings.


#include "GBPlayerStateBase.h"
#include "Kismet/GameplayStatics.h"

AGBPlayerStateBase::AGBPlayerStateBase()
{
	FlaskComponent = CreateDefaultSubobject<UFlaskComponent>(TEXT("FlaskComponent"));
}

void AGBPlayerStateBase::BeginPlay()
{
	Super::BeginPlay();

	CurrentLives = MaxLives;
	
}



void AGBPlayerStateBase::AddCurrentLives(const int32 Value)
{
	if(CurrentLives > 0)
	{
		CurrentLives += Value;	
	}
	
	if(CurrentLives == 0)
	{
		bCanContinue = false;
	}
}



	
