// // Fill out your copyright notice in the Description page of Project Settings.
//
//
// #include "GBCharacter.h"
//
// #include "GBPlayerControllerBase.h"
// #include "GBGameStateBase.h"
// #include "GBPlayerStateBase.h"
// #include "Camera/CameraComponent.h"
// #include "Components/CapsuleComponent.h"
// #include "GameFramework/CharacterMovementComponent.h"
// #include "GameFramework/SpringArmComponent.h"
// #include "Engine/World.h"
// #include "GBProj/Interfaces/InteractInterface.h"
// #include "Kismet/GameplayStatics.h"
// #include "Kismet/KismetMathLibrary.h"
// #include "FMODBlueprintStatics.h"
// #include "DrawDebugHelpers.h"
//
//
//
// // Sets default values
// AGBCharacter::AGBCharacter()
// {
//  	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
// 	PrimaryActorTick.bCanEverTick = true;
//
// 	//Set Player Capsule Collision
// 	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.f);
//
// 	//Don't rotate character to camera direction
// 	bUseControllerRotationPitch = false;
// 	bUseControllerRotationYaw = false;
// 	bUseControllerRotationRoll = false;
//
// 	//Configurate Character movement
// 	GetCharacterMovement()->bOrientRotationToMovement = true;  //rotate character to moving direction
// 	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
// 	GetCharacterMovement()->bConstrainToPlane = true;
// 	GetCharacterMovement()->bSnapToPlaneAtStart = true;
//
// 	//Create Camera Boom
// 	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
// 	CameraBoom->SetupAttachment(RootComponent);
// 	CameraBoom->SetUsingAbsoluteRotation(true); //Don't rotate camera when character does
// 	CameraBoom->TargetArmLength = 600.f;
// 	CameraBoom->bDoCollisionTest = false; //Don't want pull camera in when it collides with level
//
// 	//Create Camera
// 	GBCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("GBCamera"));
// 	GBCamera->SetupAttachment(CameraBoom);
// 	GBCamera->bUsePawnControlRotation = false; //Camera does not rotate relative to arm
//
// 	//Create SceneComponent
// 	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
// 	SceneComponent->SetupAttachment(GetCapsuleComponent());
//
// 	//Components
// 	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
// 	HealthComponent->OnDead.AddDynamic(this, &AGBCharacter::DeadEvent);
// 	StaminaComponent = CreateDefaultSubobject<UStaminaComponent>(TEXT("StaminaComponent"));
// 	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
// 	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
// 	AttributeSet = CreateDefaultSubobject<UGBAttributeSet>(TEXT("AttributeSet"));
//
// 	//Lock X position
// 	GetCharacterMovement()->SetPlaneConstraintNormal(FVector(1,0,0));
// 	GetCharacterMovement()->bConstrainToPlane = true;
// 	GetCharacterMovement()->bSnapToPlaneAtStart = true;
// 	
// 	
// 	// Activate ticking in order to update the cursor every frame.
// 	PrimaryActorTick.bCanEverTick = true;
// 	PrimaryActorTick.bStartWithTickEnabled = true;
//
// 	SpeedContainer.Add(ECombatState::MeleeState, GetCharacterMovement()->MaxWalkSpeed);
// 	SpeedContainer.Add(ECombatState::AimState, AimMoveSpeed);
// 	SpeedContainer.Add(ECombatState::BlockState, BlockInfo.MovementSpeed);
// 	SpeedContainer.Add(ECombatState::DodgeState, DodgeSpeed);
//
// }
//
// // Called when the game starts or when spawned
// void AGBCharacter::BeginPlay()
// {
// 	Super::BeginPlay();
// 	AGBPlayerController* MyPlayerController = Cast<AGBPlayerController>(GetController());
// 	if(MyPlayerController)
// 	{
// 		GBPlayerController = MyPlayerController;
// 	}
// 	else
// 	{
// 		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Red, TEXT("Failed controler"));
// 	}
// 	// if(GetCharacterMovement())
// 	// {
// 	// 	BaseSpeed = GetCharacterMovement()->MaxWalkSpeed;
// 	// }
//
// 	//Bind AbilityComponent
// 	if(HealthComponent)
// 	{
// 		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetHealthAttribute()).AddUObject(HealthComponent, &UHealthComponent::SetHealthAttribute);
// 		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxHealthAttribute()).AddUObject(HealthComponent, &UHealthComponent::SetMaxHealthAttribute);
// 	}
// 	if(StaminaComponent)
// 	{
// 		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetStaminaAttribute()).AddUObject(StaminaComponent, &UStaminaComponent::SetStaminaAttribute);
// 		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMaxStaminaAttribute()).AddUObject(StaminaComponent, &UStaminaComponent::SetMaxStaminaAttribute);
// 	}
// 	if(GetCharacterMovement())
// 	{
// 		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetMovementSpeedAttribute()).AddUObject(this, &AGBCharacter::OnMovementSpeedAttributeChanged);
// 	}
//
// 	
// 	InitMeleeWeapon();
// 	InitWeapon(CurrentWeaponIndex);
// 	SwapCurrentWeapon(ECurrentWeaponType::Primary);
// 	SetActorRotation(FRotator(0, 90, 0));
//
// 	PreviousCombatState = CombatState;
// }
// ////////////////////
// //Dodge system
// ///////////////////
// void AGBCharacter::Dodge()
// {
// 	if(CheckCanDodge())
// 	{
// 		bDodging = true;
// 		bDodgeReady = false;	
// 		if(CombatState == ECombatState::BlockState)
// 		{
// 			ExitBlockStance();
// 		}
// 		DeactivateAllWeapon();
// 		if(bIsPushing)
// 		{
// 			StopPush.Broadcast();
// 			LastMovementInputVector = GetActorForwardVector() * -1;
// 		}
// 		else
// 		{
// 			LastMovementInputVector = GetCharacterMovement()->Velocity.IsNearlyZero() ? GetActorForwardVector() : GetLastMovementInputVector();
// 		}
// 		
// 		SetCombatState(ECombatState::DodgeState);
// 		GetCharacterMovement()->MaxAcceleration = 10000.f;
// 		HealthComponent->BecomeImmune(DodgeDuration);
// 		if(DodgeMontage)
// 		{
// 			const float PlayRate = DodgeMontage->GetPlayLength() / DodgeDuration;
// 			PlayAnimMontage(DodgeMontage, PlayRate);
// 		}
// 		GetCapsuleComponent()->SetCollisionProfileName("DeadBody");
// 		OnDodgeStart();
// 		GetWorld()->GetTimerManager().SetTimer(DodgeHandle, this, &AGBCharacter::EndDodge, DodgeDuration);
// 		GetWorld()->GetTimerManager().SetTimer(DodgeCooldownHandle, this, &AGBCharacter::ResetDodgeCooldown, DodgeCooldown);
// 	}
// }
//
// void AGBCharacter::EndDodge()
// {
// 	if(bDodging)
// 	{
// 		bDodging = false;
// 		SetCombatState(ECombatState::MeleeState);
// 		GetCharacterMovement()->MaxAcceleration = 2048.f;
// 		GetCapsuleComponent()->SetCollisionProfileName("Pawn");
// 		ActivateWeapon();
// 		LastMovementInputVector = FVector::ZeroVector;
// 		OnDodgeEnd();
// 	}
// }
//
//
// bool AGBCharacter::CheckCanDodge() const
// {
// 	if(StaminaComponent)
// 	{
// 		if(!bDodgeReady)
// 		{
// 			return false;
// 		}
// 		if(GetCharacterMovement()->IsFalling())
// 		{
// 			return false;
// 		}
// 	//	if(!StaminaComponent->PayStamina(DodgeStaminaCost))
// 		{
// 			return false;
// 		}
// 		return true;	
// 	}
// 	return false;
// }
//
// //////////////////
// ///Block
// /////////////////
//
//
// bool AGBCharacter::CheckCanEnterBlock() const
// {
// 	if(CombatState == ECombatState::DodgeState)
// 	{
// 		return false;
// 	}
// 	return true;
// }
//
// void AGBCharacter::EnterBlockStance()
// {
// 	if(CheckCanEnterBlock())
// 	{
// 		SetCombatState(ECombatState::BlockState);
// 	//	StaminaComponent->ModifyRecoveryTimeByRate(BlockInfo.StaminaRecoveryRate);
// 		OnBlockStart_Event();
// 	}
// 	else
// 	{
// 		GetWorld()->GetTimerManager().SetTimer(EnterBlockTimer, this, &AGBCharacter::EnterBlockStance, BlockTimerTickTime, false);
// 	}
// }
//
// void AGBCharacter::ExitBlockStance(ECombatState NewState)
// {
// 	if(GetWorld()->GetTimerManager().IsTimerActive(EnterBlockTimer))
// 	{
// 		GetWorld()->GetTimerManager().ClearTimer(EnterBlockTimer);
// 	}
// 	if(CombatState == ECombatState::BlockState)
// 	{
// 		SetCombatState(NewState);
// 	//	StaminaComponent->ModifyRecoveryTimeByRate(1.f);
// 		OnBlockEnd_Event();
// 	}
// 	
// }
//
//
// ////////////////
// ///ClimbSystem
// ///////////////
//
// //Tick for check character can stick wall
// void AGBCharacter::ClimbTick()
// {
// 	bool Result = false;
// 	FHitResult HitResult;
// 	FVector Start = GetActorLocation();
// 	FVector End;
// 	int k = 0;	
// 	while(!Result && k < 10)
// 	{
// 		Start += FVector(0, 0, 10);
// 		End = Start + (GetActorForwardVector() * 90);
// 		k++;
// 		GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECC_GameTraceChannel4);
// 		if(HitResult.bBlockingHit)
// 		{
// 			FVector SecondEnd = HitResult.Location + (GetActorForwardVector() * 10);
// 			FVector SecondStart = SecondEnd + FVector(0,0, GetCapsuleComponent()->GetScaledCapsuleHalfHeight()/2);
// 			GetWorld()->LineTraceSingleByChannel(HitResult, SecondStart, SecondEnd, ECC_GameTraceChannel4);
// 			if(HitResult.IsValidBlockingHit())
// 			{
// 				Result = true;
// 				GetWorld()->GetTimerManager().ClearTimer(ClimbTraceTimer);
// 				StickWall(HitResult);
// 			}		
// 		}
// 	}
//
//
// }
//
// //Stick character to wall and set it in climbing state
// void AGBCharacter::StickWall(FHitResult& HitResult)
// {
// 	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);
// 	GetCharacterMovement()->StopMovementImmediately();
// 	SetCharacterState(ECharacterState::Climbing);
// 	
// 	const FVector Location = (HitResult.Location - FVector(0, 0, 90)) - GetActorForwardVector() * 45; //HitResult.Normal.RightVector
// 	//FRotator Rotation = HitResult.Normal.Rotation();
// 	FRotator Rotation = GetActorForwardVector().Rotation();
// 	//Rotation.Roll = Rotation.Roll - 180;
//
// 	FLatentActionInfo LatentInfo;
// 	LatentInfo.CallbackTarget = this;
// 	LatentInfo.ExecutionFunction = FName("OnStickWallEnd");
// 	LatentInfo.Linkage = 0;
// 	LatentInfo.UUID = 0;
// 	UKismetSystemLibrary::MoveComponentTo(GetCapsuleComponent(), Location, Rotation, false, false, 0.3, false, EMoveComponentAction::Type::Move, LatentInfo);
// }
//
// //Rotate character during back jump from climb
// void AGBCharacter::JumpRotationTick()
// {
// 	FRotator NewRotation = UKismetMathLibrary::RInterpTo(GetActorRotation(), BackJumpRotation, ClimbJump.ClimbJumpRotationDelta, ClimbJump.ClimbJumpInterpSpeed);
// 	SetActorRotation(NewRotation);
// 	RotationTime += ClimbJump.ClimbJumpRotationDelta;
// 	if(RotationTime >= ClimbJump.ClimbJumpRotationTime)
// 	{
// 		GetWorld()->GetTimerManager().ClearTimer(JumpRotationTimer);
// 		RotationTime = 0.f;
// 		SetCharacterState(ECharacterState::Walking);
// 		OnJumped();
// 	}
// }
//
// //Check character can climbup
// bool AGBCharacter::AttemptClimbUp()
// {
// 	if(CharacterState == ECharacterState::Climbing)
// 	{
// 		FVector Start = GetActorLocation() + FVector(0,0,190) + GetActorForwardVector() * 20;
// 		FVector End = Start + GetActorForwardVector() * 40;
// 		TArray<AActor*> Actors;
// 		FHitResult HitResult;
// 		UKismetSystemLibrary::CapsuleTraceSingle(GetWorld(), Start, End, 30, 90, ETraceTypeQuery::TraceTypeQuery1 , false, Actors, EDrawDebugTrace::ForDuration, HitResult, true);
// 		if(!HitResult.bBlockingHit)
// 		{
// 			GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Cyan, TEXT("AGBCharacter::AttemptClimbUp -- ValidLocation"));
// 			GetWorld()->LineTraceSingleByChannel(HitResult, HitResult.TraceEnd, HitResult.TraceEnd - FVector(0,0, 120), ECC_GameTraceChannel4);
// 			DrawDebugLine(GetWorld(), HitResult.TraceEnd, HitResult.TraceEnd - FVector(0,0, 120), FColor::Cyan, true, -1, 0 ,5);
// 			if(HitResult.IsValidBlockingHit())
// 			{
// 				GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Cyan, TEXT("AGBCharacter::AttemptClimbUp -- ValidClimb"));
// 				End = HitResult.Location + FVector(0, 0, 90);
// 				SetCharacterState(ECharacterState::ClimbUp);
// 				FLatentActionInfo LatentActionInfo;
// 				LatentActionInfo.CallbackTarget = this;
// 				LatentActionInfo.ExecutionFunction = FName("OnClimbUpEnd");
// 				LatentActionInfo.Linkage = 0;
// 				LatentActionInfo.UUID = 1;
// 				UKismetSystemLibrary::MoveComponentTo(GetCapsuleComponent(), End, GetActorRotation(), false, false, ClimbInfo.ClimbUpSpeed, true, EMoveComponentAction::Type::Move, LatentActionInfo);
// 				if(ClimbInfo.ClimbUpMontage)
// 				{
// 					float PlayRate = ClimbInfo.ClimbUpMontage->GetPlayLength() / ClimbInfo.ClimbUpSpeed;
// 					PlayAnimMontage(ClimbInfo.ClimbUpMontage, PlayRate);
// 				}
// 				return true;
// 			}
// 			GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Cyan, TEXT("AGBCharacter::AttemptClimbUp -- Not valid climb"));
// 		}
// 		GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Cyan, TEXT("AGBCharacter::AttemptClimbUp -- Niot valid location"));
// 	}
// 	return false;
// }
//
// //Check character can jump up from climbing stance
// bool AGBCharacter::AttemptClimbJump()
// {
// 	if(CharacterState == ECharacterState::Climbing)
// 	{
// 		FHitResult HitResult;
// 		FVector Start = GetActorLocation() + FVector(0, 0, 220);
// 		FVector End = Start + GetActorForwardVector() * 40;
// 		GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECC_GameTraceChannel4);
// 		if(HitResult.bBlockingHit)
// 		{
// 			End = HitResult.Location + (GetActorForwardVector() * 10);
// 			Start = End + FVector(0,0, GetCapsuleComponent()->GetScaledCapsuleHalfHeight()/2);		
// 			GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECC_GameTraceChannel4);
// 			if(HitResult.IsValidBlockingHit())
// 			{
// 				const FVector EndLocation = HitResult.Location - GetActorForwardVector() * 45 - FVector(0, 0, 90);
// 				FLatentActionInfo LatentActionInfo;
// 				LatentActionInfo.Linkage = 0;
// 				LatentActionInfo.CallbackTarget = this;
// 				LatentActionInfo.UUID = 2;
// 				LatentActionInfo.ExecutionFunction = FName("");
// 				UKismetSystemLibrary::MoveComponentTo(GetCapsuleComponent(), EndLocation, GetActorRotation(), false, false, 0.3, false, EMoveComponentAction::Type::Move, LatentActionInfo);
// 				if(ClimbJump.ClimbJumpMontage)
// 				{
// 					float PlayRate = ClimbJump.ClimbJumpMontage->GetPlayLength() / ClimbJump.ClimbJumpInterpSpeed;
// 					PlayAnimMontage(ClimbJump.ClimbJumpMontage, PlayRate);
// 				}
// 			}
// 		}
// 	}
// 	return false;
// }
// ///////////////
//
//
// void AGBCharacter::SecondaryAction(const bool Flag)
// {
// 	if(Flag)
// 	{
// 		AimDelegate.BindUFunction(this, "EnterAimState", true);
// 		GetWorld()->GetTimerManager().SetTimer(AimTimer, AimDelegate, HoldTimeForAim, false);
// 	}
// 	else if(CombatState == ECombatState::AimState)
// 	{
// 		EnterAimState(false);
// 	}
// 	else
// 	{
// 		//TODO bind to anim
// 		SwapCurrentWeapon(ECurrentWeaponType::Secondary);
// 		CurrentWeapon->MakeSingleShot(GetClosestEnemyLocation());
// 		GetWorld()->GetTimerManager().ClearTimer(AimTimer);
// 		SwapCurrentWeapon(ECurrentWeaponType::Primary);
// 	}
// }
//
// void AGBCharacter::HealEnd()
// {
// 	ActivateWeapon();
// 	GetWorld()->GetTimerManager().ClearTimer(FlaskTimerHandle);
// 	//PlayAnimMontage(MyPlayerState->FlaskComponent->GetFlaskMontage, -1.f);
// 	bUsingFlask = false;
// }
//
// // Called every frame
// void AGBCharacter::Tick(float DeltaTime)
// {
// 	Super::Tick(DeltaTime);
// 	
// 	RotationTick(DeltaTime);
// 	MovementTick(DeltaTime);
//
// }
//
// void AGBCharacter::RotationTick(float DeltaTime)
// {
// 	if(GBPlayerController && bIsAlive)
// 	{
// 		const bool Case = bIsPushing || CharacterState == ECharacterState::Climbing;
// 		if(!Case)
// 		{
// 			if(CombatState == ECombatState::AimState)
// 			{
// 				//if(FVector::Dist(GetActorLocation(), TargetLocation) > 200.f)
// 				//{
// 				FHitResult TraceHitResult;
// 				GBPlayerController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, TraceHitResult);
// 				TargetLocation = TraceHitResult.Location;
// 				const float Yaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), TargetLocation).Yaw;
// 				SetActorRotation(FRotator(0.f, Yaw, 0.f));
// 				PitchAngle = UKismetMathLibrary::FindLookAtRotation(SceneComponent->GetComponentLocation(), TargetLocation).Pitch;
// 				//}
// 			}
// 			else
// 			{
// 				if(AxisY > 0)
// 				{
// 					SetActorRotation(FRotator(0, 90, 0));
// 				}
// 				else if(AxisY < 0)
// 				{
// 					SetActorRotation(FRotator(0, -90, 0));
// 				}
// 				
// 				PitchAngle = 0.f;
// 			}
// 		}
// 	}
// 	else
// 	{
// 		GEngine->AddOnScreenDebugMessage(-1, 0, FColor::Red, TEXT("No correct controller"));
// 	}
// }
//
//  void AGBCharacter::MovementTick(float DeltaTime)
// {
// 	//TODO check fix
// 	if(bIsAlive)
// 	{
// 		if(bIsPushing)
// 		{
// 			const bool FirstCase = (AxisY > 0) == (GetActorForwardVector().Y > 0);
// 			const bool SecondCase = (AxisY < 0) == (GetActorForwardVector().Y < 0);
// 			if(bIsGrabbing &&  InteractionTarget)
// 			{
// 				const float Offset = AxisY * DeltaTime * GetCharacterMovement()->MaxWalkSpeed;
// 				InteractionTarget->AddActorWorldOffset(FVector(0.f, Offset, 0.f), true);	
// 			}
// 			else if((FirstCase || SecondCase) &&  InteractionTarget)
// 			{
// 				const float Offset = AxisY * DeltaTime * GetCharacterMovement()->MaxWalkSpeed;
// 				InteractionTarget->AddActorWorldOffset(FVector(0.f, Offset, 0.f), true);
// 			}
// 			else
// 			{
// 				StopPush.Broadcast();
// 			}
//
// 			// Push animation 
// 			if(AxisY != 0)
// 			{
// 				if((AxisY > 0) == (GetActorForwardVector().Y > 0))
// 				{
// 					PushDirection = 1.f;
// 				}
// 				else
// 				{
// 					PushDirection = -1.f;
// 				}
// 			}
// 			else
// 			{
// 				PushDirection = 0.f;	
// 			}
//
// 		}
// 		else if(bDodging)
// 		{
// 			AddMovementInput(LastMovementInputVector, 1);
// 		}
// 		else
// 		{
// 			if(CharacterState == ECharacterState::Walking)
// 			{
// 				AddMovementInput(FVector(0.f, 1, 0.f), AxisY);
// 			}
// 		}
// 	}
// 	
// }
//
// void AGBCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
// {
// 		Super::SetupPlayerInputComponent(PlayerInputComponent);
//
// 		PlayerInputComponent->BindAxis(TEXT("Move"), this, &AGBCharacter::InputAxisY);
// 		PlayerInputComponent->BindAction<FBoolInput>(TEXT("Fire"), EInputEvent::IE_Pressed, this, &AGBCharacter::InputAttack, true);
// 		PlayerInputComponent->BindAction<FBoolInput>(TEXT("Fire"), EInputEvent::IE_Released, this, &AGBCharacter::InputAttack, false);
// 		PlayerInputComponent->BindAction<FBoolInput>(TEXT("AimState"), EInputEvent::IE_Pressed, this, &AGBCharacter::SecondaryAction, true);
// 		PlayerInputComponent->BindAction<FBoolInput>(TEXT("AimState"), EInputEvent::IE_Released, this, &AGBCharacter::SecondaryAction, false);
// 		PlayerInputComponent->BindAction(TEXT("Reload"), EInputEvent::IE_Pressed, this, &AGBCharacter::Reload);
// 		PlayerInputComponent->BindAction(TEXT("Dodge"), EInputEvent::IE_Pressed, this, &AGBCharacter::Dodge);
// 		PlayerInputComponent->BindAction<FSlotInput>(TEXT("FirstSlot"), EInputEvent::IE_Pressed, this, &AGBCharacter::SwitchWeaponByIndex, 0);
// 		PlayerInputComponent->BindAction<FSlotInput>(TEXT("SecondSlot"), EInputEvent::IE_Pressed, this, &AGBCharacter::SwitchWeaponByIndex, 1);
// 		PlayerInputComponent->BindAction(TEXT("MeleeAttack"), EInputEvent::IE_Pressed, this, &AGBCharacter::EnterBlockStance);
// 		PlayerInputComponent->BindAction(TEXT("MeleeAttack"), EInputEvent::IE_Released, this, &AGBCharacter::InputExitBlockStance);
// 		PlayerInputComponent->BindAction<FBoolInput>(TEXT("Interact"), EInputEvent::IE_Pressed, this, &AGBCharacter::Interact, true);
// 		PlayerInputComponent->BindAction<FBoolInput>(TEXT("Interact"), EInputEvent::IE_Released, this, &AGBCharacter::Interact, false);
// 		PlayerInputComponent->BindAction<FBoolInput>(TEXT("Jump"), EInputEvent::IE_Pressed, this, &AGBCharacter::InputJump, true);
// 		PlayerInputComponent->BindAction<FBoolInput>(TEXT("Jump"), EInputEvent::IE_Released, this, &AGBCharacter::InputJump, false);
// 		PlayerInputComponent->BindAction(TEXT("Down"), EInputEvent::IE_Pressed, this, &AGBCharacter::DownInput);
// 		PlayerInputComponent->BindAction<FBoolInput>(TEXT("FlaskHeal"), EInputEvent::IE_Pressed, this, &AGBCharacter::InputFlaskUse, true);
// 		PlayerInputComponent->BindAction<FBoolInput>(TEXT("FlaskHeal"), EInputEvent::IE_Released, this, &AGBCharacter::InputFlaskUse, false);
// }
//
//
// void AGBCharacter::ActivateWeapon()
// {
// 	const ECurrentWeaponType Temp = CombatState == ECombatState::AimState ? ECurrentWeaponType::Secondary : ECurrentWeaponType::Primary;
// 	SwapCurrentWeapon(Temp);
// }
//
// void AGBCharacter::DeactivateAllWeapon()
// {
// 	SwapCurrentWeapon(ECurrentWeaponType::None);
// }
//
// void AGBCharacter::DestroyAllWeapon()
// {
// 	CurrentWeapon->Destroy();
// 	CurrentMeleeWeapon->Destroy();
// }
//
// void AGBCharacter::OnMovementSpeedAttributeChanged(const FOnAttributeChangeData& Data)
// {
// 	GetCharacterMovement()->MaxWalkSpeed = Data.NewValue;
// }
//
//
// void AGBCharacter::SwapCurrentWeapon(ECurrentWeaponType Type)
// {
// 	CurrentWeaponType = Type;
// 	if(Type != ECurrentWeaponType::None)
// 	{
// 		const bool bIsPrimary = Type == ECurrentWeaponType::Primary ? true : false;
// 		CurrentWeapon->ChangeActive(!bIsPrimary);
// 	//	CurrentMeleeWeapon->ChangeActive(bIsPrimary);
// 	}
// 	else
// 	{
// 		CurrentWeapon->ChangeActive(false);
// 	//	CurrentMeleeWeapon->ChangeActive(false);
// 	}
// 	
// }
//
// void AGBCharacter::InputAttack(const bool Flag)
// {
// 	if(CurrentWeaponType == ECurrentWeaponType::Primary)
// 	{
// 		if(CurrentMeleeWeapon)
// 		{
// 			MeleeAttack();
// 		}
// 	}
// 	else
// 	{
// 		if(CurrentWeapon)
// 		{
// 			CurrentWeapon->SetIsFiring(Flag);	
// 		}	
// 	}
// }
//
// void AGBCharacter::ChargeAttack(const bool Flag)
// {
// 	if(CurrentWeapon)
// 	{
// 		CurrentWeapon->UseAltFire(Flag);
// 	}
// }
//
// void AGBCharacter::MeleeAttack()
// {
// 	if(CheckCanMeleeAttack())
// 	{		
// 		//CurrentMeleeWeapon->SetActive(true);
// 		bCanMeleeAttack = false;
// 		MeleeAttack_BP();
// 	}
// }
//
// void AGBCharacter::Reload()
// {
// 	if(CombatState == ECombatState::AimState)
// 	{
// 		CurrentWeapon->ReloadStart();
// 		if(CurrentWeapon->RangedWeaponInfo.ReloadAnimMontage)
// 		{
// 			const float PlayRate =CurrentWeapon->RangedWeaponInfo.ReloadAnimMontage->GetPlayLength() /  CurrentWeapon->RangedWeaponInfo.ReloadTime;
// 			PlayAnimMontage(CurrentWeapon->RangedWeaponInfo.ReloadAnimMontage, PlayRate);
// 		}
//
// 	}
// 	
// }
//
// void AGBCharacter::SwitchWeaponByIndex(int32 Index)
// {
// 	if(InventoryComponent && Index < InventoryComponent->WeaponSlots.Num())
// 	{
// 		DeactivateAllWeapon();
// 		const bool Temp = InitWeapon(Index);
// 		if(!Temp)
// 		{
// 			UE_LOG(LogTemp, Warning, TEXT("AGBCharacter::SwitchWeaponByIndex -- InitWeapon fail"));
// 		}
// 	}
// 	else
// 	{
// 		UE_LOG(LogTemp,Warning, TEXT("AGBCharacter::SwitchWeaponByIndex -- SwitchError"));
// 	}
// }
//
//
// void AGBCharacter::InputAxisY(float Value)
// {
// 	AxisY = Value;
// }
//
// void AGBCharacter::Interact(const bool Flag)
// {
// 	if(InteractionTarget)
// 	{
// 		
// 		if(InteractionTarget->GetClass()->ImplementsInterface(UInteractInterface::StaticClass()))
// 		{
// 			IInteractInterface::Execute_Interact(InteractionTarget, this, Flag);
// 		}
// 		else
// 		{
// 			UE_LOG(LogTemp, Warning, TEXT("AGBCharacter::Interact -- Can't cast interactable"));
// 		}
//
// 	}
// }
//
// void AGBCharacter::InputJump(const bool Flag)
// {
// 	if(bIsPushing)
// 	{
// 		StopPush.Broadcast();
// 	}
// 	if(CheckCanJump())
// 	{
// 		if(CharacterState == ECharacterState::Climbing)
// 		{
// 			const bool BackJump = (AxisY > 0 && (GetActorForwardVector() * -1).Y > 0) ||  (AxisY < 0 && (GetActorForwardVector() * -1).Y < 0);
// 			if(BackJump)
// 			{
// 				GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
// 				const FVector JumpVector = (GetActorForwardVector() * -ClimbJump.YJumpForce + FVector(0,0,ClimbJump.ZJumpForce)) * ClimbJump.ClimbJumpForce;
// 				BackJumpRotation = (GetActorForwardVector() * -1).Rotation();
// 				LaunchCharacter(JumpVector, true, true);
// 				GetWorld()->GetTimerManager().SetTimer(JumpRotationTimer, this, &AGBCharacter::JumpRotationTick, 0.015, true);
// 			}
// 			else
// 			{
// 				if(!AttemptClimbUp())
// 				{
// 					if(!AttemptClimbJump())
// 					{
// 						
// 					}
// 				}
// 			}
// 		}
// 		if(Flag && CharacterState == ECharacterState::Walking)
// 		{
// 			Jump();
// 		}
// 		else
// 		{
// 			StopJumping();
// 		}
// 		FTransform HitTransform;
// 		HitTransform.SetLocation(GetActorLocation());
// 		UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), CharacterSounds.JumpStartSound, HitTransform, true);
// 	}
// }
//
// void AGBCharacter::InputFlaskUse(const bool Flag)
// {
// 	AGBPlayerStateBase* MyPlayerState = Cast<AGBPlayerStateBase>(GetPlayerState());
// 	if(MyPlayerState)
// 	{
// 		const float Delay = MyPlayerState->FlaskComponent->GetFlaskMontage ? MyPlayerState->FlaskComponent->GetFlaskMontage->GetPlayLength() : 0.5f;
// 		MyPlayerState->FlaskComponent->OnDeactivate.AddDynamic(this, &AGBCharacter::HealEnd);
// 		bCanMeleeAttack = !Flag;
// 		if(Flag)
// 		{
// 			
// 			DeactivateAllWeapon();
// 			EnterAimState(false);
// 			ExitBlockStance();
// 			//PlayAnimMontage(MyPlayerState->FlaskComponent->GetFlaskMontage, 1.f);
// 			FlaskTimerDelegate.BindUFunction(this, FName("ActivateFlask"), Flag);
// 			GetWorld()->GetTimerManager().SetTimer(FlaskTimerHandle, FlaskTimerDelegate, Delay, false);
// 			bUsingFlask = true;
// 			GetCharacterMovement()->MaxWalkSpeed = 0;
// 		}
// 		else
// 		{
// 			ActivateFlask(Flag);
// 			HealEnd();
// 		}
// 	}
// 	else
// 	{
// 		UE_LOG(LogTemp, Warning, TEXT("AGBCharacter::ActivateFlask -- can't cast to GBPlayerStateBase"));
// 	}
// }
//
// void AGBCharacter::EnterAimState(const bool Flag)
// {
// 	AGBPlayerController* MyController = Cast<AGBPlayerController>(GetController());
// 	if(MyController)
// 	{
// 		MyController->bShowMouseCursor = Flag;
// 	}
// 	if(Flag)
// 	{
// 		if(CombatState == ECombatState::BlockState)
// 		{
// 			ExitBlockStance(ECombatState::AimState);
// 		}
// 		SetCombatState(ECombatState::AimState);
// 		SwapCurrentWeapon(ECurrentWeaponType::Secondary);
// 	}
// 	else if (CombatState == ECombatState::AimState)
// 	{
// 		SetCombatState(ECombatState::MeleeState);
// 		SwapCurrentWeapon(ECurrentWeaponType::Primary);
// 	}
// 	else
// 	{
// 		PreviousCombatState = ECombatState::MeleeState;
// 		SwapCurrentWeapon(ECurrentWeaponType::Primary);
// 	}
// }
//
// void AGBCharacter::UnstickWall()
// {
// 	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
// 	SetCharacterState(ECharacterState::Walking);
// }
//
// void AGBCharacter::DownInput()
// {
// 	if(CharacterState == ECharacterState::Climbing)
// 	{
// 		UnstickWall();
// 	}
// }
//
// void AGBCharacter::DeadEvent_Implementation()
// {
// 	if(bIsAlive)
// 	{
// 		AGBPlayerStateBase* MyPlayerState = Cast<AGBPlayerStateBase>(GetPlayerState());
// 		if(MyPlayerState)
// 		{
// 			MyPlayerState->AddCurrentLives(-1);
// 		}
// 		SetIsAlive(false);
// 		DestroyAllWeapon();
// 		SetLifeSpan(TimeToDeadBody);
// 		GetCapsuleComponent()->SetCollisionProfileName("DeadBody");
// 		CharacterDead.Broadcast();
// 		FTransform HitTransform;
// 		HitTransform.SetLocation(GetActorLocation());
// 		UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), CharacterSounds.DeathSound, HitTransform, true);
// 	}
// 		
// }
//
// FVector AGBCharacter::GetClosestEnemyLocation() const
// {
// 	const TArray<TEnumAsByte<EObjectTypeQuery>> Types;
// 	TArray<AActor*> OverlappedActors;
// 	UKismetSystemLibrary::SphereOverlapActors(GetWorld(), GetActorLocation(), CurrentWeapon->RangedWeaponInfo.ShootingRange, Types, AEnemyBase::StaticClass(), TArray<AActor*>{(AActor*)this}, OverlappedActors);
// 	AActor* TempActor = nullptr;
// 	float Distance = CurrentWeapon->RangedWeaponInfo.ShootingRange;
// 	for(auto& It : OverlappedActors)
// 	{
// 		if(It->GetActorLocation().Y > GetActorLocation().Y && GetActorForwardVector().Y > 0 ||  It->GetActorLocation().Y < GetActorLocation().Y && GetActorForwardVector().Y < 0)
// 		{
// 			if(GetDistanceTo(It) < Distance)
// 			{
// 				Distance = GetDistanceTo(It);
// 				TempActor = It;
// 			}
// 		}
// 	}
// 	if(TempActor)
// 	{
// 		return TempActor->GetActorLocation();
// 	}
// 	return FVector::ZeroVector;
// }
//
// void AGBCharacter::SetIsAlive(const bool Flag)
// {
// 	bIsAlive = Flag;
// 	
// }
//
// void AGBCharacter::SetCharacterState(const ECharacterState State)
// {
// 	switch (State)
// 	{
// 		case ECharacterState::Walking:
// 			CharacterState = ECharacterState::Walking;
// 			ActivateWeapon();
// 			break;
// 		case ECharacterState::Climbing:
// 			CharacterState = ECharacterState::Climbing;
// 			DeactivateAllWeapon();
// 			break;
// 		case ECharacterState::ClimbUp:
// 			CharacterState = ECharacterState::ClimbUp;
// 			DeactivateAllWeapon();
// 			break;
// 	}
// }
//
// FVector AGBCharacter::GetTargetLocation_Implementation()
// {
// 	return TargetLocation;
// }
//
// void AGBCharacter::EnableWeapon_Implementation()
// {
// 	if(CurrentMeleeWeapon)
// 	{
// 	//	CurrentMeleeWeapon->SetActive(true);
// 	}
// }
//
// void AGBCharacter::DisableWeapon_Implementation()
// {
// 	if(CurrentMeleeWeapon)
// 	{
// 		CurrentMeleeWeapon->EndAttack();
// 	}
// }
//
// bool AGBCharacter::CanPush_Implementation()
// {
// 	if(GetCharacterMovement()->IsFalling())
// 	{
// 		return false;
// 	}
// 	if(bDodging)
// 	{
// 		return false;
// 	}
// 	if(!bIsAlive)
// 	{
// 		return false;
// 	}
// 	return true;
// }
//
// void AGBCharacter::StartPush_Implementation(AActor* Actor)
// {
// 	AddInteraction(Actor);
// 	bCanInteract = false;
// 	bCanMeleeAttack = false;
// 	bIsPushing = true;
// 	bPushing = true;
// 	DeactivateAllWeapon();
// 	const bool Case = Actor->GetActorLocation().Y > GetActorLocation().Y;
// 	if(Case)
// 	{
// 		SetActorRotation(FRotator(0.f, 90.f, 0.f));
// 	}
// 	else
// 	{
// 		SetActorRotation(FRotator(0.f, -90.f, 0.f));
// 	}
// 	if(GetCharacterMovement())
// 	{
// 		GetCharacterMovement()->bOrientRotationToMovement = false;
// 	}
//
// 	//SetCharacterMovementSpeed in BP implementation
// }
//
// void AGBCharacter::EndPush_Implementation()
// {
// 	RemoveInteraction(InteractionTarget);
// 	bCanInteract = true;
// 	bCanMeleeAttack = true;
// 	bIsPushing = false;
// 	bPushing = false;
// 	ActivateWeapon();
// 	if(GetCharacterMovement())
// 	{
// 		GetCharacterMovement()->MaxWalkSpeed = BaseSpeed;
// 		GetCharacterMovement()->bOrientRotationToMovement = true;
// 	}
// }
//
// void AGBCharacter::GrabPushed_Implementation(const bool Flag)
// {
// 	bIsGrabbing = Flag;
// 	bPulling = bIsGrabbing;
// }
//
// void AGBCharacter::AddInteraction(AActor* Interactable)
// {
// 	if(!InteractionArray.Contains(Interactable) && Interactable->GetClass()->ImplementsInterface(UInteractInterface::StaticClass()))
// 	{
// 		InteractionArray.Add(Interactable);
// 		if(bCanInteract)
// 		{
// 			InteractionTarget = Interactable;
// 		}
// 	}
// }
//
// void AGBCharacter::RemoveInteraction(AActor* Interactable)
// {
// 	if(InteractionArray.Contains(Interactable))
// 	{
// 		InteractionArray.Remove(Interactable);
// 	}
// 	if(InteractionTarget == Interactable && InteractionArray.Num() > 0)
// 	{
// 		const int32 Index = InteractionArray.Num() - 1;
// 		InteractionTarget = InteractionArray[Index];
// 	}
// }
//
// void AGBCharacter::OnReloadEnd_Implementation(bool Result)
// {
// }
//
// void AGBCharacter::OnFire_Implementation(UAnimMontage* Montage)
// {
// 	
// }
//
// void AGBCharacter::OnReload_Implementation(UAnimMontage* Montage)
// {
// }
//
// bool AGBCharacter::InitWeapon(int32 Index)
// {
// 	
// 	bool bIsSuccess = false;
// 	if(InventoryComponent)
// 	{
// 		if(CurrentWeapon == nullptr)
// 		{
// 			const TSubclassOf<AWeaponBase> Weapon = InventoryComponent->GetWeaponByIndex(Index);
// 			FVector SpawnLocation = FVector(0);
// 			FRotator SpawnRotation = FRotator(0);
//
// 			FActorSpawnParameters SpawnParameters;
// 			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
// 			SpawnParameters.Owner = this;
// 			SpawnParameters.Instigator = GetInstigator();
//
// 			ARangedWeaponBase* MyWeapon = Cast<ARangedWeaponBase>(GetWorld()->SpawnActor(Weapon, &SpawnLocation, &SpawnRotation, SpawnParameters));
// 			if(MyWeapon)
// 			{
// 				StopPush.Broadcast();
// 				const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
// 				MyWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocket"));
// 				CurrentWeapon = MyWeapon;
// 				CurrentWeapon->SetActorRelativeTransform(CurrentWeapon->OffsetTransform);
// 				CurrentWeapon->OnFire.AddDynamic(this, &AGBCharacter::OnFire);
// 				CurrentWeapon->OnReloadStart.AddDynamic(this, &AGBCharacter::OnReload);
// 				CurrentWeapon->ReloadResult.AddDynamic(this, &AGBCharacter::OnReloadEnd);
// 				bIsSuccess = true;
// 				CurrentWeaponIndex = Index;
// 	//			CurrentWeapon->SetCurrentAmmo(InventoryComponent->GetWeaponAmmo(Weapon));
// 				CurrentWeapon->SetDeadZoneComponent(SceneComponent);
// 				OnWeaponInit.Broadcast(CurrentWeapon);
// 			}
// 		}
// 	}
// 	return bIsSuccess;
// }
//
// bool AGBCharacter::InitMeleeWeapon()
// {
// 	if(InventoryComponent)
// 	{
// 		FVector SpawnLocation = FVector(0);
// 		FRotator SpawnRotation = FRotator(0);
//
// 		FActorSpawnParameters SpawnParameters;
// 		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
// 		SpawnParameters.Owner = this;
// 		SpawnParameters.Instigator = GetInstigator();
//
// 		AMeleeWeaponBase* MyWeapon = Cast<AMeleeWeaponBase>(GetWorld()->SpawnActor(InventoryComponent->MeleeWeapon, &SpawnLocation, &SpawnRotation, SpawnParameters));
// 		if(MyWeapon)
// 		{
// 			const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
// 			MyWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocket"));
// 			CurrentMeleeWeapon = MyWeapon;
// 			CurrentMeleeWeapon->SetActorRelativeTransform(CurrentMeleeWeapon->OffsetTransform);
// 		}
// 		else
// 		{
// 			return false;
// 		}
// 	}
// 	else
// 	{
// 		return false;
// 	}
// 	return true;
// }
//
// void AGBCharacter::ActivateFlask(const bool Flag)
// {
// 	 AGBPlayerStateBase* MyPlayerState = Cast<AGBPlayerStateBase>(GetPlayerState());
// 	if(MyPlayerState)
// 	{
// 		MyPlayerState->FlaskComponent->ActivateFlaskHeal(Flag);
// 	}
// }
//
// float AGBCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
//                                AActor* DamageCauser)
// {
// 	bool bBlockedDamage = false;
//
// 	if(CombatState == ECombatState::BlockState)
// 	{
// 		if(StaminaComponent)
// 		{
// 			bool FacingHit;
// 			if(DamageCauser->GetOwner())
// 			{
// 				const AActor* Damager = DamageCauser->GetOwner();
// 				FacingHit = (GetActorForwardVector().Y > 0 && Damager->GetActorLocation().Y > GetActorLocation().Y) || (GetActorForwardVector().Y < 0 && Damager->GetActorLocation().Y < GetActorLocation().Y);
// 			}
// 			else
// 			{
// 				FacingHit = (GetActorForwardVector().Y > 0 && DamageCauser->GetActorLocation().Y > GetActorLocation().Y) || (GetActorForwardVector().Y < 0 && DamageCauser->GetActorLocation().Y < GetActorLocation().Y);
// 			}
// 			
//
// 			if(FacingHit)
// 			{
// 				//if stamina is zero will return false
// 			//	if(StaminaComponent->DrainStamina(DamageAmount * BlockInfo.StaminaConsumeRate))
// 				// {
// 				// 	
// 				// 	OnBlockingHit_Event();
// 				// }
// 				// else
// 				// {
// 				// 	ExitBlockStance();
// 				// 	//TODO Stun action on block break;
// 				// 	OnBlockBreak_Event();
// 				// }
// 				// bBlockedDamage = true;
// 			}
// 		}
// 	}
//
// 	if(!bBlockedDamage || BlockInfo.DamageNegationRate < 1)
// 	{
// 		if(BlockInfo.DamageNegationRate < 1)
// 		{
// 			DamageAmount *= BlockInfo.DamageNegationRate;
// 		}
// 		Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
// 		if(HealthComponent)
// 		{
// 			if(HealthComponent->ReceiveDamage(DamageAmount))
// 			{
// 				FTransform HitTransform;
// 				HitTransform.SetLocation(GetActorLocation());
// 				UFMODBlueprintStatics::PlayEventAtLocation(GetWorld(), CharacterSounds.HitTakenSound, HitTransform, true);
// 			}
//
// 		}
// 	}
// 	return DamageAmount;
// }
//
// void AGBCharacter::OnJumped_Implementation()
// {
// 		GetWorld()->GetTimerManager().SetTimer(ClimbTraceTimer, this, &AGBCharacter::ClimbTick, ClimbTraceTickRate, true);
// }
//
// void AGBCharacter::Landed(const FHitResult& Hit)
// {
// 	Super::Landed(Hit);
// 	GetWorld()->GetTimerManager().ClearTimer(ClimbTraceTimer);
// }
//
//
// bool AGBCharacter::CheckCanJump() const
// {
// 	if(bDodging)
// 	{
// 		return false;
// 	}
// 	return true;
// }
//
// bool AGBCharacter::CheckCanMeleeAttack() const
// {
// 	if(bDodging)
// 	{
// 		return false;
// 	}
// 	if(CombatState != ECombatState::MeleeState)
// 	{
// 		return false;
// 	}
// 	if(bPushing)
// 	{
// 		return false;
// 	}
// 	return bCanMeleeAttack;
// }
//
// void AGBCharacter::OnClimbUpEnd()
// {
// 	SetCharacterState(ECharacterState::Walking);
// 	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
// }
//
// void AGBCharacter::OnStickWallEnd()
// {
// 	
// }
//
//
// void AGBCharacter::SetCombatState(const ECombatState NewState)
// {
// 	
// 	if(PreviousCombatState != CombatState && CombatState != ECombatState::BlockState && CombatState != ECombatState::DodgeState)
// 	{
// 		PreviousCombatState = CombatState;
// 	}
// 	CombatState = NewState;
// 	GetCharacterMovement()->MaxWalkSpeed = SpeedContainer[CombatState];
// }