// Fill out your copyright notice in the Description page of Project Settings.


#include "GBAIControllerBase.h"

#include "GameplayTagsManager.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GBProj/Actors/GBCharacterBase.h"
#include "GBProj/Actors/MainCharacter.h"
#include "GBProj/Actors/Enemies/EnemyBase.h"
#include "Perception/AIPerceptionComponent.h"




AGBAIControllerBase::AGBAIControllerBase()
{
	AIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("PerceptionCpomponent"));

	AIPerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this, &AGBAIControllerBase::OnTargetPerceptionUpdated);
}



void AGBAIControllerBase::BeginPlay()
{
	Super::BeginPlay();
	
	UseBlackboard(CustomBlackboard, BBComponent);

	RunBehaviorTree(CustomBehaviourTree);

	if(Blackboard)
	{
		Blackboard->SetValueAsFloat(FName("CurrentDistanceToTarget"), 10000.f);
		Blackboard->SetValueAsBool(FName("IsAlive"), true);
	}


}

void AGBAIControllerBase::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	
	if(const AGBCharacterBase* MyCharacter = Cast<AGBCharacterBase>(InPawn))
	{
		MyCharacter->HealthComponent->OnDead.AddDynamic(this, &AGBAIControllerBase::OnPawnDead);
	}

	if(const AEnemyBase* MyEnemy = Cast<AEnemyBase>(InPawn))
	{
		SetOriginLocation(MyEnemy->GetActorLocation());
		SetMaxFollowDistance(MyEnemy->MaxFollowDistance);
	}
}

void AGBAIControllerBase::OnPawnDead()
{
	Cast<AGBCharacterBase>(GetPawn())->HealthComponent->OnDead.RemoveDynamic(this, &AGBAIControllerBase::OnPawnDead);

	if(Blackboard)
	{
		Blackboard->SetValueAsBool(FName("IsAlive"), false);
	}

	UnPossess();

	Destroy();
}

void AGBAIControllerBase::OnTargetDead(AGBCharacterBase* DeadChar)
{
	if(Blackboard)
	{
		Blackboard->SetValueAsObject(FName("TargetCharacter"), nullptr);
	}

	DeadChar->CharacterDead.RemoveDynamic(this, &AGBAIControllerBase::OnTargetDead);
}

void AGBAIControllerBase::ClearTarget_Implementation()
{

}

void AGBAIControllerBase::OnTargetPerceptionUpdated_Implementation(AActor* Actor, FAIStimulus Stimulus)
{
	if(Actor->ActorHasTag(FName("Player")) && Blackboard)
	{
		if(Actor->ActorHasTag(FName("Alive")))
		{
			Blackboard->SetValueAsObject(FName("TargetCharacter"), Actor);
			AMainCharacter* TargetCharacter = Cast<AMainCharacter>(Actor);
			if(TargetCharacter)
			{
				TargetCharacter->CharacterDead.AddUniqueDynamic(this, &AGBAIControllerBase::OnTargetDead);
			}
		}
		else
		{
			Blackboard->SetValueAsObject(FName("TargetCharacter"), nullptr);
		}
		
	}
}

void AGBAIControllerBase::SetOriginLocation(FVector NewLocation)
{
	if(Blackboard)
	{
		Blackboard->SetValueAsVector(FName("OriginLocation"), NewLocation);
	}
}

void AGBAIControllerBase::SetMaxFollowDistance(float NewFollowDistance)
{
	if(Blackboard)
	{
		Blackboard->SetValueAsFloat(FName("MaxFollowDistance"), NewFollowDistance);
	}
}