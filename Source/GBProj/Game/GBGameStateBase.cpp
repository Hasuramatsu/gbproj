// Fill out your copyright notice in the Description page of Project Settings.


#include "GBGameStateBase.h"


#include "GBCharacter.h"
#include "Kismet/GameplayStatics.h"

void AGBGameStateBase::BeginPlay()
{
	Super::BeginPlay();
	AMainCharacter* MyCharacter = Cast<AMainCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if(MyCharacter)
	{
		MainCharacter = MyCharacter;
	}
}


void AGBGameStateBase::SetCombat_Implementation(const bool Flag)
{
	bIsCombat = Flag;
	OnCombatChange.Broadcast(Flag);	
}

