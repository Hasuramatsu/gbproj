// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/PlayerState.h"
#include "GBProj/Actors/ActorComponents/FlaskComponent.h"
#include "GBProj/Actors/Utility/CheckPointBase.h"

#include "GBPlayerStateBase.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API AGBPlayerStateBase : public APlayerState
{
	GENERATED_BODY()

public:
	AGBPlayerStateBase();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Component)
	UFlaskComponent* FlaskComponent = nullptr;

	UFUNCTION(BlueprintCallable)
	void AddCurrentLives(const int32 Value);

	//Setters
	UFUNCTION(BlueprintCallable)
	void SetLastCheckPoint(ACheckPointBase* CheckPoint) {LastCheckPoint = CheckPoint;}
	
	//Getters
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetMaxLives() const {return MaxLives;}
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetCurrentLives() const {return CurrentLives;}
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	ACheckPointBase* GetLastCheckPoint() const {return LastCheckPoint;}
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool CanContinue() const {return bCanContinue;}
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int32 MaxLives = 3;
	
	virtual void BeginPlay() override;
	
private:
	
	int32 CurrentLives = 0;
	
	UPROPERTY()
	ACheckPointBase* LastCheckPoint = nullptr;
	
	bool bCanContinue = true;


};
