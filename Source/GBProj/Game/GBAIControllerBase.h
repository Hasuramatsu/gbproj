// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "GBProj/Actors/GBCharacterBase.h"
#include "Perception/AIPerceptionTypes.h"

#include "GBAIControllerBase.generated.h"



UCLASS()
class GBPROJ_API AGBAIControllerBase : public AAIController
{
	GENERATED_BODY()

public:

	AGBAIControllerBase();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,  Category = "BehaviourTree")
	UBlackboardData* CustomBlackboard;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BehaviourTree")
	UBehaviorTree* CustomBehaviourTree;

	UFUNCTION(BlueprintPure)
	UBlackboardComponent* GetBBComponent() const {return BBComponent;}
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void ClearTarget();

	UFUNCTION(BlueprintCallable)
	void SetOriginLocation(FVector NewLocation);

	UFUNCTION(BlueprintCallable)
	void SetMaxFollowDistance(float NewFollowDistance);

	

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Perception")
	UAIPerceptionComponent* AIPerceptionComponent;
	
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintNativeEvent)
	void OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus);

	virtual void OnPossess(APawn* InPawn) override;


	
	UFUNCTION(BlueprintImplementableEvent)
	void OnPawnDead_Event();
private:

	UPROPERTY()
	UBlackboardComponent* BBComponent;

	UFUNCTION()
	void OnPawnDead();

	UFUNCTION()
	void OnTargetDead(AGBCharacterBase* DeadChar);
};
