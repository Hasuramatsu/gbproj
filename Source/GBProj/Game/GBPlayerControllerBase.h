// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GBCharacter.h"
#include "GameFramework/PlayerController.h"
#include "GBProj/Actors/MainCharacter.h"

#include "GBPlayerControllerBase.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPossessCharacter, AMainCharacter*, NewCharacter);
DECLARE_DELEGATE_OneParam(FBoolInput, const bool);
/**
 * 
 */
UCLASS()
class GBPROJ_API AGBPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AGBPlayerController();
	
	UPROPERTY(BlueprintAssignable);
	FOnPossessCharacter PossessCharacter;
	
	UFUNCTION(BlueprintCallable)
	void ReSpawnCharacter();

	UFUNCTION(BlueprintCallable)
	void RestartGame();

	UFUNCTION(BlueprintCallable)
	void ContinueGame();

	UPROPERTY(EditDefaultsOnly)
	bool bDebug = false;
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameCameraControl", meta=(ClampMin="0"))
	float MinArmLength = 400.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameCameraControl", meta=(ClampMin="0"))
	float MaxArmLength = 2000.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameCameraControl", meta=(ClampMin="0"))
	float UpdateTime = 0.1f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameCameraControl", meta=(ClampMin="0"))
	float ZoomStep = 50.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameCameraControl", meta=(ClampMin="0"))
	float InterpSpeed = 1.f;
	
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterDeath")
	float GlobalTimeDilation = 1.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterDeath")
	float MenuDelay = 1.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterDeath|DeathCamera")
	TSubclassOf<ACameraActor> DeathCameraClass;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterDeath|DeathCamera")
	bool bUseCustomCameraTransform = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterDeath|DeathCamera", meta=(EditCondition="bUseCustomCameraTransform"))
	FVector DeathCameraLocationOffset;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterDeath|DeathCamera", meta=(EditCondition="bUseCustomCameraTransform"))
	FRotator DeathCameraRotation;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterDeath|CameraBlend")
	float BlendTime = 1.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterDeath|CameraBlend")
	TEnumAsByte<EViewTargetBlendFunction> BlendFunction;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterDeath|CameraBlend")
	float BlendExp = 0.3f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterDeath|CameraBlend")
	bool bLookOutgoing = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterRespawn", meta=(ClampMin="0"))
	float RespawnCameraBlendTime = 0.f;

	// Overriden functions
	virtual void PlayerTick(float DeltaTime) override;
	
	virtual void SetupInputComponent() override;
	
	virtual void OnPossess(APawn* InPawn) override;

	UFUNCTION(BlueprintImplementableEvent)
	void WaitContinue();

	UFUNCTION(BlueprintImplementableEvent)
	void WaitRestart();

	// Input function

		// False = ZoomIn, True = ZoomOut
	UFUNCTION()
	void ApplyZoom(const bool Flag);
	
private:

	//TODO Remove
	bool bPaused = false;
	
	// Camera Arm Length Control
	UPROPERTY()
	float TargetAL;
	// Difference between current and target AL
	float TargetDistance;

	UPROPERTY()
	AMainCharacter* MyCharacter = nullptr;

	UPROPERTY()
	ACameraActor* DeathCamera = nullptr;
	
	FTimerHandle SpawnTimerHandle;
	FTransform DeadTransform;

	TSubclassOf<AMainCharacter> CharacterClass;

	UFUNCTION()
	void OnCharacterDeath(AGBCharacterBase* DeadCharacter);

	FTimerHandle RespawnDelayTimeHandle;
	FTimerHandle ZoomTimer;

	// Call restart menu
	UFUNCTION()
	void ContinueMenu();

	UFUNCTION()
	void UpdateZoom();

	UFUNCTION()
	void EscEvent();
};
