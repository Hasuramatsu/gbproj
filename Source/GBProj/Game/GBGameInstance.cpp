// Fill out your copyright notice in the Description page of Project Settings.


#include "GBGameInstance.h"
#include "GBProj/Func/Types.h"
bool UGBGameInstance::GetComboInfo(TSubclassOf<AWeaponBase> Primary, TSubclassOf<AWeaponBase> Secondary, TMap<FComboHandle, TSubclassOf<UGameplayAbility>>& Abilities) const
{
	bool bIsFind = false;
	if(ComboInfoTable)
	{
		TArray<FWeaponComboInfo*> Combos;
		ComboInfoTable->GetAllRows("", Combos);
		for(auto& It : Combos)
		{
			if(It->PrimaryWeapon == Primary && It->SecondaryWeapon == Secondary)
			{
				Abilities = It->WeaponAbilities;
				bIsFind = true;
				break;
			}
		}
	}
	return bIsFind;
}

bool UGBGameInstance::GetPresetAbilityTagsByName(const FName& PresetName, FAbilityTagsPreset& OutInfo)
{
	
	if(AbilityTagPresets)
	{
		FAbilityTagsPreset* TagsPreset = AbilityTagPresets->FindRow<FAbilityTagsPreset>(PresetName, "");
		if(TagsPreset)
		{
			OutInfo = *TagsPreset;
			return true;
		}
	}

	return false;
}

bool UGBGameInstance::GetCharacterDataByName(const FName& DataName, FCharacterData& OutData)
{
	if(CharacterDataTable)
	{
		FCharacterData* TagsPreset = CharacterDataTable->FindRow<FCharacterData>(DataName, "");
		if(TagsPreset)
		{
			OutData = *TagsPreset;
			return true;
		}
	}

	return false;
}
