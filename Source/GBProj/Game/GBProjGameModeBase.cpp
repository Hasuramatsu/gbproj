// Copyright Epic Games, Inc. All Rights Reserved.


#include "GBProjGameModeBase.h"



#include "GBProj/Actors/Enemies/EnemyBase.h"
#include "Kismet/GameplayStatics.h"

void AGBProjGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	MyGameState = GetGameState<AGBGameStateBase>();
	// if(MyGameState)
	// {
	// 	MyGameState->OnZoneChange.AddDynamic(this, &AGBProjGameModeBase::ZoneChanged);
	// 	MyGameState->OnCombatChange.AddDynamic(this, &AGBProjGameModeBase::CombatChanged);
	// }
	
	//Add all spawners to array
	TArray<AActor*> TempArray;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawner::StaticClass(), TempArray);
	for(AActor* It : TempArray)
	{
		ASpawner* Spawner = Cast<ASpawner>(It);
		if(Spawner->IsRespawnSpawner())
		{
			RespawnableSpawners.Add(Spawner);
			//Spawner->OnSpawn.AddDynamic(this, &AGBProjGameModeBase::EnemySpawned);
		}
	}
	RespawnLevel();
}

void AGBProjGameModeBase::ZoneChanged(FName Zone, bool Flag)
{
	
}

void AGBProjGameModeBase::CombatChanged(bool Flag)
{
}

// void AGBProjGameModeBase::EnemySpawned(AEnemyBase* Enemy)
// {
// 	Enemy->OnEnemyDead.AddDynamic(this,&AGBProjGameModeBase::EnemyDead);
// 	if(MyGameState)
// 	{
// 		MyGameState->ChangeTotalEnemiesCount(1);
// 		const FName ClassName = Enemy->ClassName;
// 		if(ClassName == TEXT("Skeleton"))
// 		{
// 			MyGameState->ChangeSkeletonCount(1);
// 		}
// 		else if(ClassName == TEXT("Vampire"))
// 		{
// 			MyGameState->ChangeVampiresCount(1);
// 		}
// 		else if(ClassName == TEXT("Werewolf"))
// 		{
// 			MyGameState->ChangeWerewolvesCount(1);
// 		}
// 		else if( ClassName == TEXT("Bat"))
// 		{
// 			MyGameState->ChangeBatsCount(1);
// 		}
// 		else
// 		{
// 			UE_LOG(LogTemp, Warning, TEXT("AGBProjGameModeBase::EnemySpawned -- Wrong enemy class name"));
// 		}
// 	}
// 	
// }
//
// void AGBProjGameModeBase::EnemyDead(AEnemyBase* Enemy)
// {
// 	if(MyGameState)
// 	{
// 		MyGameState->ChangeTotalEnemiesCount(-1);
// 		const FName ClassName = Enemy->ClassName;
// 		if(ClassName == TEXT("Skeleton"))
// 		{
// 			MyGameState->ChangeSkeletonCount(-1);
// 		}
// 		else if(ClassName == TEXT("Vampire"))
// 		{
// 			MyGameState->ChangeVampiresCount(-1);
// 		}
// 		else if(ClassName == TEXT("Werewolf"))
// 		{
// 			MyGameState->ChangeWerewolvesCount(-1);
// 		}
// 		else if( ClassName == TEXT("Bat"))
// 		{
// 			MyGameState->ChangeBatsCount(-1);
// 		}
// 		else
// 		{
// 			UE_LOG(LogTemp, Warning, TEXT("AGBProjGameModeBase::EnemySpawned -- Wrong enemy class name"));
// 		}
// 	}
// }




void AGBProjGameModeBase::RespawnLevel()
{
	TArray<AActor*> TempArray;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemyBase::StaticClass(), TempArray);
	for(auto& It : TempArray)
	{
		if(It)
		{
			It->Destroy();
		}
	}
	for(auto& It : RespawnableSpawners)
	{
		It->Spawn();
	}
}
