// Fill out your copyright notice in the Description page of Project Settings.


#include "GBPlayerControllerBase.h"

#include "GBCharacter.h"
#include "GBHUD.h"
#include "GBPlayerStateBase.h"
#include "GBProjGameModeBase.h"
#include "Camera/CameraActor.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"


AGBPlayerController::AGBPlayerController()
{
	bShowMouseCursor = false;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void AGBPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);	
}

void AGBPlayerController::SetupInputComponent()
{
	// setup gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction<FBoolInput>(TEXT("ZoomIn"), IE_Pressed,this, &AGBPlayerController::ApplyZoom, false);
	InputComponent->BindAction<FBoolInput>(TEXT("ZoomOut"), IE_Pressed,this, &AGBPlayerController::ApplyZoom, true);
	InputComponent->BindKey(EKeys::Escape, IE_Pressed, this, &AGBPlayerController::EscEvent);
}

void AGBPlayerController::ReSpawnCharacter()
{
	AGBProjGameModeBase* MyGameMode = Cast<AGBProjGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	if(MyGameMode)
	{
		MyGameMode->RespawnLevel();
	}

	//TODO remove

	if(MyCharacter)
	{
		MyCharacter->Destroy();
	}


	
	//
	
	AGBPlayerStateBase* MyPlayerState = GetPlayerState<AGBPlayerStateBase>();
	if(MyPlayerState && MyPlayerState->GetLastCheckPoint())
	{
		FVector CharacterSpawnLocation = MyPlayerState->GetLastCheckPoint()->GetSpawnCapsule()->GetComponentLocation();
		FRotator CharacterSpawnRotation = MyPlayerState->GetLastCheckPoint()->GetSpawnCapsule()->GetComponentRotation();
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = this;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		AMainCharacter* SpawnedCharacter = Cast<AMainCharacter>(GetWorld()->SpawnActor(CharacterClass, &CharacterSpawnLocation,&CharacterSpawnRotation, SpawnParameters));
		if(SpawnedCharacter)
		{
			Possess(SpawnedCharacter);
			MyPlayerState->FlaskComponent->AddFlaskCharges(MyPlayerState->FlaskComponent->GetFlaskMaxCharge() * MyPlayerState->FlaskComponent->GetFlaskChargeRespawnCoefficient());
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("AGBPlayerController::SpawnCharacter -- Failed Spawn."));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AGBPlayerController::SpawnCharacter -- No checkpoint."));
	}

	if(DeathCamera)
	{
		DeathCamera->Destroy();
	}

	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 1.f);
	
}

void AGBPlayerController::RestartGame()
{
	//TODO

	GEngine->AddOnScreenDebugMessage(-1, 2.5f, FColor::Purple, TEXT("AGBPlayerController::RestartGame -- Function not ready yet"));

	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 1.f);
}

void AGBPlayerController::ContinueGame()
{
	if(AGBHUD* HUD = Cast<AGBHUD>(GetHUD()))
	{
		HUD->ShowGameMenu(false);
	}

	bShowMouseCursor = false;

	SetPause(false);
}

void AGBPlayerController::OnCharacterDeath(AGBCharacterBase* DeadCharacter)
{
	if(MyCharacter && DeadCharacter && MyCharacter == DeadCharacter)
	{
		UGameplayStatics::SetGlobalTimeDilation(GetWorld(), GlobalTimeDilation);

		MyCharacter->CharacterDead.RemoveDynamic(this, &AGBPlayerController::OnCharacterDeath);
		
		UnPossess();
		
		if(DeathCameraClass)
		{
			FVector Location;
			FRotator Rotation = FRotator::ZeroRotator;
			if(bUseCustomCameraTransform)
			{
				Location = MyCharacter->GetActorLocation() + DeathCameraLocationOffset;
				Rotation = DeathCameraRotation;
			}
			else
			{
				Location = MyCharacter->GetActorLocation() - FVector(MyCharacter->CameraBoom->TargetArmLength, 0.f, 0.f);
			}
			DeathCamera = Cast<ACameraActor>(GetWorld()->SpawnActor(DeathCameraClass, &Location, &Rotation));
			
			if(DeathCamera)
			{
				SetViewTargetWithBlend(DeathCamera, BlendTime, BlendFunction, BlendExp, bLookOutgoing);
			}
		}

		AGBPlayerStateBase* MyState = GetPlayerState<AGBPlayerStateBase>();
		if(MyState)
		{
			MyState->AddCurrentLives(-1);

			GetWorld()->GetTimerManager().SetTimer(RespawnDelayTimeHandle, this, &AGBPlayerController::ContinueMenu, MenuDelay, false);
		}
	}


}

void AGBPlayerController::ContinueMenu()
{
	AGBPlayerStateBase* MyState = GetPlayerState<AGBPlayerStateBase>();
	if(MyState)
	{
		if(MyState->CanContinue())
		{
			WaitContinue();
		}
		else
		{
			WaitRestart();
		}	
	}
}

void AGBPlayerController::UpdateZoom()
{
	MyCharacter->CameraBoom->TargetArmLength = FMath::FInterpTo(MyCharacter->CameraBoom->TargetArmLength, TargetAL, UpdateTime, InterpSpeed);
	
	if(FMath::IsNearlyEqual(MyCharacter->CameraBoom->TargetArmLength, TargetAL))
	{
		GetWorld()->GetTimerManager().ClearTimer(ZoomTimer);
	}
}

void AGBPlayerController::EscEvent()
{
	if(!bPaused)
	{
		Pause();
		bShowMouseCursor = true;

		if(AGBHUD* HUD = Cast<AGBHUD>(GetHUD()))
		{
			HUD->ShowGameMenu(true);
		}
		
	}
	else
	{
		ContinueGame();
	}
}

void AGBPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	AGBPlayerStateBase* MyState = GetPlayerState<AGBPlayerStateBase>();
	if(MyState)
	{
		MyState->FlaskComponent->SetUser(InPawn);
		MyState->FlaskComponent->RegisterAbility();
	}
	MyCharacter = Cast<AMainCharacter>(InPawn);
	if(MyCharacter)
	{
		MyCharacter->CharacterDead.AddDynamic(this, &AGBPlayerController::OnCharacterDeath);
		MyCharacter->SetGBController(this);
		TargetAL = MyCharacter->CameraBoom->TargetArmLength;
		
		CharacterClass = MyCharacter->GetClass();

		PossessCharacter.Broadcast(MyCharacter);
	}
	
	SetViewTargetWithBlend(InPawn, RespawnCameraBlendTime, BlendFunction, BlendExp, bLookOutgoing);
}

void AGBPlayerController::ApplyZoom(const bool Flag)
{
	const float Temp = Flag ? ZoomStep : ZoomStep * -1;

	float TempFloat = TargetAL;
	
	TargetAL = FMath::Clamp(TargetAL + Temp, MinArmLength, MaxArmLength);

	if(bDebug && TempFloat != TargetAL)
	{
		const FString String = FString::Printf(TEXT("Target camera range set to %f."), TargetAL);
		GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Orange, String);
	}

	GetWorld()->GetTimerManager().SetTimer(ZoomTimer, this, &AGBPlayerController::UpdateZoom, UpdateTime, true);
}




