// // Fill out your copyright notice in the Description page of Project Settings.
//
// #pragma once
//
// #include "CoreMinimal.h"
//
// #include "GameFramework/Character.h"
// #include "GBProj/Actors/ActorComponents/HealthComponent.h"
// #include "GBProj/Actors/ActorComponents/InventoryComponent.h"
// #include "GBProj/Actors/ActorComponents/StaminaComponent.h"
// #include "GBProj/Actors/Weapon/RangedWeaponBase.h"
// #include "GBProj/Interfaces/InterfaceCharacter.h"
// #include "GBProj/Interfaces/InterfaceWeaponUser.h"
// #include "AbilitySystemInterface.h"
// #include "AbilitySystemComponent.h"
// #include "GBProj/AbilitySystem/GBAttributeSet.h"
//
//
// #include "GBCharacter.generated.h"
//
// DECLARE_DELEGATE_OneParam(FBoolInput, const bool);
// DECLARE_DELEGATE_OneParam(FSlotInput, const int32);
// DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponInit, ARangedWeaponBase*, Weapon);
// DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStateChange, ECombatState, State);
// DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCharacterDead);
// DECLARE_DYNAMIC_MULTICAST_DELEGATE(FStopPush);
//
//
// UCLASS()
// class GBPROJ_API AGBCharacter : public ACharacter, public IInterfaceCharacter, public IInterfaceWeaponUser, public IAbilitySystemInterface
// {
// 	GENERATED_BODY()
//
// public:
// 	// Sets default values for this character's properties
// 	AGBCharacter();
//
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
// 	UHealthComponent* HealthComponent = nullptr;
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
// 	UStaminaComponent* StaminaComponent = nullptr;
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
// 	UInventoryComponent* InventoryComponent = nullptr;
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Components)
// 	UAbilitySystemComponent* AbilitySystemComponent = nullptr;
// 	
// 	UPROPERTY(BlueprintAssignable)
// 	FOnWeaponInit OnWeaponInit;
// 	UPROPERTY(BlueprintAssignable, BlueprintCallable)
// 	FOnStateChange OnStateChange;
// 	UPROPERTY(BlueprintAssignable)
// 	FCharacterDead CharacterDead;
// 	UPROPERTY(BlueprintAssignable)
// 	FStopPush StopPush;
//
// 	UPROPERTY(EditAnywhere, BlueprintReadWrite)
// 	float HoldTimeForAim = 0.5f;
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
// 	float TimeToCalm = 3.f;
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
// 	int32 SecondsToIdle = 10;
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
// 	float TimeToDeadBody = 2.f;
//
// 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AimState")
// 	float AimMoveSpeed = 300.f;
//
// 	//Debug Utility
// 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
// 	bool bDebug = false;
// 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug", meta=(EditCondition=bDebug))
// 	float DebugFloatValue = 45.f;
//
// 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climb")
// 	FClimbInfo ClimbInfo;
// 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climb")
// 	FClimbJump ClimbJump;
// 	
//
//
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
// 	FCharacterSounds CharacterSounds;
//
//
// protected:
// 	//Dodge
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dodge")
// 	float DodgeStaminaCost = 50.f;
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dodge")
// 	float DodgeDuration = 1.f;
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dodge")
// 	float DodgeSpeed = 1500.f;
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dodge")
// 	float DodgeCooldown = 5.f;
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dodge")
// 	UAnimMontage* DodgeMontage;
//
// 	//Attributes
// 	UPROPERTY()
// 	UGBAttributeSet* AttributeSet;
// 	
// 	//Block
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Block")
// 	FBlockInfo BlockInfo;
// 	
//
// 	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Statement")
// 	ECharacterState CharacterState = ECharacterState::Walking;
//
// 	
// 	// Called when the game starts or when spawned
// 	UFUNCTION()
// 	virtual void BeginPlay() override;
// 	UFUNCTION()
// 	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
// 	
// 	UFUNCTION()
// 	virtual void OnJumped_Implementation() override;
// 	UFUNCTION()
// 	virtual void Landed(const FHitResult& Hit) override;
//
// 	///////////////////////////////////////////////////
// 	///Blueprint Events///////////////////////////////
// 	/////////////////////////////////////////////////
// 	
// 	//Block
// 	UFUNCTION(BlueprintImplementableEvent)
// 	void OnBlockStart_Event();
// 	UFUNCTION(BlueprintImplementableEvent)
// 	void OnBlockEnd_Event();
// 	UFUNCTION(BlueprintImplementableEvent)
// 	void OnBlockingHit_Event();
// 	UFUNCTION(BlueprintImplementableEvent)
// 	void OnBlockBreak_Event();
// 	
// private:
// 	//Components
// 	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
// 	class UCameraComponent* GBCamera;
// 	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
// 	class USpringArmComponent* CameraBoom;
// 	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
// 	class USceneComponent* SceneComponent;
//
// 	//Contain speed values for each stance
// 	TMap<ECombatState, float> SpeedContainer;
// 	
// 	//Flags
// 	UPROPERTY()
// 	bool bIsAlive = true;
// 	UPROPERTY()
// 	bool bCanInteract = true;
// 	UPROPERTY()
// 	bool bIsPushing = false;
// 	UPROPERTY()
// 	bool bIsGrabbing = false;
// 	
//
// 	
// 	//Base values
// 	float BaseSpeed;
// 	float AxisY;
//
// 	//
// 	UPROPERTY()
// 	class AGBPlayerController* GBPlayerController	 = nullptr;
//
// 	
// 	//Weapon
// 	UPROPERTY()
// 	ECurrentWeaponType CurrentWeaponType = ECurrentWeaponType::Primary;
// 	UPROPERTY()
// 	bool bCanMeleeAttack = true;
// 	UPROPERTY()
// 	ARangedWeaponBase* CurrentWeapon = nullptr;
// 	UPROPERTY()
// 	int32 CurrentWeaponIndex = 0;
// 	UPROPERTY()
// 	AMeleeWeaponBase* CurrentMeleeWeapon = nullptr;
//
// 	//Interaction
// 	UPROPERTY()
// 	AActor* InteractionTarget = nullptr;
// 	UPROPERTY()
// 	TArray<AActor*> InteractionArray;
// 	
// 	//Location of cursor
// 	UPROPERTY()
// 	FVector TargetLocation;
//
// 	//Statements
// 	ECombatState CombatState = ECombatState::MeleeState;
// 	ECombatState PreviousCombatState;
//
//
// 	//Timers
// 	FTimerDelegate FlaskTimerDelegate;
// 	FTimerHandle FlaskTimerHandle;
// 	FTimerHandle StateTimerHandle;
//
// 	//Weapon Func
// 	UFUNCTION()
//     bool InitWeapon(int32 Index);
// 	UFUNCTION()
// 	bool InitMeleeWeapon();
//
// 	//Flask func
// 	UFUNCTION()
// 	void ActivateFlask(const bool Flag);
//
// 	//Dodge
// 	bool bDodgeReady = true;
// 	bool bDodging = false;
// 	FVector LastMovementInputVector;
// 	float TempSpeed;
// 	
// 	FTimerHandle DodgeCooldownHandle;
// 	FTimerHandle DodgeHandle;
// 	
// 	void Dodge();
// 	void EndDodge();
// 	void ResetDodgeCooldown() {bDodgeReady = true;}
// 	bool CheckCanDodge() const;
//
// 	//Block
// 	float BlockTimerTickTime = 0.3f;
// 	FTimerHandle EnterBlockTimer;
//
// 	//Check condition for entering bock stance
// 	bool CheckCanEnterBlock() const;
// 	//Try enter block stance and start loop of attempts if fail
// 	void EnterBlockStance();
// 	//Exit block stance
// 	void ExitBlockStance(ECombatState NewState = ECombatState::MeleeState);
//
// 	//Climb
// 	float ClimbTraceTickRate = 0.1f;
// 	float RotationTime = 0.f;
// 	FRotator BackJumpRotation;
// 	FTimerHandle ClimbTraceTimer;
// 	FTimerHandle JumpRotationTimer;
//
// 	void ClimbTick();
// 	void StickWall(FHitResult& HitResult);
// 	void JumpRotationTick();
//
// 	//RightClick Actions
// 	FTimerDelegate AimDelegate;
// 	FTimerHandle AimTimer;
// 	void SecondaryAction(const bool Flag);
//
// 	//Heal
// 	UFUNCTION()
// 	void HealEnd();
//
// 	//Called on death
// 	UFUNCTION()
// 	void DestroyAllWeapon();
//
// 	
// 	void OnMovementSpeedAttributeChanged(const FOnAttributeChangeData& Data);
//
// public:	
// 	// Called every frame
// 	virtual void Tick(float DeltaTime) override;
// 	void RotationTick(float DeltaTime);
// 	void MovementTick(float DeltaTime);
//
// 	//Setup control
// 	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
//
//
// 	//Set current weapon visible and active
// 	UFUNCTION(BlueprintCallable)
// 	void ActivateWeapon();
// 	//Hide and deactivate all weapon
// 	UFUNCTION(BlueprintCallable)
//     void DeactivateAllWeapon();
//
//
// 	//Dodge
// 	UFUNCTION(BlueprintImplementableEvent)
// 	void OnDodgeStart();
// 	UFUNCTION(BlueprintImplementableEvent)
// 	void OnDodgeEnd();
//
// 	// Variables for animation
// 	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Animation")
// 		float PitchAngle;
// 	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Animation")
// 		bool bPushing = false;
// 	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Animation")
// 		bool bPulling = false;
// 	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Animation")
// 		float PushDirection = 0.f;
// 	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Animation")
// 		bool bUsingFlask= 0.f;
// 	
// 	//Weapon
// 	void SwapCurrentWeapon(ECurrentWeaponType Type);
// 	
// 	//InputFunc
// 	UFUNCTION(BlueprintCallable)
// 	void InputAttack(const bool Flag);
// 	UFUNCTION(BlueprintCallable)
// 	void ChargeAttack(const bool Flag);
// 	UFUNCTION(BlueprintCallable)
// 	void MeleeAttack();
// 	UFUNCTION(BlueprintImplementableEvent)
// void MeleeAttack_BP();
// 	UFUNCTION(BlueprintCallable)
// 	void Reload();
// 	UFUNCTION(BlueprintCallable)
// 	void SwitchWeaponByIndex(int32 Index);
// 	UFUNCTION(BlueprintCallable)
// 	void InputAxisY(float Value);
// 	UFUNCTION(BlueprintCallable)
// 	void Interact(const bool Flag);
// 	UFUNCTION(BlueprintCallable)
// 	void InputJump(const bool Flag);
// 	UFUNCTION(BlueprintCallable)
// 	void InputFlaskUse(const bool Flag);
// 	UFUNCTION(BlueprintCallable)
// 	void EnterAimState(const bool Flag);
// 	void UnstickWall();
// 	UFUNCTION(BlueprintCallable)
// 	void DownInput();
// 	
// 	void InputExitBlockStance() {ExitBlockStance();}
//
//
// 	//DelegateCatcherFunc
// 	UFUNCTION(BlueprintNativeEvent)
//     void OnFire(UAnimMontage* Montage);
// 	UFUNCTION(BlueprintNativeEvent)
// 	void OnReload(UAnimMontage* Montage);
// 	UFUNCTION(BlueprintNativeEvent)
// 	void OnReloadEnd(bool Result);
//
// 	virtual void DeadEvent_Implementation() override;
//
// 	//Getters
// 	UFUNCTION(BlueprintCallable, BlueprintPure)
// 	ARangedWeaponBase* GetCurrentWeapon() const {return CurrentWeapon;}
// 	UFUNCTION(BlueprintCallable, BlueprintPure)
// 	AMeleeWeaponBase* GetMeleeWeapon() const {return CurrentMeleeWeapon;}
// 	UFUNCTION(BlueprintCallable, BlueprintPure)
// 	int32 GetCurrentWeaponIndex() const {return CurrentWeaponIndex;}
// 	UFUNCTION(BlueprintCallable, BlueprintPure)
// 	virtual bool IsAlive_Implementation() const override {return bIsAlive;}
// 	UFUNCTION(BlueprintCallable, BlueprintPure)
// 	bool CanInteract() const {return bCanInteract;}
// 	UFUNCTION(BlueprintCallable, BlueprintPure)
// 	AActor* GetCurrentInteractable() const {return InteractionTarget;}
// 	UFUNCTION(BlueprintCallable, BlueprintPure)
// 	float GetAxisY() const {return AxisY;}
// 	UFUNCTION(BlueprintCallable, BlueprintPure)
// 	UCameraComponent* GetCamera() const {return GBCamera;}
// 	//add for change texture in BP
// 	UFUNCTION(BlueprintCallable, BlueprintPure)
// 	bool Dodging() const { return bDodging; }
// 	UFUNCTION(BlueprintPure)
// 	FORCEINLINE ECombatState GetCombatState() const {return CombatState;}
// 	UFUNCTION(BlueprintPure)
// 	FVector GetClosestEnemyLocation() const;
// 	
//
// 	//Setters
// 	UFUNCTION(BlueprintCallable)
// 	void SetCanMeleeAttack(const bool Value) {bCanMeleeAttack = Value;}
// 	UFUNCTION(BlueprintCallable)
// 	void SetIsAlive(const bool Flag);
// 	UFUNCTION(BlueprintCallable)
// 	void SetCanInteract(const bool Flag) {bCanInteract = Flag;}
// 	UFUNCTION(BlueprintCallable)
// 	void SetCharacterState(const ECharacterState State);
//
// 	UFUNCTION(BlueprintCallable)
// 	void SetCombatState(const ECombatState NewState);
// 	
//
// 	//Interface functions
// 	virtual FVector GetTargetLocation_Implementation() override;
//
// 	UFUNCTION(BlueprintCallable)
// 	virtual void EnableWeapon_Implementation() override;
// 	UFUNCTION(BlueprintCallable)
// 	virtual void DisableWeapon_Implementation() override;
// 	
//
// 	//PushingUPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(ClampMin="0", ClampMax="1"))
// 	UFUNCTION(BlueprintCallable, BlueprintPure)
// 	virtual bool CanPush_Implementation() override;
// 	UFUNCTION(BlueprintCallable)
// 	virtual void StartPush_Implementation(AActor* Actor) override;
// 	UFUNCTION(BlueprintCallable)
// 	virtual void EndPush_Implementation() override;
// 	UFUNCTION(BlueprintCallable)
// 	virtual void GrabPushed_Implementation(const bool Flag) override;
// 	
// 	//Interaction
// 	UFUNCTION(BlueprintCallable)
// 	void AddInteraction(AActor* Interactable);
// 	UFUNCTION(BlueprintCallable)
// 	void RemoveInteraction(AActor* Interactable);
//
// 	//Checks
// 	bool CheckCanJump() const;
// 	UFUNCTION(BlueprintPure)
// 	bool CheckCanMeleeAttack() const;
//
// 	UFUNCTION(BlueprintCallable)
// 	bool AttemptClimbUp();
// 	UFUNCTION(BlueprintCallable)
// 	bool AttemptClimbJump();
// 	UFUNCTION(BlueprintCallable)
// 	void OnClimbUpEnd();
// 	UFUNCTION(BlueprintCallable)
// 	void OnStickWallEnd();
//
// 	//Ability Interface override
// 	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override {return AbilitySystemComponent;}
// };
//
//
//
//
//
