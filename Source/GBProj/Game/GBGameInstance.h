// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/DataTable.h"
#include "Engine/GameInstance.h"
#include "GBProj/Actors/Weapon/WeaponBase.h"

#include "GBGameInstance.generated.h"


class UGameplayAbility;
struct FComboHandle;
class AWeaponBase;
/**
 * 
 */
UCLASS()
class GBPROJ_API UGBGameInstance : public UGameInstance
{
	GENERATED_BODY()


public:


	UFUNCTION(BlueprintPure)
	bool GetComboInfo(TSubclassOf<AWeaponBase> Primary, TSubclassOf<AWeaponBase> Secondary, TMap<FComboHandle, TSubclassOf<UGameplayAbility>>& Abilities) const;

	// Find and out AbilityTags with name. Return false if not found.
	UFUNCTION(BlueprintPure)
	bool GetPresetAbilityTagsByName(const FName& PresetName, FAbilityTagsPreset& OutInfo);

	UFUNCTION(BlueprintPure)
	bool GetCharacterDataByName(const FName& DataName, FCharacterData& OutData);

protected:
	// Table with combo 
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tables")
	UDataTable* ComboInfoTable = nullptr;

	// Tag Presets to load from abilities
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tables")
	UDataTable* AbilityTagPresets = nullptr;

	// Tables with character data
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tables")
	UDataTable* CharacterDataTable = nullptr;
};
