// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "GBGameStateBase.h"
#include "GameFramework/GameModeBase.h"
#include "GBProj/Actors/Utility/Spawner.h"
#include "GBProj/Func/Types.h"

#include "GBProjGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API AGBProjGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	void RespawnLevel();
protected:
	virtual void BeginPlay() override;
	
private:
	UPROPERTY()
	AGBGameStateBase* MyGameState = nullptr;

	UPROPERTY()
	TArray<ASpawner*> RespawnableSpawners;
	//Call on GamePhaseChange
	UFUNCTION()
	void ZoneChanged(FName Zone, bool Flag);
	UFUNCTION()
	void CombatChanged(bool Flag);
	// UFUNCTION()
	// void EnemySpawned(AEnemyBase* Enemy);
	// UFUNCTION()
	// void EnemyDead(AEnemyBase* Enemy);

};

