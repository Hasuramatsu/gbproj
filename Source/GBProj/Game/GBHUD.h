// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "GBProj/Func/Types.h"
#include "GBHUD.generated.h"

/**
 * 
 */
UCLASS()
class GBPROJ_API AGBHUD : public AHUD
{
	GENERATED_BODY()


public:

	// Implemented in BP
	UFUNCTION(BlueprintImplementableEvent)
	void ShowInteractAction(FDisplayInfo Info, bool Flag);

	UFUNCTION(BlueprintImplementableEvent)
	void ShowGameMenu(bool Flag);
	
protected:

	
private:

	
};
