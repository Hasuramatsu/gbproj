// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GBCharacter.h"
#include "GameFramework/GameStateBase.h"
#include "GBProj/Actors/MainCharacter.h"
#include "GBProj/Actors/Enemies/EnemyBase.h"

#include "GBGameStateBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCombatChange, bool, Flag);


/**
 * 
 */
UCLASS()
class GBPROJ_API AGBGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	bool bIsCombat = false;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
	FOnCombatChange OnCombatChange;

	
	//Max enemies of each type alive

	

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	AMainCharacter* MainCharacter = nullptr;

	
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SetCombat(const bool Flag);


};
